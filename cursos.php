
<!doctype html>
<html lang="es">
  <head>
      <?php
          include 'common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include 'common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-6 px-0">
          <h1 class="display-4">Cursos</h1>
        </div>
      </div>

      <div class="container">
        <div class="row mt-3">
          <div class="col-md-12">
            <div id="system-message-container">
            </div>
            <div class="category-list">
            <div>
            <div class="content-category">
              <div class="category-desc">
                <p>&nbsp;En <strong>Navemoción</strong> te ofrecemos variados cursos con los mejores profesionales:&nbsp;&nbsp;</p>
              </div>

              <table class="category table table-striped table-bordered table-hover table-noheader">
                <tbody>
                  <tr class="cat-list-row0">
                    <td headers="categorylist_header_title" class="list-title">
                      <a href="<?php echo $link; ?>cursos/curso-conductor-nautico.php">
                        Conductor Náutico
                      </a>
                    </td>
                  </tr>
                  <tr class="cat-list-row1">
                    <td headers="categorylist_header_title" class="list-title">
                      <a href="<?php echo $link; ?>cursos/curso-timonel-de-yate.php">
                        Curso de Timonel de yate a vela y/o Motor
                      </a>
                    </td>
                  </tr>
                  <tr class="cat-list-row0">
                    <td headers="categorylist_header_title" class="list-title">
                      <a href="<?php echo $link; ?>cursos/curso-patron-de-yate.php">
                        Curso de Patrón de Yate a Vela y Motor
                      </a>
                    </td>
                  </tr>
                  <tr class="cat-list-row1">
                    <td headers="categorylist_header_title" class="list-title">
                      <a href="<?php echo $link; ?>cursos/curso-navegacion-astronomica.php">
                        Curso de Navegación Astronómica (Piloto de Yate)
                      </a>
                    </td>
                  </tr>
                  <tr class="cat-list-row0">
                    <td headers="categorylist_header_title" class="list-title">
                      <a href="<?php echo $link; ?>cursos/curso-meteorologia.php">
                        Curso de Meteorología
                      </a>
                    </td>
                  </tr>
                  <tr class="cat-list-row1">
                    <td headers="categorylist_header_title" class="list-title">
                      <a href="<?php echo $link; ?>cursos/curso-gps-navegacion-satelital.php">
                        Curso de G.P.S - Navegación Satelital
                      </a>
                    </td>
                  </tr>
                  <tr class="cat-list-row0">
                    <td headers="categorylist_header_title" class="list-title">
                      <a href="<?php echo $link; ?>cursos/curso-clases-particulares.php">
                        Clases particulares / Traslado de embarcaciones
                      </a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include 'common/footer.php';
      ?>
    </main>
  </body>
</html>
