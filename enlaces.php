<!doctype html>
<html lang="es">

<head>
	<?php
	include 'common/head.php';
	?>
</head>

<body>
	<header style="height: 100px;">
		<?php
		include 'common/nav.php';
		?>
	</header>
	<main role="main">
		<div class="container img-header">
			<div class="col-md-6 px-0">
				<h1 class="display-4">Enlaces</h1>
			</div>
		</div>

		<div class="container">
			<div itemprop="articleBody">
				<table border="0" cellpadding="0" cellspacing="0" width="95%">
					<tbody>
						<tr>
							<td colspan="2">&nbsp;
								<table align="center" border="0" cellpadding="0" cellspacing="0" width="97%">
									<tbody>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_1.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_2.gif" style=""></td>
										</tr>
										<tr>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td width="145"><a href="http://www.revesta.com.ar/" rel="noopener noreferrer" target="_blank"><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/revesta.gif" style=""></a></td>
											<td class="tabletext">Pinturas Náuticas REVESTA.</td>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
										</tr>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_4.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_3.gif" style=""></td>
										</tr>
									</tbody>
								</table>
								&nbsp;

								<table align="center" border="0" cellpadding="0" cellspacing="0" width="97%">
									<tbody>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_1.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_2.gif" style=""></td>
										</tr>
										<tr>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td width="145"><a href="http://www.naviar.com.ar/" rel="noopener noreferrer" target="_blank"><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/naviar.gif" style=""></a></td>
											<td class="tabletext">Cartas digitales para GPS.</td>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
										</tr>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_4.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_3.gif" style=""></td>
										</tr>
									</tbody>
								</table>
								&nbsp;

								<table align="center" border="0" cellpadding="0" cellspacing="0" width="97%">
									<tbody>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_1.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_2.gif" style=""></td>
										</tr>
										<tr>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td width="145"><a href="http://www.riovia.com/fotonor.php" rel="noopener noreferrer" target="_blank"><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/pilote.gif" style=""></a></td>
											<td class="tabletext">Ahora podrás saber cómo es la marea, la corriente y el viento en nuestro río a través de los datos que recaba el <a class="linkvolver" href="http://www.riovia.com/fotonor.php" rel="noopener noreferrer" target="_blank">Pilote Norden</a> (la información se actualiza ¡cada 5 minutos!). Podrás acceder en nuestra sección <a href="/index.php/pronosticos-y-mareas">Pronósticos y Mareas</a></td>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
										</tr>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_4.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_3.gif" style=""></td>
										</tr>
									</tbody>
								</table>
								&nbsp;

								<table align="center" border="0" cellpadding="0" cellspacing="0" width="97%">
									<tbody>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_1.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_2.gif" style=""></td>
										</tr>
										<tr>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td width="145"><a href="http://www.blakexpeditions.com/" rel="noopener noreferrer" target="_blank"><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/blake.jpg" style=""></a></td>
											<td class="tabletext">Para tener detalles de las actividades realizadas por el famoso navegante Sir Peter Blake (trágicamente desaparecido en el Amazonas) entra en <a class="linkvolver" href="http://www.blakexpeditions.com/" rel="noopener noreferrer" target="_blank">Blakexpeditions</a>.</td>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
										</tr>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_4.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_3.gif" style=""></td>
										</tr>
									</tbody>
								</table>
								&nbsp;

								<table align="center" border="0" cellpadding="0" cellspacing="0" width="97%">
									<tbody>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_1.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_2.gif" style=""></td>
										</tr>
										<tr>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td width="145"><a href="http://www.terra.es/personal/luisuxio/" rel="noopener noreferrer" target="_blank"><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/nudos.jpg" style=""></a></td>
											<td align="center" class="tabletext"><a class="linkvolver" href="http://www.terra.es/personal/luisuxio/" rel="noopener noreferrer" target="_blank">http://www.terra.es/personal/luisuxio/</a></td>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
										</tr>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_4.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_3.gif" style=""></td>
										</tr>
									</tbody>
								</table>
								&nbsp;

								<table align="center" border="0" cellpadding="0" cellspacing="0" width="97%">
									<tbody>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_1.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_2.gif" style=""></td>
										</tr>
										<tr>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td align="center" width="145"><a href="http://www.neptuno.es" rel="noopener noreferrer" target="_blank"><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/neptuno.gif" style=""></a></td>
											<td class="tabletext"><a class="linkvolver" href="http://www.neptuno.es" rel="noopener noreferrer" target="_blank">http://www.neptuno.es</a></td>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
										</tr>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_4.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_3.gif" style=""></td>
										</tr>
									</tbody>
								</table>
								&nbsp;

								<table align="center" border="0" cellpadding="0" cellspacing="0" width="97%">
									<tbody>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_1.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_2.gif" style=""></td>
										</tr>
										<tr>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td width="145"><a href="http://www.pfdb.com.ar" rel="noopener noreferrer" target="_blank"><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/pepe.gif" style=""></a></td>
											<td align="center" class="tabletext"><a class="linkvolver" href="http://www.pfdb.com.ar" rel="noopener noreferrer" target="_blank">http://www.pfdb.com.ar</a></td>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
										</tr>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_4.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_3.gif" style=""></td>
										</tr>
									</tbody>
								</table>
								&nbsp;

								<table align="center" border="0" cellpadding="0" cellspacing="0" width="97%">
									<tbody>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_1.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_2.gif" style=""></td>
										</tr>
										<tr>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td width="145"><a href="http://www.trivianautica.com.ar" rel="noopener noreferrer" target="_blank"><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/trivia.gif" style=""></a></td>
											<td align="center" class="tabletext"><a class="linkvolver" href="http://www.trivianautica.com.ar" rel="noopener noreferrer" target="_blank">http://www.trivianautica.com.ar</a></td>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
										</tr>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_4.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_3.gif" style=""></td>
										</tr>
									</tbody>
								</table>
								&nbsp;

								<table align="center" border="0" cellpadding="0" cellspacing="0" width="97%">
									<tbody>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_1.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_2.gif" style=""></td>
										</tr>
										<tr>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td width="145"><a href="http://kayak.escueladeremo.com.ar" rel="noopener noreferrer" target="_blank"><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/delta.gif" style=""></a></td>
											<td align="center" class="tabletext"><a class="linkvolver" href="http://kayak.escueladeremo.com.ar" rel="noopener noreferrer" target="_blank">http://kayak.escueladeremo.com.ar</a></td>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
										</tr>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_4.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_3.gif" style=""></td>
										</tr>
									</tbody>
								</table>
								&nbsp;

								<table align="center" border="0" cellpadding="0" cellspacing="0" width="97%">
									<tbody>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_1.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_2.gif" style=""></td>
										</tr>
										<tr>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td width="145"><a href="http://www.cruceroguia.com" rel="noopener noreferrer" target="_blank"><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/cruceroguia.gif" style=""></a></td>
											<td align="center" class="tabletext"><a class="linkvolver" href="http://www.cruceroguia.com" rel="noopener noreferrer" target="_blank">http://www.cruceroguia.com</a></td>
											<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
										</tr>
										<tr>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_4.gif" style=""></td>
											<td colspan="2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
											<td height="16" width="14"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/table_corner_3.gif" style=""></td>
										</tr>
									</tbody>
								</table>
							</td>
							<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/traspa.gif" style=""></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!-- /.container -->
		<!-- FOOTER -->
		<?php
		include 'common/footer.php';
		?>
	</main>
</body>

</html>