<!doctype html>
<html lang="es">

<head>
  <?php
  include '../common/head.php';
  ?>
</head>

<body>
  <header style="height: 100px;">
    <?php
    include '../common/nav.php';
    ?>
  </header>
  <main role="main">
    <div class="container img-header">
      <div class="col-md-12 px-0">
        <h1 class="display-4">Alumnos - </h1>
      </div>
    </div>
    <div class="container grey">
      <div>
        <p>&nbsp;</p>
        <h4>APUNTES</h4>
        <hr>
        <ul class="list-group">
          <li class="list-group-item">
              <a href="<?php echo $link; ?>alumnos/login/ubam_popup.php" 
                target="popup" 
                onclick="popupCenter({url: '<?php echo $link; ?>alumnos/login/ubam_popup.php', title: 'Popup', w: 600, h: 350});">
                Curso de Meteorología
              </a>
          </li>
          <li class="list-group-item">
              <a href="<?php echo $link; ?>alumnos/login/uca_popup.php" 
                target="popup" 
                onclick="popupCenter({url: '<?php echo $link; ?>alumnos/login/uca_popup.php', title: 'Popup', w: 600, h: 400});">
                Curso de Timonel de Yate a vela y/o motor 1
              </a>
          </li>
          <li class="list-group-item">
              <a href="<?php echo $link; ?>alumnos/login/uba_popup.php" 
                target="popup" 
                onclick="popupCenter({url: '<?php echo $link; ?>alumnos/login/uba_popup.php', title: 'Popup', w: 600, h: 400});">
                Curso de Timonel de Yate a vela y/o motor 2
              </a>
          </li>
          <li class="list-group-item">
              <a href="<?php echo $link; ?>alumnos/login/uba2_popup.php" 
                target="popup" 
                onclick="popupCenter({url: '<?php echo $link; ?>alumnos/login/uba2_popup.php', title: 'Popup', w: 600, h: 350});">
                Curso de Patrón de Yate a vela y motor
              </a>
          </li>
        </ul>
        <p>&nbsp;</p>
      </div>
    </div>
    <!-- /.container -->
    <!-- FOOTER -->
    <?php
    include '../common/footer.php';
    ?>
  </main>
</body>

</html>