<!doctype html>
<html lang="es">

<head>
  <?php
  include '../common/head.php';
  ?>
</head>

<body>
  <header style="height: 100px;">
    <?php
    include '../common/nav.php';
    ?>
  </header>
  <main role="main">
    <div class="container img-header">
      <div class="col-md-12 px-0">
        <h1 class="display-4">Alumnos - Ecología</h1>
      </div>
    </div>
    <div class="container grey">
      <div itemprop="articleBody">
        <p>&nbsp;</p>
        <p><img class="img-fluid" src="<?php echo $link; ?>img/ecologia1.jpg" align="right" border="0" hspace="5" vspace="0"></p>
        <div align="center">Sentinas al Sol</div>
        <div align="center">
          <p><i><b>INFORME 2004 DE LA UNIVERSIDAD NACIONAL DE LA PLATA</b></i></p>
        </div>
        <div class="commontext" align="left">
          <p>Buenos Aires: Una nueva sustancia ocasionaría cáncer de hígado <br>Según los especialistas podría causar tumores en los seres humanos. La prueba se realizó con ratones de laboratorio en los que encontraron anomalías debido a la ingesta de agua de río <br>Segun un informe de la Universidad Nacional de La Plata (UNLP), se encontró una toxina en el Río de la Plata que ocasionaría tumores en seres humanos. Esta sustancia se desprende de un alga y fue inoculada en ratones de laboratorio, los cuales murieron y se detectaron anomalías en sus hígados. <br>Un muestreo tomado en la zona de influencia del Puerto de La Plata por especialistas de la UNLP, que ya fue girado al Gobierno bonaerense, advierte sobre la presencia de una toxina en la superficie del agua que no se elimina con el proceso de potabilización y que puede causar cáncer de hígado. <br>El informe habla de un concreto riesgo para la salud de la población, y en él se pide al Gobierno provincial medidas urgentes. <br>El documento, redactado hace menos de un mes, menciona la presencia de la "Cyanobacteria Microcystis aeruginosa", un alga "conocida como potencial productora de hepatotoxinas". <br>Con la muestra obtenida del agua, los especialistas de la UNLP realizaron una prueba con resultados preocupantes. La inyectaron en ratones de laboratorio, y todos los animales inoculados "murieron en menos de 40 minutos". Como parte de un proceso que es rutina en ámbitos científicos, los ratones fueron disecados, y los investigadores comprobaron que sus hígados habían "variado de color, tamańo y peso". <br>Los potenciales riesgos <br>En el mismo documento se asegura que "las microcistinas son ampliamente conocidas como promotoras de tumores, y su presencia en agua de consumo humano se correlaciona con altos niveles de cáncer primario de hígado". <br>También se subraya que esta situación se pone en conocimiento del Gobierno provincial porque "la presencia de un florecimiento altamente tóxico de Microcystis aeruginosa representa un riesgo para la fauna acuática, así como para la población expuesta al área de influencia". <br>Recientes publicaciones científicas sobre estas algas dicen que "el potencial de algunas especies frecuentes de cianobacterias (especialmente Microcystis y Anabaena) para formar espuma es una razón adicional para estar alerta". Y explican que "en pocas horas, estas espumas pueden aumentar la densidad celular local y la concentración de toxinas, lo que hace que el riesgo para bańistas y las personas que tengan contacto con el agua varíe rápido, de moderado a alto". <br>Las recomendaciones <br>Según los especialistas de la UNLP: "Los resultados obtenidos en el Puerto de La Plata son de relevancia para la toma de decisiones urgentes, a fin de salvaguardar la salud de la población y poder evitar situaciones como las de Caruarú (estado de Pernambuco, en Brasil), donde más de 50 personas murieron a partir de un florecimiento de algas productoras de cianotoxinas". <br>Por otros casos registrados en el Río de la Plata, la Prefectura recomendó como medida de prevención "evitar el contacto directo con las algas y evitar la ingestión del agua de la zona afectada y la inhalación de la espuma generada por el oleaje en áreas de acumulación". <br>Actualmente las conclusiones del muestreo están en manos del Organismo Regulador de Aguas Bonaerenses (ORAB), cuyo directorio está virtualmente acéfalo, ante la reciente designación de quien era su presidente, Luis Sanguinetti, como subsecretario de Servicios Públicos bonaerense. Y hasta el momento no hubo decisiones que se hicieran públicas. <br>El alga había sido detectada <br>El 1ş de diciembre de 2003 cientos de sábalos cubrieron las arenas de la costa del Río de La Plata, en Punta Lara. La muerte de los peces no tenía, hasta entonces, una causa aparente. Pero las autoridades de la Subsecretaría de Política Ambiental de la Provincia tomaron poco tiempo después muestras en las aguas del río, y el 30 del mismo mes difundieron parte de un informe en el que ya se admitía la presencia de algas "potencialmente tóxicas", pero no se daba cuenta de la peligrosidad que tenían para la vida humana. <br>En consideración de la aparición de algas, el estudio dice que "se pudo constatar la existencia en las muestras de 'Microcystis aeruginosa', especie que produce frecuentemente floraciones, provocando consumo de oxígeno del agua durante la noche y pudiendo ocasionar fenómenos de anoxia, los que pueden afectar a los peces y otros organismo acuáticos". <br>Pero ese tipo de algas no sólo compite por el escaso oxígeno presente en las castigadas aguas del Río de la Plata. Se trata, según el informe oficial, de "algas que son potencialmente tóxicas". Ahora se revela que también son peligrosas para la vida humana. <br>Sus consecuencias: <br>Los efectos de la toxina que libera el alga no siempre son letales, pero el riesgo potencial parece ser enorme, según las advertencias de los especialistas en la materia. <br>En altas concentraciones de células cianobacterianas, la probabilidad de síntomas irritantes es elevada. <br>Los datos usados para el valor guía provisional para microcistina en agua potable también se pueden usar para evaluar el riesgo por el simple contacto de una persona con el alga. <br>Por ejemplo, al bańarse en el río, los nadadores tragan involuntariamente cierta cantidad de agua y "corren igual peligro que si hubieran consumido agua potable con el mismo contenido de toxinas", según un informe que se presentó el ańo pasado. <br>Además, hay buenas evidencias sobre los efectos potencialmente severos, asociados con espumas de cianobacterias tóxicas. Tomando como referencia la información de la Organización Mundial de la Salud (OMS), se determinó que los niveles de microcistina registrados en la zona de mayor acumulación algal en el Dique 4 de Puerto Madero podrían ocasionar problemas para la salud, como irritación de la piel y/o trastornos digestivos. Esto dependería de variables tales como: la vía de contacto, el tiempo de exposición y las altas concentraciones algales presentes.</p>
          <p><img class="img-fluid" src="<?php echo $link; ?>img/ecologia2.jpg"></p>
          <div align="center">
            <p>Opinión</p>
          </div>
          <p>Este comienzo de siglo, que también es considerado como el término de la Edad Moderna, es evidentemente una época turbulenta, de grandes transformaciones. Pero es en tiempos así, cuando las estructuras parecen aflojarse como papel mojado, cuando debe darse cabida a una nueva sociedad. Así como en cierto período de la historia universal fueron cayendo los regímenes monárquicos, para dar cabida al nacimiento de las repúblicas porque fueron una necesidad de su tiempo (sin significar esto que el cambio deba ser radical, sino adecuado; como nos lo demuestra el hecho de que haya monarquías adaptadas a los días actuales, que desempeńan un fructífero papel).</p>
          <p>Es por esto, que debe aprovecharse este irrepetible estado, el de una situación que va agonizando el régimen comunista se deshizo en unos meses y "la forma de vida americana" es ya obsoleta-, para ir dando lugar a un nuevo orden con alegría y creatividad. Porque como dice un aforismo: "Si lloras la muerte del sol, las lágrimas no te permitirán ver las estrellas."</p>
          <p>No se le pueden pedir peras al olmo, y tampoco a una sociedad como la actual que tenga otro fruto diferente a la contaminación. Por más que abonemos el árbol, lo podemos y renovemos su tierra, siempre será un olmo.</p>
          <p>Así mientras nuestras estructuras internas no cambien, seguiremos contaminando nuestro alrededor, por más que apliquemos multas a quién arroja deshechos o limitemos la circulación automovilística de acuerdo con el número de terminación de la patente.</p>
          <p>La problemática ambiental no es para nada superficial, sino bien profunda. Tiene que ver con la formación íntima de la persona. Con la educación, que no consiste en llenar a una persona de datos como si se tratase de un contenedor, sino en mejorarla y en hacer que comience a arder de entusiasmo (la palabra entusiasmo proviene del griego y significa " fuego interior " )</p>
          <p>Porque, żsería lógico pretender limpiar el Riachuelo, mientras el canal 16 sigue siendo una cloaca?</p>
          <p>Porque, żpodemos exigirle a alguien que respete la naturaleza cuando no está preparado para respetar su propia naturaleza ni la de sus vecinos?</p>
          <p><img class="img-fluid" src="<?php echo $link; ?>img/ecologia3.jpg"></p>
          <div align="center">
            <p>Otra opinión</p>
          </div>
          <p>Hagamos de cuenta que un hombre considere que el concepto de suciedad es ambiguo, que no sepa distinguir la suciedad de la limpieza, porque piense que es una cuestión relativa al color del cristal con que se mire. Y asi, de esta manera relaje tanto los significados, que hasta las manos de un carbonero le parezcan en cierta medida limpias, a la vez que los pétalos de una rosa silvestre, en cierta medida sucios.</p>
          <p>Y que luego a este hombre en su trabajo, su jefe al olfatear su desagradable olor durante varios días, lo intimase bajo pena de apercibimientos y sanciones a asearse.</p>
          <p>De seguro, este hombre que no sabe bien que significa la limpieza, comenzaría por sentirse agobiado, triste y al cabo desesperado, y luego trataría de mejorar la situación con medidas tales como cambiarse las medias o ponerse una flor en el ojal.</p>
          <p>Su jefe acaso quedase satisfecho por un día, además de complacido por su capacidad de mando.</p>
          <p>Pero al cabo el tufo volvería. Y las advertencias y amenazas de castigo se repetirían durante meses. Y aunque el jefe, quién tampoco tenía muy en claro lo que era el aseo, había hecho un gran esfuerzo, su empleado nunca había llegado a estar verdaderamente limpio.</p>
          <p>Cuando hablamos de ecología y de contaminación, żsabemos a qué nos estamos refiriendo?.</p>
          <p>Ecología es una palabra proveniente del griego que significa "estudio de la casa", de nuestra casa "La Tierra". En primer lugar hablamos de estudio, alguien que estudia aplica su espíritu para comprender; en este caso a la naturaleza, que nos habla a través de su bien elocuente lenguaje. żTenemos los oídos preparados para descifrar sus mensajes?. Y si no somos capaces de conocer nuestra casa, żcómo vamos a cuidarla?. żCómo vamos a amarla?, si no puede amarse sino lo que se conoce. <br><br><br></p>
          <hr width="40%"><br><br>
          <div class="commontext" align="center">
            <p><i>La lanza de Aquiles cerrará las mismas heridas que provoca</i></p>
            <p><i>siempre que la manejen otras manos. Y si la mano que hiere y la que</i></p>
            <p><i>cura es la misma, el tránsito de una acción a la otra revela que algo</i></p>
            <p><i>ha cambiado sustancialmente en el corazón de Aquiles.</i></p>
            <p><b>Massuh "La flecha del tiempo"</b></p>
          </div>
          <br><br><br>
          <hr width="40%"><br><br>
          <div align="center"><b>RIO DE LA PLATA</b>
            <p>CARACTERISTICAS</p>
          </div>
          <p>El Río de la Plata representa la salida de la cuenca del mismo nombre, que abarca parcialmente los territorios de Argentina, Brasil, Uruguay y Paraguay. Es el resultado del aporte de dos grandes ríos, el Paraná con un caudal de los 17.000 m3/s y el Uruguay con un caudal aproximado de unos 5.000 m3/s. y que acarrean una gran cantidad de material en suspensión, lo que confiere a sus aguas un color leonado característico. Otro de sus rasgos salientes es la baja profundidad y la existencia de áreas deposicionales y erosionales denominadas unidades morfológicas, como Playa Honda, Banco Grande de Ortiz, Gran Hoya del Canal Intermedio, Franja Costera Sur, Barra del Indio, etc.</p>
          <p>La escasa profundidad media del río constituye un factor negativo para su autodepuración. Sin embargo, también existen caracteristicas que favorecen dicho proceso: la riqueza y variedad del plancton, su importante caudal, y su gran dinámica. Intervienen en este último factor el régimen de mareas, que transporta cargas de contaminantes ciclicamente aguas arriba y abajo, la acción meteorológica, las diferencias de densidad entre agua dulce y de origen marino y los aportes de los efluente.</p>
          <p>Las caracteristicas de circulación, tanto de las aguas como del material en suspensión son el resultado de la interacción entre la onda de marea oceánica y sus corrientes asociadas. Las corrientes de bajante siguen el curso natural del cauce del río, y las corrientes de creciente siguen la dirección de la onda de marea hacia el Río de la Plata Superior.</p>
          <div align="center">
            <p><b>CONTAMINACION Y EL RIO</b></p>
          </div>
          <p>Las Naciones Unidas a definido la contaminación de las aguas como: <b>"La introducción por el hombre ya sea en forma directa o indirecta, de sustancias o energía dentro del medio cuático que produzca efectos deletéreos, o dańos a los recursos vivos, riesgo a la salud humana, amenaza a las actividades acuáticas incluyendo la pesca, perjuicio o deterioro de la calidad de las aguas y reducción de las actividades recreativas".</b></p>
          <p>&nbsp;</p>
          <div><b><i>żcuando podemos hablar de contaminación y contaminante?</i></b></div>
          Para que podamos entenderlo vamos a un ejemplo sencillo , consideremos el monóxido de carbono (CO), que esta siempre presente en la atmosfera, cuando su concentración excede las 1000 ppm es peligrosamente tóxico y puede causar la muerte; por otra parte se considera que no tienen efectos negativos sobre la salud exposiciones prolongadas a concentraciones que excedan las 10 ppm.
          <p>&nbsp;</p>
          <div><b><i>żQué nos indica esto?</i></b></div>
          Que exista monóxido de carbono donde nos encontramos no significa que haya contaminacón.
          <p>Un elemento o compuesto químico será considerado contaminante según sea su concentración y su masa vertida al cuerpo receptor ( en el tema que no ocupa hoy, es el agua). Para poder hablar por lo tanto de contaminante debemos conocer la concentración de dicha sustancia en ese medio y solo si, su concentración es mayor a los parametros normales se puede decir que ese medio esta contaminado.</p>
          <p>El ingreso de contaminantes se produce a través de cursos de significativa magnitud: el río Reconquista, el Luján, el Riachuelo y una serie de arroyos: Sarandí y Santo Domingo, que canalizan efluentes pluviales, cloacales e indrustriales del conurbano bonaerense. Además existen dos importantes vuelcos cloacales, uno en Berazategui, que reúne los producidos en la Capital Federal y parte del Gran Buenos Aires, y otro en Berisso, donde se evacuan los efluentes de La Plata.</p>
          <p>La contaminación capaz de afectar el sistema fluviomarino pueden ser los siguientes:</p>
          <p>BACTEREOLOGICA: <br>es aquella producida por bacterias, virus, protozoos y hongos que pueden ocasionar enfermedades producidas por organismos patógenos. El indicador biológico de contaminación fecal es el grupo de los Coliformes.</p>
        </div>
      </div>
    </div>
    <!-- /.container -->
    <!-- FOOTER -->
    <?php
    include '../common/footer.php';
    ?>
  </main>
</body>

</html>