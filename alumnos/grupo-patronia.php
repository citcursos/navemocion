<!doctype html>
<html lang="es">

<head>
  <?php
  include '../common/head.php';
  ?>
</head>

<body>
  <header style="height: 100px;">
    <?php
    include '../common/nav.php';
    ?>
  </header>
  <main role="main">
    <div class="container img-header">
      <div class="col-md-12 px-0">
        <h1 class="display-4">Alumnos - Grupo Patronia</h1>
      </div>
    </div>
    <div class="container grey">
      <div itemprop="articleBody">
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <div align="center"><img class="img-fluid" src="<?php echo $link; ?>img/pintura.jpg"></div>
        <p>&nbsp;</p>
        <div class="commontext" align="center"><small>(la pintura que se ve arriba se llama "LA REGATA" y fue realizada por E. MINAMI de "Pintores sin Manos").</small></div>
        <div class="bluetext" align="center">
          <p><b>Un fósforo puede hundir un barco y un tornillo salvarlo, siempre y cuando haya un hombre que los use. Por eso, además de equipar el barco, pongamos máxima atención en la valorización y fomento de las habilidades de los tripulantes.</b></p>
        </div>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <div class="commontext" align="center">
          <div><b>Entusiasmo por la cultura náutica</b> <br>(El Grupo Patronia está formado por alumnos y ex alumnos de la escuela de naútica.No dejes de visitar la página de otro grupo nuestro, muy querido: <a href="http://ar.geocities.com/sharkabukis/" target="_blank" rel="noopener noreferrer">LOS SHARKABUKI</a> ).</div>
          <br><br>
          <hr width="40%"><br><br>
          <div class="commontext" align="center"><b>“Dame quilla y te daré millas”</b></div>
          <br><br>
          <hr width="40%"><br><br>
          <div align="left">“Por más sabio que uno sea, no es nunca vergonzoso escuchar a los otros y ceder a tiempo. Bien ves cómo en la orilla del río el árbol que se encorva salva el ramaje, y en cambio rueda descuajado el que pretende quedarse firme; y cómo el que mantiene tensa la vela en el mar bravío y no afloja, vuelca el barco y el viaje acaba con la quilla al aire.” <br><br><b>ANTÍGONA -Sófocles</b> <br><br>
            <hr width="40%"><br><br> Mi alma contempla el mar, y se asombra ante esa huella de Dios, ante ese signo de lo que no tiene fin. Está bien que te asombres alma, pero ahora escucha el mar es sólo una caricatura tuya- y ˇsalta de gozo!, ˇamada de Dios y eterna! <br><br><b>Esteban Provera</b> <br><br>
            <hr width="40%"><br><br> Hace siglos mi barco descansa sobre arena reseca, hoy comenzó a garuar -por primera vez-, y en mi ser ya arde la gran esperanza de que todo este inmenso desierto se inunde. <br><br><b>Esteban Provera</b> <br><br>
            <hr width="40%"><br><br> Hace mucho que –aunque no lo supiese- quería ser navegante. De chico, como no había barcos disponibles entre mis familiares navegaba en los muelles.¸si, en un muelle de Olivos pescaba horas y horas con una lata, una línea y un trozo de carne cruda . Y eso era como navegar – aunque no lo supiese- , el muelle era el barco con el oleaje y la corriente acariciando el casco de piedra. Y eso era como navegar, como esa noche en el muelle de pilotes de madera de Mar del Tuyú, en que las decenas de remos en movimiento, de pronto se transformaban –al salir del ensueño- en cañas de mediomundo de los pescadores. Pienso que tanto en los muelles como en los veleros, lo que siempre me atrajo irresistiblemente fue –y es- el agua y su voz. <br><br><b>Esteban Provera</b> <br><br><br>
            <div class="bluetext">- <i><b>Provisorio como barco de hielo.</b></i></div>
            <div align="center">
              <p><br><br>Bendita sea la luz <br>y la Santa Veracruz, <br>y el Señor de la verdad, <br>y la Santa Trinidad.</p>
              <p>Bendita sea el alba. <br>y el Señor que nos la manda; <br>bendito sea el día, <br>y el Señor que nos lo envía. Amén</p>
              <p>(Plegaria que recitaban al alba. <br>Cristobal Colón y sus tripulantes <br>en alta mar).</p>
            </div>
            <br><br>
            <hr width="40%"><br><br> <b>"La sonda es al navegante, lo que el bastón al ciego"</b><br> Para navegar hay que tener confianza, vamos a ciegas, sin saber que hay bajo el agua. Nos guiamos por referencias y nos sentimos bastante seguros. Confiamos en la carta náutica, en boyas y puntos notables, en publicaciones y consejos. Y con todo este bagaje nos lanzamos a navegar, sin saber que hay bajo el agua. Sin saber si habrá buen viento, temporal o una calmurria, nos lanzamos a navegar. Revisamos la jarcia, las velas o el motor, y sabemos que no van a fallar cuando haga falta. Y si llega un momento crítico nos acordamos de Dios, "que salga a navegar el que no sepa rezar..." No hay lugar a dudas de que navegar es cosa de creyentes, sino sería tan pesado soltar amarras...
            <p><b>Esteban Provera</b> <br><br></p>
            <hr width="40%"><br><br> "El hombre cuando se embarca debe rezar una vez, cuando va a la guerra dos y cuando se casa, tres"
            <p><b>Antiguo Refrán Español</b></p>
            <p>&nbsp;</p>
            <hr>
            <div align="center">
              <p>2do. Auditorio de náutica ˇGran desafío! <br>Hay que escribir un texto (poesía, cuento o canción) que contenga las <br>siguientes palabras marineras: Garete, Barlovento, Sondaleza, <br>Estribor, Babor y Boyarín <br><span id="cloak7b5b5edf210912d6ee62a7680fb15057"><a class="tablelink" href="mailto:info@navemocion.com">ˇˇMandanos tu trabajo!!</a></span>
                <script type="text/javascript">
                  document.getElementById('cloak7b5b5edf210912d6ee62a7680fb15057').innerHTML = '';
                  var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                  var path = 'hr' + 'ef' + '=';
                  var addy7b5b5edf210912d6ee62a7680fb15057 = '&#105;nf&#111;' + '&#64;';
                  addy7b5b5edf210912d6ee62a7680fb15057 = addy7b5b5edf210912d6ee62a7680fb15057 + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
                  var addy_text7b5b5edf210912d6ee62a7680fb15057 = 'ˇˇM&#97;nd&#97;n&#111;s t&#117; tr&#97;b&#97;j&#111;!!';
                  document.getElementById('cloak7b5b5edf210912d6ee62a7680fb15057').innerHTML += '<a class="tablelink" ' + path + '\'' + prefix + ':' + addy7b5b5edf210912d6ee62a7680fb15057 + '\'>' + addy_text7b5b5edf210912d6ee62a7680fb15057 + '<\/a>';
                </script> <br><p>ˇˇ Leelos y votá el que mas te guste !!</p>
              </p>
            </div>
            <p><br><br><br></p>
            <div align="center"><p>Gran Auditorio de Náutica ! <br>Divertite con estos trabajos de nuestros navegantes y votá el que más te guste…</p> <br><br><br><br><img class="img-fluid" src="<?php echo $link; ?>img/dibu.jpg">
              <p><b>Dibujo de Ileana Klein (integrante del Staff de Dibu) ˇMuchas Gracias!</b> <br><br></p>
              <hr width="40%"><br><br>
            </div>
            <div class="bluetext" align="center">
              <h3>Aprendé del Arca de Noé</h3>
            </div>
            <div class="bluetext" align="left">
              <p><b><i>Primero:</i></b> <br>No pierdas el barco.</p>
              <p><b><i>Segundo:</i></b> <br>Recuerda todos estamos en el mismo barco.</p>
              <p><b><i>Tercero:</i></b> <br>Anticípate, No llovía cuando Noe comenzó a construir el arca.</p>
              <p><b><i>Cuarto:</i></b> <br>Mantente en forma, cuando tengas 60 años alguien puede pedirte que hagas algo grande.</p>
              <p><b><i>Quinto:</i></b> <br>No oigas las criticas, solo dedícate al trabajo que se tiene que realizar.</p>
              <p><b><i>Sexto:</i></b> <br>Construye tu futuro en tierras altas.</p>
              <p><b><i>Séptimo:</i></b> <br>Por seguridad, viaja en pareja.</p>
              <p><b><i>Octavo:</i></b> <br>La velocidad no es siempre una ventaja, los caracoles estaban a bordo junto con los cheetas.</p>
              <p><b><i>Noveno:</i></b> <br>Cuando estés estresado, flota un momento.</p>
              <p><b><i>Décimo:</i></b> <br>Recuerda que el arca fue construida por amateurs, el Titanic por profesionales.</p>
              <p><b><i>Onceavo:</i></b> <br>No importa la tormenta, cuando estas con Dios, siempre hay un Arco Iris esperando.... y Dios ... es amigos, amor, esperanza sin importar que religión... incluyo aquellos que no tengan religión ...</p>
            </div>
            <p><br><br></p>
            <hr width="40%">
            <p><br><br></p>
            <div class="commontext"><b>"Un velero es tan lento que en vez de ser una forma de viajar, es una forma de vida."</b>
              <p>Esteban Provera <br><br></p>
              <hr width="40%"><br><br> <b>"La mar es una señora que debemos tratar con sumo respeto".</b>
              <p>Antiguo Refrán Español <br><br></p>
              <hr width="40%"><br><br> <b>EL RIO</b> <br>Este es el verdadero río, amor soñado. Tú estás en la nada y yo en la vida. Sólo tiempo que corre entre los dos. Las dos orillas son iguales, pero una apenas se ve de la otra, ahora que el río es tan ancho. Pero el río se irá estrechando. Y yo te veré , amor soñado, y yo pasaré , y me quedaré contigo para siempre. <br><small><i>Juan Ramón Jiménez</i></small> <br><br>
              <hr width="40%"><br><br> Descubierta la dirección de donde viene el río del tiempo, los arquitectos dieron a su ciudad la forma de un barco; con una afilada quilla que hiende los siglos y le permite seguir allí flotando proa a la corriente, intacta, a salvo del deterioro y del cambio.
              <p>Descubierta la manera de observar el paso del tiempo, se ha detectado que río arriba flota una enorme burbuja con forma de barco: un barco que va quedando allá atrás y al que ya nadie podrá llegar.</p>
              <p><i>Marcial Souto</i> <br><br></p>
              <hr width="40%"><br><br> Hay entrega de diplomas a los nuevos Pilotos de Yate. Se encuentran todos los que recibirán sus habilitaciones y sus familiares. Se encuentra un reconocido piloto navegante de muchos mares, que además ha escrito libros y artículos, es anciano pero de aire juvenil, pelo blanco y barba blanca en un rostro cobrizo. Se lo puede observar sentado, seguro de sí mismo, algo nervioso.
              <p>A su debido momento, es invitado a pararse para entregar uno de los títulos. Debajo de su barba blanca, su piel rojiza alcanzó a sonrrojarse y su mirada estaba perdida. "La fuerza y el futuro es de los jóvenes", pensaba. <br><br></p>
              <hr width="40%"><br><br> <b>El marinero es franqueza, el hombre de tierra es sutileza.</b>
              <p><i>Herman Melville</i> <br><br></p>
              <hr width="40%"><br><br> <b>Agua buena, vida buena; agua mala, mala vida.</b>
              <p><b>Sin agua no hay vida.</b></p>
              <p><i>Sir Peter Blake</i> <br><br></p>
              <hr width="40%"><br><br> - Me preguntas en qué latitud y longitud me encuentro; no tengo ni idea de qué es longitud y latitud, pero son dos palabras fantásticas.
              <p><i>Lewis Carrol. "Alicia en el país de las maravillas".</i> <br><br></p>
              <hr width="40%"><br><br> - En tierra sólo hay problemas.
              <p><i>D. Haeften. Cómo afrontar los temporales</i> <br><br></p>
              <hr width="40%"><br><br> <b>Dicen que para navegar a vela hay que tener paciencia. Al contrario, porque en un velero no hay nada que esperar, uno ya disfruta por navegar aunque el puerto esté lejos.</b>
              <p><i>Gabriel (grupo Patronia)</i> <br><br></p>
              <hr width="40%"><br><br> -Las olas nos ocultan los errores de los pilotos, así como la tierra cubre las equivocaciones de los médicos.
              <p><i>F. Arago</i> <br><br></p>
              <hr width="40%"><br><br> Pablo: -ˇEs ancho en serio el Río de la Plata! <br>Carlos: -El mundo es más ancho
              <p><i>(Diálogo entre los recién recibidos como timoneles P. Giraudi y C. Picazo durante su primer cruce a Colonia)</i> <br><br></p>
              <hr width="40%"><br><br>
            </div>
            <div class="bluetext" align="center"><br>Cierro todo y borro el centro, <br>Solo pa´ tener mil caras.. <br>En el puerto suelto amarras <br>Y me voy velero río adentro.
              <p>Pierdo todo en el fondo <br>Y en el horizonte entero <br>Cuando hay tiempo fulero, <br>Se siente el dolor más hondo.</p>
              <p>En una Pampa mojada, <br>Jirones a la deriva. <br>El Sol, la Luna, día a día <br>Barreno si el viento calla.</p>
              <p>Entre cabos, entre vientos, <br>Timoneando mis mareas, <br>Navegando borro penas, <br>Problemas y sufrimientos.</p>
              <p>Evitando a las sirenas, <br>Lo mismo da ir al Oeste, <br>Al Norte, al Sur o al Este. <br>Y sobre mí, sólo las estrellas.</p>
              <div align="right"><b>Tao</b></div>
              <div align="right">&nbsp;</div>
              <div align="right">
                <hr>
              </div>
              <div align="right">
                <div class="commontext" align="center"><b>REGATA DE LA EMOCIÓN</b>
                  <p>(VA A HABER BORDES Y VIRADAS PERO EL QUE LLEVA LA CAÑA ES EL CORAZÓN)</p>
                  <p>Aquí van las primeras estrofas de la tripulación del velero "Kabuki"</p>
                  <p>(Ejercicio: la consigna era redactar una poesía , narración, canción , etc. aplicando los siguientes términos náuticos: amura – estribor – stay – obenque – serreta – cuaderna – quillote) <br><br></p>
                  <hr width="40%"><br><br> Todo parece un viaje <br>con amura a estribor <br>No es tan sencilla la vida <br>ni la navegación
                  <p>Gracias a los obenques y stays <br>permanecí firme como un mastil, <br>sino fuera por el quillote <br>más de una vez hubiera zozobrado, <br>pero aún mantengo el equilibrio.</p>
                  <p>Más de una tormenta <br>quiso herir el alma de esta embarcación <br>las cuadernas me protegieron <br>y cuanto más golpeaban las olas.. <br>las serretas más resistencia ofrecían <br>cuidando al corazón <br>de esta noble embarcación.</p>
                  <p><b>Kabuki</b> <br><br></p>
                  <hr width="40%"><br><br> <b>SE ACERCA OTRA TRIPULACIÓN (DEL VELERO BELÉN):</b>
                  <p>Amurado a mi corazón, <br>los sentimientos escritos alguna vez <br>en aquel libro, que estay guardado <br>a estribor de aquel viejo quillote, <br>a la deriva pienso en ti, <br>apoyado sobre el obenque, <br>perpendicular al sol, como las serretas a las cuadernas. <br>Nunca olvidaré los viajes realizados en ti, <br>mi viejo barco.</p>
                  <p>Belén <br><br></p>
                  <hr width="40%"><br><br> <b>AQUÍ VA OTRA (la consigna era emplear las siguientes palabras del vocabulario marinero: quillote, escorado, carlinga, estribor, barlovento, escota)</b>
                  <p><b>Desde el Kabuki</b></p>
                  <p>Con el quillote sujeto al fondo <br>soportaba la deriva, <br>que un fuerte viento de mayo <br>dejó mi barco escorado y mi alma herida. <br>En una virada en avante <br>Y con maniobra en ceñida <br>Los alcahuetes me indican <br>Que buenos vientos se avecinan <br>Bien calzada en empopada <br>Fluye mi sueño e ilusión y <br>Confiada en mi firme carlinga <br>Doy rienda suelta a mi amor <br>Pero nada parejo está el tiempo <br>Y aunque tenía amuras de estribor <br>Quedo mal parada a barlovento <br>En medio de este fugaz esplendor <br>Después del inicial desconcierto <br>Recobro firme el timón <br>Filando segura de la escota, <br>Soportando el sinsabor <br>Menos mal que la mía <br>Es embarcación entera <br>Y que quilla y cuadernas <br>Son de fuerte madera <br>Y aunque la ilusión fue placentera <br>Y la orilla del amor no alcancé <br>Tengo vela y motor duradero <br>Para seguir la travesía <br>y aunque era grande mi osadía <br>esa vez me enamoré.</p>
                  <p><b>Liliana Guarnaccia del Kabuki</b> <br><br></p>
                  <hr width="40%"><br><br> <b>AHORA LE TOCA EL TURNO A LA TRIPULACIÓN DE FLIPPER: </b>
                  <p><b>La consigna era redactar una poesía , narración, canción , etc. aplicando los siguientes términos náuticos: amura – estribor – stay – obenque – serreta – cuaderna – quillote</b></p>
                  <p>"El Reflejo"</p>
                  <p>Que amanecer tan bello <br>tu rostro reflejado en el agua azul de estribor. <br>Nuestras almas unidas <br>en la trabajosa tarea de navegar. <br>Stays, obenques marcan la amura <br>que nos lleva a un maravilloso y placentero paseo. <br>Aun sin conocernos <br>conoceremos el rumbo de nuestra embarcación. <br>El sol apenas se esconde <br>en su llano horizonte. <br>Sentimos crujir la serreta con sus cuadernas <br>por debajo de cubierta. <br>Tu pelo largo, negro y brilloso <br>marca hacia la aleta. <br>Quillote, indica el calado de aguas profundas. <br>Y allá, no muy lejos de proa <br>un pequeño y agil delfín <br>que lo hemos de bautizar "Flipper". <br>Nos guía, a donde, quien sabe <br>solo Dios <br>marcará nuestro destino.</p>
                  <p><b>FLIPPER</b> <br><br></p>
                  <hr width="40%"><br><br> <b>HACE SU ENTRADA LA PAYADORA DE "KABUKI": </b>
                  <p><b>(la consigna era redactar una poesía , narración, canción , etc. aplicando los siguientes términos náuticos: amura – estribor – stay – obenque – serreta – cuaderna – quillote)</b></p>
                  <p>Esta rima campechana quiere arrimar al amigo <br>algun consejo sencillo para moverse en el Rio. <br>Siempre que quiera pasar ... amurese al estribor <br>obenque tirante a derecha... con la vela hacia babor... <br>No me diga nada ahora, se que mucho no aclaré <br>estribor es la derecha mirando para proel... <br>Si se viene complicando no se preocupe paisano <br>proel es para adelante y quillote lo de abajo. <br>Si quiere sigo explicando y le tiro un par de datos <br>quillote es pieza importante pues le da equilibrio al barco. <br>Y siguiendo por abajo aparecen las cuadernas <br>Una especie de costillas que lo vienen reforzando <br>Pa´seguir desasnando le presento la serreta <br>Un tirante largo y fino que va uniendo cuadernas. <br>Cuando hablamos de un obenque no piense en una montura <br>son unos cables de acero que le definen amura. <br>Pa´terminar no quiero olvidarme del stay <br>que si bien le suene a gringo es otro cable de atar... <br>similares a los obenques pero en vez a los costados <br>se agarra firme y parejo de las puntas de los barcos. <br>No quiero irme sin antes poderle recomendar <br>Aprender es importante si se quiere navegar <br>Instructor y amigos nuevos ... una forma de empezar <br>Y un velero bien gauchito que te pueda bien llevar... <br>Lluvia, viento y granizada ... sol, fatura y matecito <br>Son algunos sacrificios si queres un viajecito <br>Y me despido mi viejo con un dicho pajarero <br>Si salis de cualquier puerto contratate un marinero.</p>
                </div>
                <p>&nbsp;</p>
                <div style="text-align: center;" align="right">El Paturuzù Nautico.</div>
                <p style="text-align: center;">&nbsp;</p>
                <div style="text-align: center;" align="right">FINALMENTE GANA LA REGATA EL VELERO "KABUKI" <br>PERO "FLIPPER" Y "BELÉN" żACASO NO TRIUNFARON?</div>
                <p style="text-align: center;"><br><br><b><i>EL EJERCICIO CONSISTÍA EN ESCRIBIR UNA POESÍA O NARRACIÓN, UTILIZANDO LAS SIGUIENTES PALABRAS: FOGONADURA, ESCOTA, DRIZA, BOTAVARA, CORNAMUSA, BALUMA Y SERRETA. VEAN QUE BIEN SE DEFENDIERON ESTOS TRIPULANTES DEL CURSO DE TIMONEL DE LA UNIVERSIDAD DE BUENOS AIRES:</i></b></p>
                <p style="text-align: center;">Coplas del principiante <br>(de humor)</p>
                <p style="text-align: center;">Detrás de aquel cirro <br>Venía caminando <br>Esperando música de lanzamiento <br>Y me encontré con obra muerta <br>Y palo seco <br>Me mostraste tu buzarda <br>Y me agarré al mancebo <br>Mostraste fogonadura <br>Y agarré la carlinga <br>Quedé desventado <br>No así preocupado <br>Ahí nomás cazé genoa <br>Y orzé por avante. <br>Dos rizos mayor <br>Cambiaron el rumbo <br>ˇdejándome en cubierta <br>secándome al sol! <br>Timón a la vía <br>Crujía mi corazón.</p>
                <p style="text-align: center;">&nbsp;</p>
                <div style="text-align: center;" align="left"><b>FABIÁN SANTORO</b></div>
                <p style="text-align: center;">Amanece el nuevo día y la suave brisa matinal me invita a navegar, sin dudar preparo la embarcación, la cual, espera recibir la caricia del viento para que la lleve sin derrotero fijo al lugar que elija mi destino. <br>Salgo de puerto dejando atrás la ciudad, el velero colocado ya de ceñida al viento espera que tome la driza para así elevar la vela mayor, y al hacerlo, la embarcación es empujada por el viento que acompaña mi derrotero. <br>Navegando por este río calmo y sin los sonidos característicos de mi ciudad, me relajo. <br>De pronto, y sin tener conciencia del paso del tiempo me doy cuenta, el cielo comienza a obscurecerse y el viento a dejar de acompañarnos. Es el momento de colocar el genoa, tomo la escota y tiro con fuerza hasta lograr desplegarla. <br>Aprovechando el poco viento pero sabiendo que pronto, el cielo amenazante de lluvias descargará la necesaria frescura para esta mañana gris. <br>Navego con la embarcación amurada a estribor y mi botavara a babor. <br>Caen ya las primeras gotas, las observo como serpentean por el mástil hasta llegar hasta la fogonadura. <br>Mala suerte mi destino, un tronco a la deriva golpea mi embarcación, me dirijo a estribor, gran sorpresa lleve al ver un pequeño rumbo por el cual se podía observar una de las serretas. <br>Con rapidez me muevo logrando colocar un pallete de colisión, tomando sus cabos en las cornamusas de estribor. <br>Y así continúo mi derrota de regreso a casa, para volver en otro momento tal vez, a disfrutar como siempre del inmenso placer de navegar.</p>
                <p style="text-align: center;">&nbsp;</p>
                <div style="text-align: center;" align="left"><b>PABLO PETIX</b></div>
                <p style="text-align: center;">Quisiera ser libre con tu ayuda <br>Pues sigo arraigada a tu fogonadura. <br>Fortalezco mis escotas, <br>Pero la driza me retiene <br>La botavara no me suelta, <br>La cornamusa tensa, y no cesa.</p>
                <p style="text-align: center;">Sueño con ser amiga del viento, <br>Que me acaricie su movimiento, <br>Y a no oponer resistencia, <br>Que mi baluma vuele alto <br>Y se despida por fin <br>De tu serreta y tu barco.</p>
                <p style="text-align: center;">&nbsp;</p>
                <div style="text-align: center;" align="left"><b>ADA SUAREZ</b></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container -->
    <!-- FOOTER -->
    <?php
    include '../common/footer.php';
    ?>
  </main>
</body>

</html>