<!doctype html>
<html lang="es">

<head>
	<?php
	include '../../common/head.php';
	?>
</head>

<body>
	<main role="main">
		<div class="container grey">
			<div class="col-md-12 px-0 text-center">
				<img src="<?php echo $link; ?>img/logonav.png" style="max-width: 320px;" class="img-fluid" alt="Navemoción">
				<?php
				if ($_POST['login']) {
					if ($_POST['login'] == "amura") {
						header("Location: uba2_ar_popup.php");
						exit;
					} else {
						echo '<div class="alert alert-danger" role="alert">La clave es incorrecta</div>';
					}
				}
				?>
				<form class="form-inline justify-content-center" method="POST" action="<?php echo $link; ?>alumnos/login/uba2_popup.php" role="form">
					<div class="form-group mx-sm-3 mb-2">
						<label for="inputPassword2" class="sr-only">Ingresa la clave:</label>
						<input type="password" class="form-control" name="login" placeholder="Password">
					</div>
					<button type="submit" class="btn btn-primary mb-2">Entrar</button>
				</form>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
			</div>
		</div>
		<!-- /.container -->
		<!-- FOOTER -->
	</main>
</body>

</html>
