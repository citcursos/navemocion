<!doctype html>
<html lang="es">

<head>
	<?php
	include '../../common/head.php';
	?>
	<title>Bajar archivos</title>
</head>

<body>
	<main role="main">
		<div class="container grey">
			<div class="col-md-12 px-0 text-center">
				<img src="<?php echo $link; ?>img/logonav.png" style="max-width: 320px;" class="img-fluid" alt="Navemoción">

				<h4>Curso de Meteorolog&iacute;a</h4>
				<p><a href="<?php echo $link; ?>files/nieblas.rar">
					<img src="<?php echo $link; ?>img/file_word.jpg" border="0">
				</a>
				</p>
				<p>Nieblas.ppt</p>
				<span style="color: red"><b>Importante:</b></span> Los archivos est&aacute;n en formato .ZIP. Si no lo ten&eacute;s, bajate el <a href="http://winzip.com" target="_blank">Winzip</a> aqu&iacute;.

			</div>
		</div>
		<!-- /.container -->
		<!-- FOOTER -->
	</main>
</body>

</html>