<!doctype html>
<html lang="es">

<head>
	<?php
	include '../../common/head.php';
	?>
	<title>Bajar archivos</title>
</head>
<body>
	<main role="main">
		<div class="container grey">
			<div class="col-md-12 px-0 text-center">
				<img src="<?php echo $link; ?>img/logonav.png" style="max-width: 320px;" class="img-fluid" alt="Navemoción">

				<h4>Curso de Timonel</h4>
				<div class="row">
					<div class="col-4">
						<a href="<?php echo $link; ?>files/nomenclatura1.rar">
							<img src="<?php echo $link; ?>img/file_word.jpg" border="0">
						</a>
						<p>1.1 Nomenclatura Marinera</p>
					</div>
					<div class="col-4">
						<a href="<?php echo $link; ?>files/1.1.1velasymaniobras.rar">
							<img src="<?php echo $link; ?>img/file_word.jpg" border="0">
						</a>
						<p>1.1 b. y c <br>Velas y Maniobras</p>
					</div>
					<div class="col-4">
						<a href="<?php echo $link; ?>files/2.Reglamentaci&oacute;n.rar">
							<img src="<?php echo $link; ?>img/file_word.jpg" border="0">
						</a>
						<p>2.Reglamentaci&oacute;n.rar</p>
					</div>
					<div class="col-4">
						<a href="<?php echo $link; ?>files/1.2 Meteorolog&iacute;a.rar">
							<img src="<?php echo $link; ?>img/file_word.jpg" border="0">
						</a>
						<p>1.2 Meteorolog&iacute;a</p>
					</div>
					<div class="col-4">
						<a href="<?php echo $link; ?>files/vuelta_escota.zip">
							<img src="<?php echo $link; ?>img/file_quicktime.jpg" border="0"></a>
						<p>Vuelta de <br>escota.qt</p>
					</div>
				</div>
				<span style="color: red"><b>Importante:</b></span> Los archivos est&aacute;n en formato .ZIP. Si no lo ten&eacute;s, bajate el <a href="http://winzip.com" target="_blank">Winzip</a> aqu&iacute;.
			</div>
		</div>
		<!-- /.container -->
		<!-- FOOTER -->
	</main>
</body>

</html>