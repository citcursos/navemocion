<!doctype html>
<html lang="es">

<head>
  <?php
  include '../common/head.php';
  ?>
</head>

<body>
  <header style="height: 100px;">
    <?php
    include '../common/nav.php';
    ?>
  </header>
  <main role="main">
    <div class="container img-header">
      <div class="col-md-12 px-0">
        <h1 class="display-4">Alumnos - Mano de Risas</h1>
      </div>
    </div>
    <div class="container grey text-center">
      <div itemprop="articleBody">
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>
          <img class="img-fluid" src="<?php echo $link; ?>img/clip_image030.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image0030.gif" width="585" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image0029.gif" width="585" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image0026.jpg" width="585" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image0027.jpg" width="585" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image0028.jpg" width="585" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image0025.gif" width="585" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image0024.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image0023.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image0022.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image0021.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image0020.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image0019.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image0018.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image0017.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image0016.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image0015.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image011.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image012.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/clip_image013.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/risa28.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/risa25.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/risa26.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/risa27.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/mano1.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/mano2.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/mano3.gif" border="0"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/risas_n_1.jpg"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/risas_n_2.jpg"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/risas_n_3.jpg"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/risas_n_4.jpg"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/risas_n_5.jpg"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/risas_n_6.jpg"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/risas_n_7.jpg"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/risas_n_8.jpg"><br><br><img class="img-fluid" src="<?php echo $link; ?>img/risas_n_9.jpg"><br><br><br><img class="img-fluid" src="<?php echo $link; ?>img/risas_n_10.jpg"></p>
        <div class="commontext">
          <p>Navega una galera romana con 200 galeotes y de pronto les dice el capitán:&nbsp;<br>- Mis estimados galeotes, os informo que después de 2 años y 312 días de navegación os voy a dar 2 noticias, una buena y otra mala.&nbsp;<br>Todos los galeotes gritaban:&nbsp;<br>- ¡La buena, la buena, capitán!&nbsp;<br>Entonces les dice el capitán:&nbsp;<br>- La buena es que al fin os vais a poder cambiar los calzoncillos. Y la mala es que, tú Augustus te los cambias con Octavius, tú Marcus con Alfonsus, tú Micaelus con Porfidus...</p>
          <p>&nbsp;</p>
          <div class="commontext">Una vez un marinero fue donde su capitán muy asustado gritando: <br>- ˇCapitán, capitán, nos hundimos! <br>El capitán le contesta: <br>- Cálmate chico este es un submarino. <br><br><br>
            <hr width="40%"><br><br>
            <p>"Aunque el gobierno de ese entonces hizo todo lo posible por ocultar esta noticia, en el año 1992 se celebró una competencia de remeros entre dos equipos, uno compuesto por trabajadores de una empresa argentina privatizada y el otro compuesto por sus colegas de una similar japonesa. <br>Apenas fue dada la orden de partida, los remeros japoneses comenzaron a destacarse. Llegaron a la meta prontamente. El equipo argentino lo hizo una hora después. De regreso a nuestro país, la Dirección de la empresa se reunió para analizar las causas del desconcertante y a la vez imprevisto resultado. Se arribó a la siguiente conclusión: <br>En el equipo japonés había un Jefe de equipo y diez remeros, mientras que en el equipo argentino había un remero y diez jefes de equipo. Se decidió entonces adoptar las medidas pertinentes para que en la competencia del año siguiente no sucediera lo mismo.</p>
            <p>En 1995, apenas producida la largada, desde la primera remada el equipo japonés volvió a tomar la delantera. El equipo argentino llegó esta vez dos horas después. La Dirección volvió a reunirse luego del nuevo revés y tras estudiar lo acontecido, comprobó que: <br>En el equipo japonés había un jefe de equipo y diez remeros, mientras que en el equipo argentino, luego de un intenso y meditado estudio realizado el año anterior, estaba compuesto por un Jefe de equipo, dos asesores de gerencia, siete jefes de sección y un remero. Por lo cual, luego de un minucioso análisis, se llegó a la conclusión unánime: "el remero es un incompetente".</p>
            <p>En 1996 el equipo japonés escapó de salida. La tripulación argentina, cuya composición había esta vez sido encomendada al "Departamento de Nuevas Tecnologías", llegó tres horas más tarde.</p>
            <p>A los efectos de tomar al toro por las astas se llevó a cabo una importante reunión en el salón VIP de la compañía, ubicado en el cuarto piso de la casa matriz, esta vez con la presencia del Staff completo, con concurrencia obligatoria so pena de ser severamente sancionados, en donde se llegó a la siguiente conclusión: Posiblemente para desconcertar a nuestro equipo, en esta ocasión el equipo japonés optó por la formación tradicional de un jefe de equipo y diez remeros.</p>
            <p>El equipo argentino, que había sido conformado según el informe de una prestigiosa Consultora Internacional con sede en la ciudad de Nueva York, prefirió optar por una formación vanguardista integrada por un jefe de equipo, dos auditores de la consultora antes mencionada, un asesor en Calidad Total y cuatro Controllers Administrativos que no quitaban el ojo al único remero a quien habían amonestado y castigado severamente quitándole todos los plus e incentivos por los fracasos anteriores.</p>
            <p>Luego de varios días de reuniones realizadas en un famoso spa de Punta del Este, se concluyó que debía recurrirse a la contratación de un nuevo remero, pero a través de un contrato de tercerización a los efectos de no tener que lidiar con el sindicato y los piqueteros, y no estar atados a convenios laborales que atrofian la eficacia y degradan la productividad."</p>
          </div>
          <p><br><br></p>
          <div align="center"><img class="img-fluid" src="<?php echo $link; ?>img/risas_n_11.jpg" align="middle"> <br><br> <img class="img-fluid" src="<?php echo $link; ?>img/risas_n_12.jpg" align="middle"></div>
          <p><br><br></p>
          <div class="commontext">
            <p>żEn qué se parecen las mujeres a los huracanes? <br>En que llegan con fuerza y salvajemente, y se van con tu coche y tu casa <br><br><br></p>
            <hr width="40%"><br><br> Cuatro hombres casados van a pescar. Después de una hora, se escucha esta conversación: <br>HOMBRE 1: "No tienen idea lo que tuve que hacer para venir a pescar este fin de semana. ˇTuve que prometer a mi señora que pintaría toda la casa el próximo fin de semana!" <br>HOMBRE 2: "Eso no es nada. ˇYo tuve que prometer que le construiría una terraza nueva para la piscina, con quincho y todo! <br>HOMBRE 3: "ˇA ustedes les toca fácil! Yo tuve que prometerle que remodelaría la cocina completa, incluyendo el cambio de todos los electrodomésticos" <br>Y así, continúan pescando hasta que se dan cuenta que el cuarto amigo no había dicho una palabra, así que deciden preguntarle: <br>"Y tu no has dicho nada acerca de lo que tuviste que hacer para venir a pescar este fin de semana. żNo tuviste que prometer nada? <br>HOMBRE 4: "Yo puse el despertador a las 4:30 A.M. y cuando sonó, me acerqué al oído de mi mujer y le dije: żpesca o sexo?" <br>...y ella me contestó: "Abrígate bien..." <br><br>
            <hr width="40%"><br><br> Esposo desde la cubierta a la esposa que esta en la cabina: <br>ˇˇˇMi amor!!!...ˇˇˇmi amor!!!...ˇˇhace media hora que se cayó tu madre al agua!! <br>- żY por qué me avisas ahora? <br>- Es por que no podía hablar de la risa. <br><br>
            <hr width="40%"><br><br> Respuesta en un examen oral de náutica: <br>żCuántas anclas tiene un barco? <br>Esteeee, mmmm, ˇOnce! <br>żQué? żCómo que once? <br>ˇClaro! żNo ve que siempre dicen: Eleven anclas? <br><br>
            <hr width="40%"><br><br> żPor qué un naufrago se pone a llorar y gritar en el medio del océano? <br>Para desahogarse <br><br>
            <hr width="40%"><br><br> Resulta que un caníbal viajaba en un barco de pasajeros, y a la hora de almorzar, se le acerca un mozo y le pregunta: <br>żLe traigo la carta del menú? <br>No, mejor tráigame la lista de pasajeros. <br><br>
            <hr width="40%"><br><br> Cuentan las malas lenguas que lo relatado a continuación ocurrió en un examen oral de maniobra. <br>- Profesor: Imagínese que está al pairo esperando para entrar en puerto, y se levanta un fuerte viento de la mar hacia tierra. żQué haría usted si no puede arrancar la máquina? <br>- Alumno: Fondearía <br>- Profesor: żY si el viento arrecia? <br>- Alumno: Fondearía con otra ancla <br>- Profesor: żY si continúa refrescando? <br>- Alumno: Mandaría fondear otra ancla. <br>- Profesor: żDe donde saca usted tantas anclas? <br>- Alumno: ˇˇˇDel mismo sitio que usted el viento!!!!. <br><br>
            <hr width="40%"><br><br> Corre el rumor de que los barcos de última generación estarán tan automatizados que solo necesitarán de un Capitán, para echarle las culpas, y un perro, para que el Capitán no se acerque a la computadora. <br><br>
            <hr width="40%"><br><br> Carlitos siempre llegaba tarde a la escuela y un día la maestra lo castigó haciendo que conjugara un verbo. La maestra le indicó conjugar el verbo nadar. Carlitos empieza en voz muy alta: "él nada, tú nadas, ellos nadan", y la maestra le dice: "más bajito Carlitos, más bajito" y Carlitos dice: "él bucea, tú buceas, ellos bucean." El capitán de un barco que se está hundiendo grita desesperadamente: <br>"żHay alguien a bordo creyente y que sepa rezar con devoción?" <br>"Sí, yo", responde un pasajero. <br>"ˇPerfecto, porque nos falta un chaleco salvavidas!" <br><br>
            <hr width="40%"><br><br> En la barra de una Cantina caribeña se encuentra un mozo y un veterano pirata tomando unos cacharros. El joven observa que el pirata es prácticamente un tullido, con su pata de palo, garfio y parche en el ojo. Impresionado, le pregunta donde perdió la pierna; el pirata le cuenta un feroz abordaje a un barco de su graciosa majestad donde un cañonazo se llevo por delante, su pierna. La curiosidad puede más que el mozo y vuelve a inquirir al viejo pirata sobre su mano; éste le cuenta que en otro abordaje, un español se la arrebató con un mandoble de espada cuando el barco estaba casi rendido. El mozo impresionado, no sale de su asombro y le pide que le cuente como perdió el ojo. El pirata hace un silencio, y dice: <br>-Eso fue culpa de una mosca al día siguiente de perder la mano.
          </div>
          <div align="right">&nbsp;</div>
          <div style="text-align: center;" align="right">
            <hr>
          </div>
          <div style="text-align: center;" align="right">&nbsp;</div>
          <div style="text-align: center;" align="right"><br> <img class="img-fluid" src="<?php echo $link; ?>img/risas_n_13.jpg"><br><br> <img class="img-fluid" src="<?php echo $link; ?>img/risas_n_14.jpg"><br><br> <img class="img-fluid" src="<?php echo $link; ?>img/risas_n_15.jpg"><br><br> <br>
            <div class="bluetext" align="center">NUEVOS DESCUBRIMIENTOS SOBRE LA LÍNEA DE CRUJÍA</div>
            <p>&nbsp;</p>
            <div class="commontext">Como sabemos, crujía es la línea imaginaria que recorre el centro de una embarcación de proa a popa. Se ha comprobado que esta línea tiene fuerte influencia sobre el viento, ya que cualquier viento, sea de la dirección que sople, termina corriendo de proa a popa (por lo que sugerimos que de ahora en más, ya que es imaginaria, todos nos imaginemos a la línea de crujía de través a través.) <br>Por otra parte, el origen del nombre de esta línea -crujía- , provendría de los lastímeros crujidos que dejan oir los exhaustos veleros tratando de orzar cuarenta grados contra el viento, con parecido grado de escora. <br><i><b>Esteban Provera</b></i></div>
            <img class="img-fluid" src="<?php echo $link; ?>img/risas_n_16.jpg"><br><br> <img class="img-fluid" src="<?php echo $link; ?>img/risas_n_17.jpg"><br><br> <img class="img-fluid" src="<?php echo $link; ?>img/risas_n_18.jpg"><br><br>
            <div align="center"><img class="img-fluid" src="<?php echo $link; ?>img/risas_n_19.jpg"><br><br> <img class="img-fluid" src="<?php echo $link; ?>img/risas_n_20.jpg"></div>
            <br><img class="img-fluid" src="<?php echo $link; ?>img/risas_n_21.jpg"><br><br> <img class="img-fluid" src="<?php echo $link; ?>img/risas_n_22.jpg"> <br><br>
            <div class="bluetext">NAVEGANTE DEL FUTURO, NOS ADELANTA UNA NOVEDAD ASOMBROSA</div>
            <p>&nbsp;</p>
            <div class="commontext">Navego en un velero blindado, su rumbo actual para ser exactos es de 124° 22.809´, la velocidad de 5,12 nudos, la temperatura del agua de 18°, y pasa un bagre de 16 centímetros bajo mi quilla. El ambiente en esta mañana está muy agradable, ya que está funcionando el aire acondicionado. <br>Desde la computadora está todo organizado, hasta las velas se van ajustando para aprovechar en forma óptima el viento. A mí, me están reservadas tareas como escuchar a través de internet la lectura de los diarios de mi ciudad, Buenos Aires, hacer compras (para cuando regrese), o ver programas de televisión. <br>Pero lo que en realidad, quería comentarles, es una excelente noticia aparecida en un sitio dedicado a la navegación. Parece ser, que en unos dos años, una empresa noruega, estará en condiciones de lanzar al mercado, unas cartas náuticas de cada sector del mundo en papel (como si fuesen una enorme y exacta impresión de la carta que uno puede ver en una pantalla). Las ventajas de estas cartas son muchas, como lo hace notar la misma empresa. Uno puede usarlas en donde sea, incluso echado en la cucheta, hacer dos tracitos con un lápiz y una regla, y apoyando el dedo índice decir seguro: ˇEstoy acá! (ˇqué placer, si!), y luego dirigir uno mismo el barco a un puerto, a un costo bajísimo, sin virus, sin que se cuelgue el programa o se agoten las baterías. Cartas de papel sí, que buena idea, ˇbellas cartas de papel, que uno puede acariciar o encuadrar si quiere! <br>Cartas de papel, reglas, lápiz, y un capitán a bordo ˇqué buena idea!, ˇqué placer más noble!, ˇejercicio para el alma, para el cuerpo y la mente! <br>
              <div align="right">Esteban Provera</div>
            </div>
            <br><br> <img class="img-fluid" src="<?php echo $link; ?>img/risas_n_23.jpg">&nbsp;
          </div>
        </div>
      </div>
    </div>
    <!-- /.container -->
    <!-- FOOTER -->
    <?php
    include '../common/footer.php';
    ?>
  </main>
</body>

</html>