<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Alumnos - Artículos</h1>
        </div>
      </div>
        <div class="container grey">
        <div itemprop="articleBody">

<p>&nbsp;</p>
<div align="left" class="bluetext">
<p>&nbsp;</p>

<p><b>Temas varios</b></p>

<ul>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/31-articulos-caballeria-naval">Caballería Naval</a>&nbsp;<span style="color: #ff0000;">(Nuevo)</span></li>
	<li><span style="color: #ff0000;"><a href="/index.php/alumnos/articulos/14-alumnos/articulos/33-no-tan-a-la-deriva">(No tan) a la deriva</a></span></li>
	<li><span style="color: #ff0000;"><a href="/index.php/alumnos/articulos/14-alumnos/articulos/34-un-huracan-sin-nombre">Un Huracán sin nombre</a></span></li>
	<li><span style="color: #ff0000;"><a href="/index.php/alumnos/articulos/14-alumnos/articulos/35-el-inconstante-campo-magnetico-de-la-tierra">El inconstante Campo Magnético de la Tierra</a></span></li>
</ul>

<p>&nbsp;</p>

<p><b> Derrotero</b></p>

<ul>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/36-derrotas-seguras-para-cruzar-el-canal-mitre">Derrotas seguras para cruzar el Canal Mitre</a></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/37-navegacion-a-las-islas-malvinas">Navegación a las Islas Malvinas</a><b></b></li>
</ul>

<p>&nbsp;</p>

<p><strong>Elementos de a bordo</strong></p>

<ul>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/39-el-radar">El Radar</a></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/38-acerca-de-los-multiples-usos-del-barometro">Acerca de los múltiples usos del barómetro</a></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/40-entendiendo-a-los-largavistas">Entendiendo a los largavistas</a></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/41-historia-de-los-nudos">Historia de los nudos</a></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/42-el-silbato-de-contramaestre">El silbato de Contramaestre</a></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/43-el-balde">El balde</a></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/44-la-ecosonda-consideraciones-para-un-crucero-sin-sobresaltos">La ecosonda (Consideraciones para un crucero sin sobresaltos) </a></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/45-cartas-de-navegacion-electronicas">Cartas de navegación electrónicas</a></li>
</ul>

<p>&nbsp;</p>

<p><b>Los barcos todos</b></p>

<ul>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/46-el-sarcofago-de-portus">El sarcófago de Portus</a> <span style="color: #ff0000;">(Nuevo)</span></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/47-el-barco-antirrolido">El barco antirrolido</a> <span style="color: #ff0000;">(Nuevo)</span></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/48-el-holandes-errante">El Holandés Errante</a></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/49-acorazado-graf-spee">Acorazado "Graf Spee"</a></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/50-como-evaluar-un-barco-usado-planilla-de-control">¿Cómo evaluar un barco usado? - Planilla de Control -</a></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/51-optimist-navegando-sobre-rulemanes">Optimist: Navegando sobre rulemanes</a></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/52-gomones-y-semirrigidos">Gomones y semirrígidos</a></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/53-algo-sobre-motos-de-agua-y-jet-ski">Algo sobre Motos de Agua y Jet Ski</a></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/54-fragata-sarmiento">Fragata Sarmiento</a></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/55-corbeta-uruguay">Corbeta Uruguay</a></li>
	<li><a href="/index.php/alumnos/articulos/14-alumnos/articulos/56-historia-increible-a-bordo-de-la-fragata-libertad">Historia increíble a bordo de la Fragata Libertad</a></li>
</ul>
</div>
	</div>
	    </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
