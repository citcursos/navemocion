
<!doctype html>
<html lang="es">
  <head>
      <?php
          include 'common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include 'common/nav.php';
      ?>
    </header>
    <main role="main">


  <!-- Marketing messaging and featurettes
  ================================================== -->
  <!-- Wrap the rest of the page in another container to center all the content. -->

  <div class="container img-header">
  	<div class="col-md-6 px-0">
		<h1 class="display-4">Astillero</h1>
	</div>
  </div>

  <div class="container">
	<p><i><b>En esta sección encontrarás todas las novedades y datos útiles que tienen que ver con el equipamiento, seguridad y mantenimiento de las embarcaciones</b></i></p>

	<div class="row text-center">
		<div class="col-md-4">
			<div class="row">
				<div class="col-md-12">
					<img alt="" class="img-fluid" src="<?php echo $link; ?>img/seguridad-icon2.fw.png" >
				</div>
				<div class="col-md-12">
					<a href="<?php echo $link; ?>astillero/matafuegos.php">
						<img alt="" src="<?php echo $link; ?>img/matafuegos.jpg">
					</a>
					<a href="<?php echo $link; ?>astillero/seguridad.php">
						<img alt=""src="<?php echo $link; ?>img/seguridad.jpg">
					</a>
					<a href="<?php echo $link; ?>astillero/seguros.php">
						<img alt="" src="<?php echo $link; ?>img/seguros.jpg">
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="row">
				<div class="col-md-12">
					<img alt="" class="img-fluid" src="<?php echo $link; ?>img/equipamiento-icon2.fw.png" >
				</div>
				<div class="col-md-12">
					<a href="<?php echo $link; ?>astillero/cintas-y-tapetes.php">
						<img alt="" src="<?php echo $link; ?>img/cintas.jpg">
					</a>
					<a href="<?php echo $link; ?>astillero/gps.php">
						<img alt="" src="<?php echo $link; ?>img/cartografia.jpg">
					</a>
					<a href="<?php echo $link; ?>astillero/paneles-solares.php">
						<img alt="" src="<?php echo $link; ?>img/panelessolares.gif">
					</a>
					<a href="<?php echo $link; ?>astillero/regla-de-mar.php">
						<img alt="" src="<?php echo $link; ?>img/regla_mar.jpg">
					</a>
					<a href="<?php echo $link; ?>astillero/timon-de-viento.php">
						<img alt="" src="<?php echo $link; ?>img/timon_viento.jpg">
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="row">
				<div class="col-md-12">
					<img alt="" class="img-fluid" src="<?php echo $link; ?>img/puesta-punto-icon2.fw.png" >
				</div>
				<div class="col-md-12">
					<a href="<?php echo $link; ?>astillero/baterias.php">
						<img alt="" src="<?php echo $link; ?>img/baterias.jpg">
					</a>
					<a href="<?php echo $link; ?>astillero/grafica.php">
						<img alt="" src="<?php echo $link; ?>img/grafica.jpg">
					</a>
					<a href="<?php echo $link; ?>astillero/heladeras.php">
						<img alt="" src="<?php echo $link; ?>img/heladeras.jpg">
					</a>
					<a href="<?php echo $link; ?>astillero/herreria.php">
						<img alt="" src="<?php echo $link; ?>img/herreria.jpg">
					</a>
					<a href="<?php echo $link; ?>astillero/loneria.php">
						<img alt="" src="<?php echo $link; ?>img/loneria.jpg">
					</a>
					<a href="<?php echo $link; ?>astillero/motores.php">
						<img alt="" src="<?php echo $link; ?>img/motores.jpg">
					</a>
					<a href="<?php echo $link; ?>astillero/pintor-nautico.php">
						<img alt=""src="<?php echo $link; ?>img/pintornautico.jpg">
					</a>
					<a href="<?php echo $link; ?>astillero/pinturas.php">
						<img alt="" src="<?php echo $link; ?>img/pinturas.jpg">
					</a>
					<a href="<?php echo $link; ?>astillero/plastico.php">
						<img alt="" src="<?php echo $link; ?>img/plastico.jpg">
					</a>
					<a href="<?php echo $link; ?>astillero/selladores.php">
						<img alt="" src="<?php echo $link; ?>img/selladores.jpg">
					</a>
					<a href="<?php echo $link; ?>astillero/tapiceria.php">
						<img alt="" src="<?php echo $link; ?>img/tapiceria.jpg">
					</a>
					<a href="<?php echo $link; ?>astillero/velas.php">
						<img alt="" src="<?php echo $link; ?>img/velas.jpg">
					</a>
				</div>
			</div>
		</div>
	</div>
  </div><!-- /.container -->


  <!-- FOOTER -->
  <?php
        include 'common/footer.php';
    ?>
</main>
</body>
</html>
