<!doctype html>
<html lang="es">

<head>
	<?php
	include '../common/head.php';
	?>
</head>

<body>
	<header style="height: 100px;">
		<?php
		include '../common/nav.php';
		?>
	</header>
	<main role="main">
		<div class="container img-header">
			<div class="col-md-12 px-0">
				<h1 class="display-4">Librerío - Navegando con Dios</h1>
			</div>
		</div>
		<div class="container grey">
			<div itemprop="articleBody">

				<p>La Biblia es un libro sagrado, un verdadero tesoro: no conviene leerla como una revista o una novela, tampoco por mera curiosidad. Hay que leerla "como lo que es realmente: la Palabra de Dios, que actúa en los que creen", como el Libro de la Alianza de amor que Dios hizo con la humanidad, como la Buena Noticia más "buena" y más "noticia", la única que da su verdadero sentido a la vida y a la muerte.</p>

				<p><br>
					Hay que leerla con humildad. "Te alabo, Padre, Señor del cielo y de la tierra -oró Jesús- por haber ocultado estas cosas (las del Reino de Dios) a los sabios y a los prudentes, y haberlas revelado a los pequeños."</p>

				<p><br>
					Porque si además de navegar -como lo hizo el mismo Jesús- , penetra en nuestro corazón la Buena Noticia, seremos plenamente felices y capaces de hacer felices a los demás.</p>

				<p>Sí. Tenemos la posibilidad de dejarlo entrar en nuestro corazón, y.... ¡tener un Dios que camina sobre las aguas, manda calmar al viento y al oleaje, y que nos AMA más de lo que podamos imaginar!</p>
				&nbsp;

				<ul>
					<li>
						<a href="#" data-toggle="modal" data-target="#pesca-mila">La pesca milagrosa</a>

						<div class="modal fade" id="pesca-mila" tabindex="-1" role="dialog">
							<div class="modal-dialog modal-dialog-scrollable">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title">La pesca milagrosa</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body text-left">
										<div class="commontext" style="padding: 10 10 10 20;">Cierto día la gente se agolpaba alrededor de Jesús para escuchar la palabra de Dios, y él estaba de pie a la orilla del lago de Genesaret. En eso vio dos barcas amarradas al borde del lago; los pescadores habían bajado y lavaban las redes. Subió a una de las barcas, que era la de Simón, y le pidió que se alejara un poco de la orilla; luego se sentó y empezó a enseñar a la multitud desde la barca.
											<p>Cuando terminó de hablar, dijo a Simón: “Navega mar adentro y echen las redes para pescar.” Simón respondió: “Maestro, por más que lo hicimos durante toda la noche, no pescamos nada; pero, si tú lo dices, echaré las redes.” Así lo hicieron, y pescaron tal cantidad de peces, que las redes casi se rompían. Entonces hicieron señas a sus compañeros que estaban en la otra barca para que vinieran a ayudarles. Vinieron y llenaron tanto las dos barcas, que por poco se hundían.</p>

											<p>Al ver esto, Simón Pedro se arrodilló ante Jesús, diciendo: “Señor, apártate de mí, que soy un hombre pecador.” Pues tanto él como sus ayudantes se habían quedado sin palabras por la pesca que acababan de hacer. Lo mismo les pasaba a Santiago y Juan, hijos de Zebedeo, compañeros de Simón. Jesús dijo a Simón: “No temas; en adelante serás pescador de hombres.” En seguida llevaron sus barcas a tierra, lo dejaron todo y siguieron a Jesús.</p>
											<h6 class="font-italic">Evangelio de San Lucas</h6>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
									</div>
								</div>
							</div>
						</div>


					</li>
					<li>
						<a href="#" data-toggle="modal" data-target="#arca">El arca de Noé</a>

						<div class="modal fade" id="arca" tabindex="-1" role="dialog">
							<div class="modal-dialog modal-dialog-scrollable">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title">El arca de Noé</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body text-left">
										<div class="commontext" style="padding: 10 10 10 20;">
											<p><b>Lo que sigue corresponde al primer libro del Antiguo Testamento la Biblia llamado Génesis:</b></p>

											<p>Hijos de Dios e hijas de los hombres</p>

											<p>6 Cuando los hombres empezaron a multiplicarse sobre la tierra y les nacieron hijas, los hijos de Dios se dieron cuenta de que las hijas de los hombres eran hermosas y tomaron por esposas de entre todas aquellas que les gustaron. Entonces dijo Yavé: "No permanecerá para siempre mi espíritu en el hombre, porque no es más que carne; que sus días sean de ciento veinte años." En ese entonces había gigantes sobre la tierra, y también los hubo después, cuando los hijos de Dios se unieron a las hijas de los hombres y tuvieron hijos de ellas. Estos fueron los héroes de la antigüedad, hombres famosos.</p>

											<p>El diluvio</p>

											<p>Yavé vio que la maldad del hombre en la tierra era grande y que todos sus pensamientos tendían siempre al mal. Se arrepintió, pues de haber creado al hombre y, muy a su pesar, dijo: "Exterminaré de la tierra a los hombres que he creado, desde el hombre hasta los animales, los reptiles y las aves del cielo; pues me pesa haberlos creado."</p>

											<p>Noé, sin embargo, se había ganado el cariño de Yavé.</p>

											<p>Esta es la historia de Noé. Noé fue, en su tiempo, un hombre justo y que se portó bien en todo; Noé andaba con Dios. Los hijos de Noé fueron: Sem, Cam y Jafet.</p>

											<p>La tierra se corrompió a la vista de Dios y se llenó de violencia. Viendo Dios que la tierra estaba corrompida, pues todos los mortales se habían descarriado en la tierra, dijo Dios a Noé: "He decidido acabar con todos los mortales, porque la tierra está llena de violencia por culpa de ellos. Por eso los voy a hacer desaparecer de la tierra.</p>

											<p>Haz para ti un arca de madera de ciprés; en el arca dispondrás celditas , y la calafatearás con brea por dentro y por fuera. Estas serán sus medidas: Longitud del arca, ciento cincuenta metros; ancho, veinticinco metros; alto, quince metros. Al arca le pondrás un techo y le dejarás medio metro de entretecho, pondrás la puerta del arca en un costado y harás un primer piso, un segundo y un tercero.</p>

											<p>Por mi parte, voy a mandar el diluvio, o sea, las aguas sobre la tierra, para acabar con todo ser que respira y vive bajo el cielo; todo cuanto existe en la tierra morirá. Pero contigo voy a firmar mi pacto: Entrarás en el arca tú y tus hijos, tu esposa y las esposas de tus hijos contigo. Meterás en el arca una pareja de todo ser viviente, o sea de todos los animales, para que puedan sobrevivir contigo. Tomarás macho y hembra. De cada especie de pájaros, de animales, de cada especie de los que se arrastran por el suelo entrarán contigo dos de cada una para que puedan salvar su vida. Tú mismo, además, procúrate toda clase de alimentos y guárdalos, pues te servirán de comida a ti y a ellos."</p>

											<p>Hizo pues, Noé lo que Yavé le había mandado.</p>

											<p>7 Yavé dijo a Noé: " Entra en el Arca, tú y tu familia, pues tú eres el único justo que he encontrado en esta generación. De todos los animales puros, tomarás siete parejas de cada especie: cada macho con su hembra. De los animales impuros, tomarás un macho con su hembra. Del mismo modo, de las aves del cielo tomarás siete parejas: macho y hembra. Esto será con el fin de conservar las especies sobre la tierra. Porque dentro de siete días haré llover sobre la tierra durante cuarenta días y cuarenta noches, y exterminaré a todos los seres que creé."</p>

											<p>Noé hizo todo lo que Yavé le había mandado. Noé tenía seiscientos años cuando se produjo el diluvio que inundó la tierra.</p>

											<p>Noé, pues, junto con su esposa, sus hijos y las esposas de sus hijos, entró en el Arca para salvarse de las aguas del diluvio. Animales puros e impuros, aves del cielo y reptiles de la tierra, entraron con Noé en el Arca. Entraban de dos en dos, macho y hembra, como Dios lo había ordenado. Luego, a los siete días, comenzaron a caer sobre la tierra las aguas del diluvio.</p>

											<p>A los seiscientos años de la vida de Noé, el día diecisiete del segundo mes del año, brotaron todos los manantiales del fondo del mar y las compuertas del cielo se abrieron. Estuvo lloviendo sobre la tierra por cuarenta días y cuarenta noches. Ese mismo día entró Noé en el Arca con sus hijos Cam, Sem y Jafet, su esposa y nueras. Y también entraron con ellos cada una de las especies de animales salvajes, de los otros animales, de los reptiles que se arrastran en el suelo, y de las aves; toda clase de pájaros y de insectos alados. De todo ser que respira y vive entraron con Noé en el arca en fila de a dos. Y los que entraban eran un macho y una hembra de cada especie que iban llegando según la orden de Dios.</p>

											<p>Y Yavé cerró la puerta del Arca detrás de Noé. EL diluvio cayó por espacio de cuarenta días sobre la tierra. Crecieron, pues, las aguas y elevaron el Arca muy alto sobre la tierra.</p>

											<p>Las aguas crecieron mucho sobre la tierra; mientras tanto el arca flotaba sobre las aguas. Subió el nivel de las aguas y crecieron mucho, muchísimo, sobre la tierra, y quedaron cubiertos los montes más altos que hay debajo del cielo. Quince metros por encima subió el nivel de las aguas, quedando cubiertos los montes. Murió todo ser mortal que se mueve sobre la tierra; aves, bestias, animales y todo lo que se mueve sobre la tierra. Y toda la humanidad.</p>

											<p>Todo ser vivo que existía sobre la tierra murió. Así, perecieron todos los vivientes que había sobre la tierra, desde el hombre hasta los animales, los reptiles y las aves del cielo. Todos fueron borrados de la superficie de la tierra. Sólo sobrevivieron Noé y los que estaban con él en el Arca.</p>

											<p>8 Las aguas inundaron la tierra por espacio de ciento cincuenta días. Y Dios se acordó de Noé y de todas las fieras salvajes y de los otros animales que estaban con él en el arca.</p>

											<p>Los manantiales que brotaban desde el fondo del mar y las compuertas del cielo, que estaban abiertas, se cerraron, y la lluvia cesó de caer sobre la tierra.</p>

											<p>Las aguas iban bajando en la superficie de la tierra: Comenzaron a bajar al cabo de ciento cincuenta días. El día diecisiete del séptimo mes, el Arca descansó sobre los montes de Ararat. Y las aguas siguieron bajando hasta el mes décimo, hasta que el día primero de este mes aparecieron las cumbres de los montes.</p>

											<p>Después de cuarenta días, Noé abrió la ventana que había hecho en el Arca y soltó al cuervo, el cual revoloteaba sobre las aguas, yendo y viniendo, hasta que se evaporaron las aguas de la tierra. Después Noé soltó a la paloma, para ver si las aguas habían bajado en la superficie de la tierra. La paloma, no encontrando dónde posarse, volvió al Arca, pues todavía las aguas cubrían toda la superficie de la tierra. Noé alargó la mano, tomó la paloma y la hizo entrar en el Arca. Esperó siete días más y de nuevo soltó a la paloma fuera del Arca. La paloma volvió al atardecer, trayendo en su pico una rama verde de olivo.</p>

											<p>Así, Noé se dio cuenta que las aguas habían bajado en la superficie de la tierra. Aún esperó otros siete días más y soltó a la paloma, que ya no volvió más al Arca. Noé retiró la cubierta del Arca, miró y vio que la superficie del suelo estaba seca.</p>

											<p>El día veintisiete del segundo mes del año seiscientos uno de la vida de Noé quedó seca la tierra. Entonces Dios habló de esta manera a Noé: "Sal del arca, tú y tu esposa, tus hijos y tus nueras. Saca también contigo a todos los seres vivientes que tienes dentro, de cualquier especie, ya sean aves, animales o reptiles que se arrastran por el suelo; que pululen, llenen la tierra y se multipliquen." Salió, pues, Noé y con él sus hijos, su esposa y sus nueras. Todos los animales salvajes y domésticos, todas las aves y todos los reptiles que se arrastraban sobre la tierra salieron por familias del arca.</p>

											<p>Noé construyó un altar a Yavé y, tomando de todos los animales puros y de todas las aves puras, ofreció sacrificios en el altar. Al aspirar el agradable aroma, Yavé decidió: "Nunca más maldeciré la tierra por culpa del hombre, pues veo que desde su infancia está inclinado al mal. Ni volveré más a castigar a todo ser viviente como acabo de hacerlo."</p>

											<p>Mientras dure la tierra,<br>
												habrá siembra y cosecha,<br>
												pues nunca cesarán<br>
												ni el frío ni el calor,<br>
												ni el verano o el invierno<br>
												ni los días o las noches.</p>

											<p align="right"><b>Fin</b></p>
										</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>

					</li>
					<li><a href="http://www.navemocion.com/librerio/yave_popup.html" onclick="window.open(this.href, 'yave', 'resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=450,height=450'); return false;">Yavé responde a Job</a></li>
					<li><a href="http://www.navemocion.com/librerio/desierto_popup.html" onclick="window.open(this.href, 'desierto', 'resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=450,height=450'); return false;">Elías huye al desierto</a></li>
					<li><a href="http://www.navemocion.com/librerio/jonas_popup.html" onclick="window.open(this.href, 'jonas', 'resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=450,height=450'); return false;">Jonás</a></li>
					<li><a href="http://www.navemocion.com/librerio/signo_popup.html" onclick="window.open(this.href, 'signo', 'resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=450,height=450'); return false;">El signo de Jesús</a></li>
					<li><a href="http://www.navemocion.com/librerio/aguas_popup.html" onclick="window.open(this.href, 'aguas', 'resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=450,height=450'); return false;">Jesús anda sobre las aguas del lago</a></li>
					<li><a href="http://www.navemocion.com/librerio/tempestad_popup.html" onclick="window.open(this.href, 'tempestad', 'resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=450,height=450'); return false;">La tempestad calmada</a></li>
					<li><a href="http://www.navemocion.com/librerio/pecados_popup.html" onclick="window.open(this.href, 'pescados', 'resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=450,height=450'); return false;">Pecados de la lengua</a></li>
				</ul>

			</div>
		</div>
		<!-- /.container -->
		<!-- FOOTER -->
		<?php
		include '../common/footer.php';
		?>
	</main>
</body>

</html>