<!doctype html>
<html lang="es">

<head>
	<?php
	include '../common/head.php';
	?>
</head>

<body>
	<header style="height: 100px;">
		<?php
		include '../common/nav.php';
		?>
	</header>
	<main role="main">
		<div class="container img-header">
			<div class="col-md-12 px-0">
				<h1 class="display-4">Librerío - Libros de Náutica</h1>
			</div>
		</div>
		<div class="container grey">
			<div itemprop="articleBody">
				<p>&nbsp;</p>

				<p style="text-align: center;">Todos estos libros pueden consultarse personalmente o adquirirse en <b>“Librerío”</b> Crámer 3980 “A” (Coordinando la visita al <b>4701-4410</b>).<br>
					También enviamos los libros a domicilio, y a todo el país, comunicándose a <a href="mailto:info@navemocion.com"><b><span id="cloak438e12e97a2550fd8eddd0ec8f326ce8"><a href="mailto:info@navemocion.com">info@navemocion.com</a></span>
							<script type="text/javascript">
								document.getElementById('cloak438e12e97a2550fd8eddd0ec8f326ce8').innerHTML = '';
								var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
								var path = 'hr' + 'ef' + '=';
								var addy438e12e97a2550fd8eddd0ec8f326ce8 = '&#105;nf&#111;' + '&#64;';
								addy438e12e97a2550fd8eddd0ec8f326ce8 = addy438e12e97a2550fd8eddd0ec8f326ce8 + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
								var addy_text438e12e97a2550fd8eddd0ec8f326ce8 = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
								document.getElementById('cloak438e12e97a2550fd8eddd0ec8f326ce8').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy438e12e97a2550fd8eddd0ec8f326ce8 + '\'>' + addy_text438e12e97a2550fd8eddd0ec8f326ce8 + '<\/a>';
							</script>
						</b></a>&nbsp;o en la sección <a href="/index.php/contact">Contacto</a>&nbsp;(los gastos de envío no están incluídos en los precios)</p>

				<p><br>
					&nbsp;</p>

				<table align="center" border="0" cellpadding="0" cellspacing="0" width="90%">
					<tbody>
						<tr>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_1.jpg"></p>
							</td>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_2.jpg"></p>
							</td>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_3.jpg"></p>
							</td>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_4.jpg"></p>
							</td>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_5.jpg"></p>
							</td>
						</tr>
						<tr>
							<td align="center" height="65" valign="top">
								<a href="#" data-toggle="modal" data-target="#catamaran">Un catamarán en apuros</a>
								<div class="modal fade" id="catamaran" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Un catamarán en apuros</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: Agrelo Crampton</h6>
												<p>El sueño de dos jubilados: Consuelo Agrelo (argentina) y Reginald Crampton (inglés) habían soñado por años con esta travesía. Pero el día de zarpar, la noticia de la invasión a Malvinas les cayó como un rayo. Sin pensarlo demasiado, resolvieron que ya era tarde para desistir.<br>
													Un ¨Catamarán en Apuros¨&nbsp;es la crónica electrizante de un crucero de 17.000 millas donde las tormentas, las ballenas, los piratas caribeños o los arrecifes traicioneros están presentes a cada paso.<br>
													Con todo, la suerte de esta historia queda echada con la prohibición de desembarcar en Dársena Norte que los militares argentinos imponen al matrimonio por enarbolar Pabellón británico.<br>
													Una pareja cruzando el Atlántico en una travesía de Inglaterra a Buenos Aires con todos los condimentos.</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>


							</td>
							<td align="center" valign="top">
								<a href="#" data-toggle="modal" data-target="#odisea">La Odisea de Liberdade</a>

								<div class="modal fade " id="odisea" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">La Odisea de Liberdade</h5>

												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: Joshua Slocum</h6>
												<p>Texto completo de uno de los más brillantes navegantes de todos los tiempos, más dos capítulos inéditos.<br>
													Corría 1886 cuando el Capitán Joshua Slocum entraba por segunda vez al Río de la Plata. Apenas dos años antes, su esposa Virginia había sido enterrada en el Cementerio británico de Buenos Aires. Pero no era su recuerdo el que lo traía nuevamente a estas playas. Slocum luchaba desesperadamente para mantener a su familia sin abandonar el mar. Poco después naufragaría con el “Aquidneck” junto a su segunda mujer y sus hijos. Pero la garra de este superdotado del mar aparece en toda su dimensión cuando con los restos del naufragio construye con sus manos el velero aparejado a junco que los llevaría de vuelta a los Estados Unidos.
												</p>
												<p><b>Prólogo de Slocum a su libro:</b><br>
													<i>“A manera de saludo: Este libro, con su forma y aparejo originales, lleva a su bordo la narración de los extraños acontecimientos en un hogar flotante. Su constructor, marino de larga experiencia, pudo haberlo aderezado convenientemente, pero no quiso entrometerse imprudentemente en otros dominios. ¡Ojalá consiga llevarlo a buen puerto, como los experimentados navegantes costeros, y que además se lo perdonen los lectores!<br>
														¿Qué importa si afrontamos corrientes contrarias? Y si corren a favor nuestro, ¿hacia dónde y con que propósito nos llevan? ¡Nuestro viaje es tan insignificante que quizá poco importe nuestro destino, dado que la “bendición de un nuevo día” nos acompaña. ¿No es la aceptación de esto último lo que hace feliz al viejo marino, aun en plena tormenta, y lo llena de esperanzas aunque se halle sobre una tabla en medio del océano? ¡Con toda seguridad! Porque la belleza espiritual del mar, en comunión con el alma humana, no admite infieles en su extensión infinita.”</i></p>


											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>



							</td>
							<td align="center" valign="top">
								<a href="#" data-toggle="modal" data-target="#historia">Historia secreta de barcos<br>y navegantes</a>
								<div class="modal fade" id="historia" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Historia secreta de barcos y navegantes</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: León Renard</h6>
												<p>¿Cómo era el barco de Cleopatra?<br>
													¿Cómo se reclutaban los galeotes para las galeras francesas?<br>
													¿Qué le pasó a Fulton cuando estrenó su barco a vapor?<br>
													¿Es cierto que la nave de Tolomeo Filopator tenía un bosque con pájaros en una de sus doce cubiertas?<br>
													¿Cuál es la verdadera leyenda del holandés errante?<br>
													¿Qué contestó Napoleón cuando le propusieron que sus marineros estudiaran en tierra?<br>
													Cautivante paseo por las intimidades de barcos y navegantes</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>




							</td>
							<td align="center" valign="top">
								<a href="#" data-toggle="modal" data-target="#mensajes">Mensajes en botellas</a>
								<div class="modal fade" id="mensajes" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Mensajes en botellas</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: Hernán Alvarez Forn</h6>

												<p><b>Un clásico: relato humorístico de una regata Buenos Aires - Río de Janeiro</b></p>

												<div class="commontext" style="padding: 10 10 10 30;">
													<p>Párrafos del prólogo de la primera edición: “cada tres años una importante regata de mar plumerea las telarañas cerebrales de los yachtmens del Río de la Plata: es la Buenos Aires - Río de Janeiro.<br>
														Consiste en recorrer a vela solamente y en el menor tiempo posible la mitad de la panza atlántica de América del Sur, con el aliciente de tener el Golfo de Santa Catalina de por medio.<br>
														La diversidad de vientos y mares que se pueden encontrar durante el trayecto y la prohibición de hacer escalas so pena de ser descalifcado, obliga a una cuidadosa y eficiente preparación de barcos y tripulantes, que generalmente se va cumpliendo con gran pereza en los años intermedios y afiebradamente durante los últimos quince días previos a la largada, en los que surgen toda clase de inconvenientes, reparaciones y amarinamientos que es menester llevar a cabo a marchas forzadas.<br>
														De este modo se consigue dejar de navegar un mes entero y se llega al día de la partida con todo a medio terminar, absolutamente desentrenado y completamente agotado, condiciones tan normales entre los regatistas de alto bordo que lo contrario parecería ilógico.<br>
														La regata está organizada por el Yacht Club Argentino y el Iate Clube de Río de Janeiro. Las marinas argentinas, brasileña y uruguaya colaboran con barcos escolta y aviones de patrulla. Mensajes especiales para la flota se radian dos o tres veces por día para dar el parte meteorológico -Meteoro en la jerga náutica- y una vigilancia continua permite captar cualquier llamada de auxilio.<br>
														Para los barcos medianos suele durar entre diez y quince días, y su clasificación se arregla conforme a los handicaps de cada barco, de modo que la “chance” es pareja.<br>
														Cada barco es una isla de civilización. En esos diez o quince días un número reducido de hombres y ocasionalmente alguna mujer- deben convivir en unos pocos metros cuadrados habitables.<br>
														Así se pasan las veinticuatro horas de cada día, sin poder aislarse, sin poder cambiar de ambiente, sin tener ninguna forma de escapar de la presencia de los demás. Lo que allí sucede queda entre ellos. Por eso me ha interesado relatar algunos episodios de entrecasa, más que la regata propiamente dicha..<br>
														Pues el yachting, entre otras definiciones, es el método más cansador de descansar que ha inventado el hombre; es el deporte de las veinticuatro horas. Imagínense ustedes, por ejemplo, a un grupo de enloquecidos jugando golf diez días y diez noches seguidas, durmiendo algunos en la cancha mientras los relevan los otros, y tendrán una idea de la causa por la cual así defino al yachting.”</p>

													<p></p>

													<div align="right">Hormiga Negra</div>
												</div>

											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>
							</td>
							<td align="center" valign="top">
								<a href="#" data-toggle="modal" data-target="#babas-del-diablo">Babas del Diablo</a>
								<div class="modal fade" id="babas-del-diablo" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Babas del Diablo</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: H. Gil Elizalde</h6>

												<p>Este clásico libro, es una evocación humorística de muchas situaciones que se dan en la navegación deportiva. Aquí van algunas definiciones:<br>
													“-el Yachting, como todos sabemos, se divide en dos rebanadas: la regata y el crucero. La regata es el arte de navegar con un trescientos por ciento de incomodidad extra, para conseguir un uno por ciento de velocidad suplementaria.<br>
													El crucero es el método más cansador que el hombre haya inventado para descansar.<br>
													- Propietario es sinónimo de Mecenas, es la persona que compra un barco para que lo disfruten sus amigos, y amigos son los que tiran cenizas del cigarrillo en el piso, se toman el alcohol de a bordo o pierden los remos del chinchorro.<br>
													- Rumbo de agua: es el agujero que aparece en un casco cuando desaparece la piedra que lo tapaba, después de un topetazo a toda marcha.<br>
													- el sextante es el único instrumento que nos puede convencer de que estamos navegando con nuestro barco en el partido de Chacabuco o en las inmediaciones de la Basílica de Luján.”</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>

							</td>
						</tr>
						<tr>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_6.jpg"></p>




							</td>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_7.jpg"></p>
							</td>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_8.jpg"></p>
							</td>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_9.jpg"></p>
							</td>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_1.jpg"></p>
							</td>
						</tr>
						<tr>
							<td align="center" height="45" valign="top">
								<a href="#" data-toggle="modal" data-target="#antarticos">Antárticos</a>
								<div class="modal fade" id="antarticos" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Antárticos</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: Hernán Alvarez Forn</h6>
												<p></p>

												<p>Así como los montañistas tienen el Aconcagua o el Everest, los navegantes adoran el Cabo de Hornos y la Antártida.<br>
													Mientras el Cabo es el paradigma de los vientos violentos y del mar tempestuoso, Antártida representa lo inhóspito, lo frío, lo inaccesible. Su antesala es dominio de aventureros: las 460 millas del Pasaje de Drake, inquietante porción de la única franja sin tierras que circunvala el mundo.<br>
													El gran protagonista de la Antártida es el hielo. Desde las moles como catedrales hasta los témpanos menores y ubicuos, pasando por los arteros gruñones y otra variada gama de formaciones trashumantes que acechan al intruso. La navegación antártica es un ejercicio alucinante para los navegantes de aguas libres. Consumarla en un velero de 8,80 metros implica un desafío mayúsculo.<br>
													En 1987, Hernán Alvarez Forn y sus tripulantes llegaron a la Antártida en el “Pequod”, primer yate argentino que cumplió la hazaña. Estas páginas inolvidables, llenas de emoción y alegría, describen paso a paso aquella aventura, desde que empezaron los preparativos hasta su retorno a Buenos Aires.</p>


											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>
							<td align="center" valign="top">
								<a href="#" data-toggle="modal" data-target="#contra-infierno-y-marea">Contra infierno y marea</a>
								<div class="modal fade" id="contra-infierno-y-marea" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Contra infierno y marea</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: Clare Francis</h6>
												<p></p>

												<p>Con apenas 45 kilos de peso y no más de un metro cincuenta, Clare Francis, sin embargo, puede ponerse a la altura de los grandes navegantes de este siglo. Ex bailarina de ballet y estudiante de economía, fue una de los 125 participantes en la “Regata Transatlántica en solitario de 1976”. Este es su apasionante relato de aquella competencia, una abrumadora batalla que provocó varios naufragios y que cobró la vida de dos tripulantes a lo largo de condiciones climáticas muy rigurosas. A pesar de que treinta y siete navegantes se vieron obligados a desertar, Clare Francis, a bordo del Robertson´s Golly, su sloop de 11,40 metros, navegó de Plymouth, Inglaterra, a Newport, Estados Unidos, en 29 días.<br>
													Durante dicha travesía sobrevivió a mares tempestuosos, a vientos arrolladores y a dos semanas de niebla permanente, y apenas si pudo evitar su desaparición entre dos témpanos.<br>
													Escrito con irresistible sentido del humor, “Contra Infierno y Marea” propone también una explicación sobre los verdaderos motivos que llevan a los navegantes de pequeñas embarcaciones a arriesgar sus vidas en largas travesías oceánicas.</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>

							</td>




							<td align="center" valign="top">
								<a href="#" data-toggle="modal" data-target="#el-naufrago-de-las-estrellas">El náufrago de las estrellas</a>
								<div class="modal fade" id="el-naufrago-de-las-estrellas" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">El náufrago de las estrellas</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: Eduardo Belgrano Rawson</h6>

												<p>Amaba a esa mujer tanto como a su barco y sin embargo precipitó a ambos por los más difíciles caminos. Un antiguo velero que hace la ruta del Cabo de Hornos es el escenario de esta odisea marina del siglo XX, cuyos protagonistas los hombres y el barco- ilustran acabadamente al lector cómo puede ser, al decir de Robert Louis Stevenson, “la peor de todas las vidas posibles”.<br>
													Obra fundamentalmente escrita para los amantes del mar, “El Náufrago de las Estrellas” es, además, una pintura tierna y humorística de un triángulo amoroso milenario: el de un hombre con su mujer y su barco.</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>
							</td>

							<td align="center" valign="top">

								<a href="#" data-toggle="modal" data-target="#por-la-ruta-de-los">Por la ruta de los Clippers</a>

								<div class="modal fade" id="por-la-ruta-de-los" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Por la ruta de los Clippers</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: Francis Chichester</h6>
												<p></p>

												<p>Esta es la ruta marítima más romántica del mundo y las historias incluidas en el presente volumen cubren los más diversos aspectos de aquella epopeya legendaria. Pero además de los Clippers, estas historias se refieren a pequeños veleros tripulados por navegantes solitarios que doblaron el Cabo de Hornos; a travesías signadas por furibundos temporales; a naufragios y vueltas de campana; a diversas proezas de navegación; a aventuras entre los hielos; a ballenas y calamares gigantes e incluso al flagelo del escorbuto.</p>

												<p>Bajo la incomparable guía de Francis Chichester, autor y recopilador de la obra, los amantes del mar se reencontrarán con las legendarias figuras de Alain Bombard, Joshua Slocum, Frank Bullen, Ann Davison, Richard Dana, Vito Dumas, Ernst Shackleton, Ernesto Uriburu y otros protagonistas de estos apasionantes capítulos de la vida real.</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>


							</td>


							<td align="center" valign="top">
								<a href="#" data-toggle="modal" data-target="#los-40-bramadores">Los 40 bramadores</a>
								<div class="modal fade" id="los-40-bramadores" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Los 40 bramadores</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: Vito Dumas</h6>

												<p></p>

												<p>El mejor y más clasico libro de náutica de nuestro país.<br>
													Relato de Vito sobre su vuelta al mundo en cuatro etapas, en plena Guerra Mundial, y en solitario, por la “Ruta Imposible”, es la apasionante historia de una de las más audaces aventuras de la navegación a vela, donde el autor tuvo que experimentar indecibles sufrimientos y salvar enormes peligros y dificultades, doblando los tres temibles cabos, Buena Esperanza, Tasmania y Hornos, con sus continuas y furiosas tempestades. Esta última edición está completada con ilustraciones inéditas de vito dumas, facilitadas por su hijo.<br>
													(Sinceramente: si sos argentino y navegante, no te podés perder este libro, que además de todo lo que representa está muy bien escrito).</p>

												<p>Párrafo del libro: “Conocía muy bien el barco y sabía de su aguante y comportamiento en alta mar. En un viaje a Río de Janeiro realizado en 1937, al regreso fui sorprendido por un pampero que sopló a ciento cuarenta kilómetros por hora. Fue el mismo que abatió en las rocas de Punta del Este al Bonni Joan y al Shaheen, que se encontraban en puerto. La violencia del temporal los inutilizó. Me encontraba precisamente en la zona en que zozobró el Cachalote, originando una tragedia de la cual no quedaron rastros.<br>
													Era un atardecer y preparaba chocolate, en la esperanza de pasar la noche lo mejor posible. Afuera, el viento había hecho estragos en el velamen. Sólo quedaban jirones. La vista de ese mar embravecido producía escalofríos. Estaba yo capeando, aguantando proa a la tormenta. De repente, lo imprevisto: una enorme sacudida y me sentí despedido. El golpe fue espantoso. Siguiendo el movimiento del barco, rodé hasta encontrarme sentado en el techo de la camareta. Por unos segundos, eternos segundos, los mástiles quedaron apuntando al fondo del mar y la quilla, al cielo. El chocolate flotaba en es piso que era un techo. Sin referencias exteriores, pensé que el barco se hundiría. Algo atontado por el golpe, tuve la sensación de que todo acababa…y sin defensa. Las manos ensangrentadas, las sentía calientes. Era impotente ante el suceso. ¿Cómo salir de ese encierro en el que reinaba el más absoluto desorden? La quilla hacia arriba, los mástiles hacia abajo y el LEGH II que sería invadido por las aguas…La camareta, herméticamente cerrada. Me hallaba en una cárcel sin salida, en un ataúd. Me entregué al destino. Hasta una especie de laxitud me invadió; un no sé qué de conformidad, de agradecimiento y de respeto a la muerte tantas veces desafiada. Iba llegando, llegando, mientras el barco se hundiría. Nació en mí un abandono total. No quedaba una sola posibilidad de lucha. Sin armas, me resigné blandamente. Se me ocurrió que iba siendo otra vez niño.<br>
													El LEGH II fue recobrando lentamente su posición normal. Llegaba una esperanza, aunque tardaba mucho. En cuanto fue posible, salté a cubierta. La laxitud fue reemplazada por la mayor energía. Fuerzas de todos lados me animaban: de los músculos, del cerebro, de los nervios….Venían a raudales, como en bandadas. Miré el mar y sonreí. Cara a cara nos mirábamos. Ya no estaba en el ataúd. Me encontraba en cubierta. El chinchorro estaría lejos, semihundido. No importaba nada. Podía luchar y con los ojos jubilosos, con el corazón esperanzado, dueño de todas mis facultades y mis fuerzas. Se lo agradecí al barco con toda mi alma, hablándole, diciéndole un montón de palabras que el viento huracanado arrastraba. Y en ese recuerdo se afirmó mi fe futura en el LEGH II.”</p>

												<p><i>La señora Ofelia no sabe que hacer. Problemas en el trabajo, conflictos familiares, el dinero no alcanza. </i></p>

												<p><i>Está tendida en la cama y muy desanimada. Se pregunta: ¿Vale la pena seguir?, no le veo sentido a tanto sacrificio. </i></p>

												<p><i>La señora Ofelia escucha consejos de sus amigas y no se reconforta. Nota que ya no queda gente que la emocione, que hay pocos que luchan por un ideal. Tampoco sus amigas, las que le hacen llegar sus palabras de aliento..... </i></p>

												<p><i>A la inflación todos la conocemos, es un desequilibrio económico que nos carcome. Pero hay otra inflación que es más destructiva, consiste en emitir palabras que no están respaldadas con las acciones. Ya nos lo dijo el filósofo: "Argentinos a los hechos". </i></p>

												<p><i>El señor Dumas nos dejó su obra, era marino y artista o mejor dicho un marino artista. En cada cuadro, en cada una de sus heroicas travesías, quiso dejar un mensaje que enrriquezca la vista de aquel que mira, que fortalezca la voluntad del que flaquea. Bellas y enormes obras de arte, que requieren ser observadas con la perspectiva de los años que van pasando, y que nos dan una visión cada vez más madura y completa. Como los frescos de una cúpula, que sólo llegan a interpretarse cabalmente a muchos metros de distancia. </i></p>

												<p><i>Por lo que es alentador que tengamos a nuestro alcance libros escritos por el propio Vito: "Sólo rumbo a la Cruz del Sur" (primer viaje en solitario desde Francia a Buenos Aires a bordo del L.E.G.H. I -que se encuentra en el Museo de Luján); "Los Cuarenta Bramadores (viaje en solitario dando la vuelta al mundo a bordo del L.E.G.H. II -que se encuentra en el Museo Naval de Tigre); y "Crucero de lo Imprevisto" , libro de reciente aparición editado por el Instituto de Publicaciones Navales, que se refiere a un viaje en solitario a Nueva York, que terminó imprevistamente en un doble cruce del Atlántico a bordo del velero "Sirio" (este libro antes solamente había sido publicado en francés nunca en castellano) </i></p>

												<p><i>¡Cualquiera de estos ejemplares es altamente recomendable!, dice la señora Ofelia, que comienza en marzo su curso de Timonel.</i></p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_2.jpg"></p>
							</td>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_3.jpg"></p>
							</td>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_4.jpg"></p>
							</td>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_5.jpg"></p>
							</td>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_6.jpg"></p>
							</td>
						</tr>
						<tr>
							<td align="center" height="45" valign="top">
								<a href="#" data-toggle="modal" data-target="#aventuras-en-el-paraiso">Aventuras en el Paraíso</a>

								<div class="modal fade" id="aventuras-en-el-paraiso" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Aventuras en el Paraíso</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: John Claus Voss</h6>
												<p></p>

												<p>Particularmente célebre por sus teorías sobre el arte de capear temporales con ancla de mar, John Claus Voss figura hoy en el Olimpo de los navegantes legendarios, a la par de monarcas como Dumas, Chichester o Slocum.<br>
													Nacido en Suecia en 1858 y muerto en 1922, este canadiense naturalizado era a los veinte años un veterano tripulante de grandes veleros y a los cuarenta un Capitán renombrado. En 1898 un aventurero le propuso asociarse para la búsqueda de un tesoro en la Isla de los Cocos, ubicada en el Pacífico a 1600 millas de la costa mejicana. Para cumplir su increíble travesía consiguieron el Xora, un sloop de 10,65 metros con cangreja, escandalosa y foque.<br>
													Sin embargo, no sería esta aventura novelesca la que inscribiría su nombre en la historia de la náutica sino el viaje de cuarenta mil millas cumplido a bordo del Tilikum, un velero de tres palos construido sobre el casco de una canoa indígena. Su nombre sería recordado también por el tifón que afrontó en el “Reina del Mar” (7,65 metros) frente a las costas de Yokohama, episodio central de uno de los tres relatos que componen esta obra.<br>
													También en este libro Voss se refiere al ancla de mar, ese implemento al que él atribuye todos los méritos de una navegación segura y hasta placentera. De tal modo, sus detalladas instrucciones para el uso de la misma son reproducidas aquí con todo detalle, confrontándolas, eso sí, con las opiniones de prestigiosos navegantes modernos, lo cual produce un saludable y enriquecedor debate.<br>
													Por lo tanto recomendamos especialmente estas obras completas de Voss, a los que busquen aventuras y conocimientos.</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>

							</td>
							<td align="center" valign="top">
								<a href="#" data-toggle="modal" data-target="#pampero">Pampero</a>

								<div class="modal fade" id="pampero" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Pampero</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: Alvaro Sires</h6>

												<p>Recostado en su canastilla de mimbre, el recién nacido está molesto de tanto calor que hace. Su madre lo ha dejado allí, debajo del sauce llorón en el patio de su sencilla casa, sombreado y perfumado patio a orillas del Río Paranacito. Su madre lo ha puesto en aquel lugar luego de haberlo hecho mamar y se ha ido a preparar la comida para el resto de sus hijos.</p>
												<p>El bebé está molesto y no se duerme, es por la tórrida brisa húmeda -como el aliento de una boca muy cercana, aliento febril de un hombre obeso dormitando con su boca abierta -.<br>
													Un joven se dirige hacia allí corriendo desde el horizonte, lleva un gran sombrero blanco de forma irregular -parecido al de un chef -, tiene un cigarro negro, y su rostro brilla -con la palidez del rayo- y su voz resuena -grave como un trueno -. Viene a caballo y a todo galope, agita los árboles y aviva las aguas. El hombre obeso despierta asombrado y huye al instante, su cuerpo de paquidermo tiene la agilidad de un chimpancé.<br>
													Y a caen los primeros gotones y la madre corre a buscar a su criatura, que acaba de dormirse plácida y distendida.<br>
													Dicen que las figuras espectrales del hombre obeso y del joven jinete, vienen siendo vistas en forma sucesiva o simultánea -desde que hubo alguien con capacidad de verlas- en los distintos parajes del Virreinato del Plata.<br>
													Sólo puedo aportar dos datos más: el hombre obeso es brasileño -tal vez paraguayo, del norte al fin -, se lo ha visto navegando aguas abajo del Paraná a horcajadas de un pesado tronco de lapacho; mientras que el jinete joven se sabe que viene de la Pampa, aunque no le dicen pampeano sino pampero.</p>

												<p><b>"PAMPERO. QUE ES Y COMO ENFRENTARLO"</b> de Álvaro Sires editado por el Departamento de Estudios Históricos Navales de la Armada Argentina.</p>

												<p>Es un libro de cuidada escritura que enfoca el fenómeno Pampero desde tres interesantes ángulos. Desde el punto de vista meteorológico dando un completo panorama de los elementos que entran en juego y de los indicios a tener en cuenta para hacer un buen pronóstico. Desde el enfoque histórico, se narran una serie de muy jugosas anécdotas en las que a lo largo de los siglos el protagonista es el mismo: el Pampero.<br>
													Y luego para terminar se brinda el testimonio de un representativo y experimentado grupo de nuestros navegantes. Es como para no desaprovechar: son ojos que observaron, son caras que sintieron rozándolas<br>
													-cada cual de una manera distinta- las presencias fantasmales del hombre obeso y del joven jinete.</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>

							</td>
							<td align="center" valign="top">

								<a href="#" data-toggle="modal" data-target="#orzavito">¡Orza Vito!</a>

								<div class="modal fade" id="orzavito" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">¡Orza Vito!</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: Enrique Celesia</h6>
												<p></p>

												<p>"...El temporal me había hecho cruzar sin para el golfo San Matías y también ¡aleluya! me había traído de nuevo a mis pagos. A partir de allí todo cambió, la escoba dorada del sol barrió los nubarrones, la alta costa ocre de la Patagonia se convirtió en el amarillo y verde de la llanura pampeana, el azul profundo del mar se transformó en esmeralda, el viento se afirmó del este, suave pero constante, aparecieron las gaviotas y me saludaron con sus carcajadas burlonas... ¡Estaba de vuelta!"</p>

												<p>El VITO, pequeño velero de ocho metros de Enrique Celesia, traza historias marinas en los océanos americanos. Este libro no contiene sólo relatos de viajes, es algo más, es la forma de Celesia de expresar un modo de vida que privilegia el descubrimiento y la superación de las barreras que la sociedad impone.</p>

												<p>¡ORZA VITO! invita a sus lectores a replantearse su existencia y a descubrir los verdaderos valores. Celesia no sugiere fórmulas, simplemente susurra al oído sus aventuras, cargadas de miedos y alegrías, dudas y convicciones, expresadas con una prosa a veces poética, a veces terriblemente directa y capaz de conmover hasta la piel.</p>

												<p>Quien lea estas páginas debe saber que no será el mismo al concluir. Acompañar a Celesia en la vuelta a Sudamérica significa tomar un lugar en el VITO y zarpar en un viaje iniciático, de vagabundeo romántico y osadía corsaria.</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>


							</td>
							<td align="center" valign="top">
								<a href="#" data-toggle="modal" data-target="#gpsmaravilla">G.P.S. la pequeña maravilla</a>

								<div class="modal fade" id="gpsmaravilla" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">G.P.S. la pequeña maravilla</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: Ingeniero Gerardo Boticini</h6>
												<p></p>

												<p>"Me dediqué toda mi vida al estudio de las ciencias y arte de la navegación y me hice un entusiasta de todo lo relacionado con su historia, métodos de observación y cálculo de la posición. Es decir, la posición en un desierto. Y es este el problema fundamental y más difícil que tuvo que enfrentar todo marino desde tiempos remotos, pues hubo generaciones de hombres con inmensa capacidad marinera para los cuales una violenta tempestad era algo familiar y casi rutinario, pero el desconocimiento de su posición y de los límites de la Tierra los asustaba muchísimo más. Pero apareció en escena un aparatito que nos dice al instante nuestras coordenadas dentro de un radio de pocos metros, además es portátil y su precio es más que económico. ¿Ocurrió un milagro?"</p>

												<p>Estos son algunos párrafos del prólogo de este completo libro, que nos explica todos los detalles y secretos del G.P.S. Realmente quien quiera profundizar sobre este tema tendrá aquí el material más completo."</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>
							</td>


							</td>
							<td align="center" valign="top">

								<a href="#" data-toggle="modal" data-target="#manual-de-timonel">Manual de Timonel</a>

								<div class="modal fade" id="manual-de-timonel" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Manual de Timonel</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: Ing. Gerardo Botticini</h6>
												<p></p>

												<p>El presente manual de 440 páginas cubre todos los puntos exigidos en el programa oficial de Prefectura Naval Argentina y tiene muchas ventajas que lo hacen recomendable por varias de las escuelas de navegación de nuestro país. Por un lado es un texto muy completo (que incluso toca temas de patrón y un tema fundamental que no está muy contemplado en los programas: GPS) También tiene la ventaja de haber sido escrito por un navegante argentino muy experimentado en la docencia (ya que los textos españoles tienen un costo mucho más elevado, y diferencias en el vocabulario y en algunos conceptos que están enfocados para el hemisferio norte).<br>
													La edición es reciente (año 2007) por lo que se encuentra totalmente actualizada. Además cuenta con muchos gráficos que lo hacen muy didáctico.</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>

							</td>
						</tr>
						<tr>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_7.jpg"></p>
							</td>
							<td align="center">
								<p><img alt="" border="0" class="img-fluid" src="<?php echo $link; ?>img/libro_5.jpg"></p>
							</td>
						</tr>
						<tr>
							<td align="center" height="45" valign="top">
								<a href="#" data-toggle="modal" data-target="#navegacion-de-altura">Navegación de Altura</a>

								<div class="modal fade" id="navegacion-de-altura" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Navegación de Altura<br>Ayuda para curso de Pilotos de Yate</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: Ing. Gerardo Botticini</h6>
												<p></p>
												<p>Veamos lo que nos dice el mismo autor en el prólogo del libro: "El presente texto es el resultado de la experiecia recogida durante muchos años preparando alumnos para rendir, ya sea como libres o como regulares, en los exámenes de Piloto de la PNA.<br>
													Las exigencias de la PNA son elevadas y la enseñanza de este tema que, en esencia, no es demasiado difícil, a pesar de la aureola de ciencia oculta que presenta, se torna complicada porque queriendo no perder de vista el hilo rojo conceptual de la materia se debe sortear el obstáculo de temas secundarios de cálculos con coeficientes, tablas, interpolaciones, etc. que, siendo lamentablemente inevitables, pasan a ocupar un primer plano ficticio y ensombrecen la simple y bellísima idea básica.<br>
													Los deportistas aspirantes al carnet de piloto deben aprobar un programa de examen bastante complejo para el cual no resulta fácil apoyarse en textos clásicos de uso para profesioneles de la marina militar o mercante, y los textos que se encuentran disponibles españoles, ingleses, o americanos ademós de precios elevados discrepan con las exigencias del programa.<br>
													De allí la necesidad de cubrir esta carencia procurando realizar este texto que , pretendiendo ajustarse al programa, ponga énfasis en los temas conceptualmente imprescindibles tratándolos con mayor claridad, sin perder la profundidad y el rigor científico necesario, con una didáctica esmerada basadas en muchos croquis y figuras, y que no suene a cientificismo o vanidad de autor que quiere mostrar "lo mucho que sabe".<br>
													Espero con esto hacer mi pequeña contribución a la comunidad náutica deportiva que quiere acceder a la máxima distinción y alentarlos a continuar el estudio de todo lo relacionado con la navegación, que no tiene límite, y donde nadie puede decir que lo sabe todo. G.B.</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>

							</td>
							<td align="center" height="45" valign="top">
								<a href="#" data-toggle="modal" data-target="#german-frers">Germán Frers. Viajes, diseños, regatas</a>

								<div class="modal fade" id="german-frers" tabindex="-1" role="dialog">
									<div class="modal-dialog modal-dialog-scrollable">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Germán Frers. Viajes, diseños, regatas</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-left">
												<h6 class="font-italic">Autor: Germán Frers</h6>
												<p>"Este libro conserva en la memoria de los navegantes el recuerdo de quien contribuyó a la popularización de su deporte; impulsó y corrió las primeras regatas a Mar del Plata y Río de Janeiro y luego defendió los colores nacionales en Bermuda y Fastnet; proporcionó el impulso inicial a nuestra extraordinaria industria naval; y quebró el tabú de que el mar, después de Punta del Este, contenía una gran serpiente que se comí a los osados veleristas que lo desafiaban." Comentario del libro de Hernán Álvarez Forn.</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</div>
								</div>

							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!-- /.container -->
		<!-- FOOTER -->
		<?php
		include '../common/footer.php';
		?>
	</main>
</body>

</html>