<style type="text/css">
#paginaContacto{
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 100%
}

#redesSociales{

    width: 40%;
    margin-top: 20px

}

#tituloRedes,#tituloWa{

    font-weight: bold;
    font-size: 20px;
    margin-top: 10px
}

#iconosRedes{

    display: flex;


}


#formulario{

    width: 60%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding-left: 5%;
    margin-bottom: 30px
}


label {
    display:block;
    margin-top:7px;
    letter-spacing:1px;
}


 
input, textarea {
    width:380px;
    height:27px;
    background:#D5D5D5;
    border:2px solid #f6f6f6;
    padding:10px;
    margin-top:5px;
    font-size:10px;
    color:#ffffff;
}
 
textarea {
    height:110px;
}
 
#submit {
    width: 85px;
    height: 45px;
    border: none;
    font-size: 15px;
    cursor: pointer;
    background: #886AFA !important;
    display: flex;
    justify-content: center;
    align-items: center;
}

@media (max-width: 1000px) {
#paginaContacto{
    display: flex;
    flex-direction: column;
    
}

#formulario {
    width: 100%;
    align-items: center;
    padding-left: 0%;
    margin-bottom: 10px;
}

#submit{
    margin-top: 20px
}

#redesSociales{
    width: 100%;
}

#iconosRedes,#iconoWa{
    display: flex;
    justify-content: center;
}
#tituloRedes, #tituloWa {
    text-align: center;
}

  }

	
</style>
<div class="container" id="paginaContacto">
  <form id="formulario" action="contact.php" method="post">
    <label for="nombre">Nombre:</label>
     <input id="nombre" type="text" name="nombre" placeholder="Nombre y Apellido" required="" />
     <label for="email">Email:</label>
     <input id="email" type="email" name="email" placeholder="ejemplo@correo.com" required="" />
     <label for="mensaje">Mensaje:</label>
     <textarea id="mensaje" name="mensaje" placeholder="Mensaje" required=""></textarea>
     <input id="submit" type="submit" name="submit" value="Enviar" />
   </form>
   <div id="redesSociales">
    
     <div id="instaFace">
       <p id="tituloRedes">Seguinos en las redes</p>
       <div id="iconosRedes">
              <a style="margin-right: 10px" href="https://www.facebook.com/Navemocion" rel="noopener noreferrer" target="_blank" class="fa fa-facebook"></a>
              <a class="fa fa-instagram" href="https://www.instagram.com/ubatimonelcursosnavemocion/" rel="noopener noreferrer" target="_blank"></a>
       </div>
     </div>
       <div id="Wa">
       <p id="tituloWa">Contactanos vía Whatsapp</p>
       <div id="iconoWa">
            <a class="fa fa-whatsapp" href="https://api.whatsapp.com/send?phone=+541157824263&amp;abid=+541157824263" rel="noopener noreferrer" target="_blank"></a>
       </div>
     </div>

   </div>

</div>

