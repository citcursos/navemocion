<!doctype html>
<html lang="es">

<head>
	<?php
	include 'common/head.php';
	?>
</head>

<body>
	<header style="height: 100px;">
		<?php
		include 'common/nav.php';
		?>
	</header>
	<main role="main">


		<!-- Marketing messaging and featurettes
  ================================================== -->
		<!-- Wrap the rest of the page in another container to center all the content. -->

		<div class="container img-header">
			<div class="col-md-6 px-0">
				<h1 class="display-4">Viajes</h1>
			</div>
		</div>

		<div class="container">
			<div class="row mt-3">
				<div class="col-md-12">
					<p style="padding-left: 60px;">A lo largo de los cursos de Timonel, Patrón y Navegación Astronómica se realizan distintos cruceros de instrucción a diversos puertos uruguayos, brasileros, y de la Costa Argentina y los lagos del Sur.</p>

					<p style="padding-left: 60px;">Si te interesa participar de estas navegaciones, consultá en:&nbsp;<br>
						<span id="cloakbb655c91f9fad10e1de501bf0b7ebf7d"><a href="mailto:info@navemocion.com">info@navemocion.com</a></span>
						<script type="text/javascript">
							document.getElementById('cloakbb655c91f9fad10e1de501bf0b7ebf7d').innerHTML = '';
							var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
							var path = 'hr' + 'ef' + '=';
							var addybb655c91f9fad10e1de501bf0b7ebf7d = '&#105;nf&#111;' + '&#64;';
							addybb655c91f9fad10e1de501bf0b7ebf7d = addybb655c91f9fad10e1de501bf0b7ebf7d + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
							var addy_textbb655c91f9fad10e1de501bf0b7ebf7d = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
							document.getElementById('cloakbb655c91f9fad10e1de501bf0b7ebf7d').innerHTML += '<a ' + path + '\'' + prefix + ':' + addybb655c91f9fad10e1de501bf0b7ebf7d + '\'>' + addy_textbb655c91f9fad10e1de501bf0b7ebf7d + '<\/a>';
						</script>&nbsp;, y anotate para reservar tu lugar en cada una de las tripulaciones.
					</p>

					<p>&nbsp;</p>
				</div>
			</div>
			<div class="row mt-3">
				<div class="col-md-4">
					<div class="container text-center">
						<div class="row my-2">
							<div class="col-md-12">
								<img alt="" class="rounded float-left mr-3" height="35" src="<?php echo $link; ?>img/argentina-peque.fw.png" width="54">
								<h4><strong>ARGENTINA</strong></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<ul class="list-group">
									<li class="list-group-item">
										<a href="<?php echo $link; ?>viajes/argentina/quilmes.php">Quilmes</a>
									</li>
									<li class="list-group-item">
										<a href="<?php echo $link; ?>viajes/argentina/puerto-madryn.php">Puerto Madryn</a>
									</li>
									<li class="list-group-item">
										<a href="<?php echo $link; ?>viajes/argentina/general-lavalle.php">General Lavalle</a>
									</li>
									<li class="list-group-item">
										<a href="<?php echo $link; ?>viajes/argentina/mar-del-plata.php">Mar del Plata</a>
									</li>
									<li class="list-group-item">
										<a href="<?php echo $link; ?>viajes/argentina/lago-futalaufquen.php">Lago Futalaufquen</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="container text-center">
						<div class="row my-2">
							<div class="col-md-12">
								<img alt="" class="rounded float-left mr-3" height="35" src="<?php echo $link; ?>img/brasil-peque.fw.png" width="53">
								<h4><strong>BRASIL</strong></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<ul class="list-group">
									<li class="list-group-item">
										<a href="<?php echo $link; ?>viajes/brasil/florianapolis.php">Florianápolis</a>
									</li>
									<li class="list-group-item">
										<a href="<?php echo $link; ?>viajes/brasil/angra-dos-reis.php">Angra dos Reis</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="container text-center">
						<div class="row my-2">
							<div class="col-md-12">
								<img alt="" class="rounded float-left mr-3" height="35" src="<?php echo $link; ?>img/uruguay.fw.png" width="53">
								<h4><strong>URUGUAY</strong></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<ul class="list-group">
									<li class="list-group-item">
										<a href="<?php echo $link; ?>viajes/uruguay/la-paloma-punta-del-este.php">La Paloma-Punta del Este</a>
									</li>
									<li class="list-group-item">
										<a href="<?php echo $link; ?>viajes/uruguay/sauce.php">Sauce</a>
									</li>
									<li class="list-group-item">
										<a href="<?php echo $link; ?>viajes/uruguay/montevideo.php">Montevideo</a>
									</li>
									<li class="list-group-item">
										<a href="<?php echo $link; ?>viajes/uruguay/piriapolis.php">Piriápolis</a>
									</li>
									<li class="list-group-item">
										<a href="<?php echo $link; ?>viajes/uruguay/colonia-riachuelo.php">Colonia-Riachuelo</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div class="row mt-3">
				<div class="col-md-4">
					<div class="container text-center">
						<div class="row my-2">
							<div class="col-md-12">
								<img alt="" class="rounded float-left mr-3" height="35" src="<?php echo $link; ?>img/coracia.fw.png" width="53">
								<h4><strong>CROACIA</strong></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<ul class="list-group">
									<li class="list-group-item">Split</li>
									<li class="list-group-item">Corkula</li>
									<li class="list-group-item">Vis</li>
									<li class="list-group-item">Hvar</li>
									<li class="list-group-item">Stari Grad </li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="container text-center">
						<div class="row my-2">
							<div class="col-md-12">
								<img alt="" class="rounded float-left mr-3" height="33" src="<?php echo $link; ?>img/grecia.fw.png" width="50">
								<h4><strong>GRECIA</strong></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		</div><!-- /.container -->


		<!-- FOOTER -->
		<?php
		include 'common/footer.php';
		?>
	</main>
</body>

</html>