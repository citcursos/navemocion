<!doctype html>
<html lang="es">

<head>
  <?php
  include 'common/head.php';
  ?>
</head>

<body>
  <header style="height: 95px;">
    <?php
    include 'common/nav.php';
    ?>
  </header>

  <main role="main">
    <?php
    include 'common/carousel.php';
    ?>

    <!-- Marketing messaging and featurettes
  ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

    <p id="headerCursos">Nuestros Cursos</p>
      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4 tarjetaCurso">      
            <img class="imgCursos" src="http://localhost/navemocion/img/presentacion3.jpg" alt="">
          <h2 class="tituloCurso">Conductor Naútico</h2>
          <div class="detalles">
          <p class="duracionCurso">Duración: <br> <span class="bold">1 mes</span></p>
          <p class="fechaCurso">Fecha Inicio: <br> <span class="bold">Marzo/Junio/Agosto/Noviembre</span></p> 
          </div>
          <p><a class="btn btn-secondary" href="http://localhost/navemocion/cursos/curso-conductor-nautico.php" role="button">Ver Detalle</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4 tarjetaCurso blanca">      
           <img class="imgCursos" src="http://localhost/navemocion/img/barco2.jpg" alt="">
          <h2 class="tituloCurso">Timonel de yate <br> a vela y/o motor</h2>
          <div class="detalles">
          <p class="duracionCurso">Duración: <br> <span class="bold">5 meses</span></p>
          <p class="fechaCurso">Fecha Inicio: <br> <span class="bold">Agosto</span></p> 
          </div>
          <p><a class="btn btn-secondary" href="http://localhost/navemocion/cursos/curso-timonel-de-yate.php" role="button">Ver Detalle</a></p>
        </div>
        <div class="col-lg-4 tarjetaCurso">      
            <img class="imgCursos" src="http://localhost/navemocion/img/patron1.jpg" alt="">
          <h2 class="tituloCurso">Curso de Patrón <br> de Yate a Vela y Motor</h2>
          <div class="detalles">
          <p class="duracionCurso">Duración: <br> <span class="bold">4 meses</span></p>
          <p class="fechaCurso">Fecha Inicio: <br> <span class="bold">Marzo/Agosto</span></p> 
          </div>
          <p><a class="btn btn-secondary" href="http://localhost/navemocion/cursos/curso-patron-de-yate.php" role="button">Ver Detalle</a></p>
        </div>
        
         <div class="col-lg-4 tarjetaCurso blanca">      
           <img class="imgCursos" src="http://localhost/navemocion/img/sextante.jpg" alt="">
          <h2 class="tituloCurso">Navegación Astronómica <br>(Piloto de Yate)</h2>
          <div class="detalles">
          <p class="duracionCurso">Duración: <br> <span class="bold">4 meses</span></p>
          <p class="fechaCurso">Fecha Inicio: <br> <span class="bold">Marzo/Agosto</span></p> 
          </div>
             <p><a class="btn btn-secondary" href="http://localhost/navemocion/cursos/curso-navegacion-astronomica.php" role="button">Ver Detalle</a></p>
        </div>
          <div class="col-lg-4 tarjetaCurso">      
            <img class="imgCursos" src="http://localhost/navemocion/img/mapa-argentina-clima_1.png" alt="">
          <h2 class="tituloCurso">Meteorología</h2>
          <div class="detalles">
          <p class="duracionCurso">Duración: <br> <span class="bold">2 meses</span></p>
          <p class="fechaCurso">Fecha Inicio: <br> <span class="bold">Mayo/Octubre</span></p> 
          </div>
          <p><a class="btn btn-secondary" href="http://localhost/navemocion/cursos/curso-meteorologia.php" role="button">Ver Detalle</a></p>
        </div>
        <div class="col-lg-4 tarjetaCurso blanca">      
           <img class="imgCursos" src="http://localhost/navemocion/img/gpssaling.fw.png" alt="">
          <h2 class="tituloCurso">G.P.S - Navegación <br> Satelital</h2>
          <div class="detalles">
          <p class="duracionCurso">Duración: <br> <span class="bold">...</span></p>
          <p class="fechaCurso">Fecha Inicio: <br> <span class="bold">Mayo/Octubre</span></p> 
          </div>
             <p><a class="btn btn-secondary" href="http://localhost/navemocion/cursos/curso-gps-navegacion-satelital.php" role="button">Ver Detalle</a></p>
        </div>
      </div><!-- /.row -->

      <div class="clasesParticulares jumbotron">
        <p class="tituloCP">
          Clases Particulares
        </p>
        <p class="textoCp">Para aprender acerca de los temas que más interesan en vela o motor -incluso puede ser en su propia embarcación- en los días y horarios que más convengan</p>
        <p><a class="btn btn botonCP" href="http://localhost/navemocion/cursos/curso-clases-particulares.php" role="button">Ver Detalle</a></p>
      </div>

      <!-- START THE FEATURETTES -->

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It’ll blow your mind.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
          <svg class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="500" height="500" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x500">
            <title>Placeholder</title>
            <rect width="100%" height="100%" fill="#eee" /><text x="50%" y="50%" fill="#aaa" dy=".3em">500x500</text>
          </svg>
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7 order-md-2">
          <h2 class="featurette-heading">Oh yeah, it’s that good. <span class="text-muted">See for yourself.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5 order-md-1">
          <img src="img/presentacion1.jpg" class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" alt="...">
        </div>
      </div>
      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">Oh yeah, it’s that good. <span class="text-muted">See for yourself.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
          <img src="img/presentacion2.jpg" class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" alt="...">
        </div>
      </div>
      <hr class="featurette-divider">


      <div class="row featurette">
        <div class="col-md-7 order-md-2">
          <h2 class="featurette-heading">Oh yeah, it’s that good. <span class="text-muted">See for yourself.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5 order-md-1">
          <img src="img/presentacion3.jpg" class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" alt="...">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7 align-self-center">
          <div class="container">
            <div class="row justify-content-md-center">
              <div class="col-md-auto">
                <a href="<?php echo $link; ?>pronosticos-y-mareas.php" class="fa fa-sun-o"></a>
              </div>
              <div class="col-md-8">
                <h4>Pronósticos y mareas</h4>
                <p>Preveé tu viaje con los mejores pronósticos on-line</p>
              </div>
            </div>
            <div class="row justify-content-md-center">
              <div class="col-md-auto">
                <a href="<?php echo $link; ?>librerio.php" class="fa fa-book"></a>
              </div>
              <div class="col-md-8">
                <h4>Librerío</h4>
                <p>Accedé a la colección de libros relacionados con la navegación</p>
              </div>
            </div>
            <div class="row justify-content-md-center">
              <div class="col-md-auto">
                <a href="<?php echo $link; ?>enlaces.php" class="fa fa-link"></a>
              </div>
              <div class="col-md-8">
                <h4>Enlaces</h4>
                <p>Todas las páginas afines a nuestra forma de vida</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-5">
          <iframe align="middle" frameborder="0" width="100%" height="400px" scrolling="no" src="https://embed.windy.com/embed2.html?lat=-34.832&amp;lon=-57.327&amp;zoom=7&amp;level=surface&amp;overlay=wind&amp;menu=&amp;message=&amp;marker=&amp;calendar=&amp;pressure=&amp;type=map&amp;location=coordinates&amp;detail=&amp;detailLat=-35.528&amp;detailLon=-58.447&amp;metricWind=default&amp;metricTemp=default&amp;radarRange=-1" width="334"></iframe>
        </div>
      </div>

      <hr class="featurette-divider">
      <div class="row featurette">
        <div class="col-md-12">
          <div id="instafeed"></div>
        </div>
      </div>

      <hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->

    </div><!-- /.container -->


    <!-- FOOTER -->
    <?php
    include 'common/footer.php';
    ?>
  </main>
</body>

</html>