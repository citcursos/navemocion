<!doctype html>
<html lang="es">
  <head>
      <?php
          include 'common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include 'common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-6 px-0">
          <h1 class="display-4">Contacto</h1>
        </div>
      </div>

      <div class="container">
          <?php
          include 'contacto/formulario.php';
      ?>
      </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include 'common/footer.php';
      ?>
    </main>
  </body>
</html>
