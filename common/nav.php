<nav class="navbar navbar-expand-md navbar-light fixed-top bg-light">
    <a class="navbar-brand" href="<?php echo $link; ?>"><img src="<?php echo $link; ?>img/logonav.png" style="max-width: 170px;" alt="Navemoción"> </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo $link; ?>cursos.php">CURSOS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo $link; ?>viajes.php">VIAJES</a>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ALUMNOS</a>
            <div class="dropdown-menu">
            <a class="dropdown-item" href="<?php echo $link; ?>alumnos/apuntes.php" >APUNTES</a>
            <a class="dropdown-item" href="<?php echo $link; ?>alumnos/mano-de-risas.php">MANO DE RISAS</a>
            <a class="dropdown-item" href="<?php echo $link; ?>alumnos/grupo-patronia.php">GRUPO PATRONIA</a>
            <a class="dropdown-item" href="<?php echo $link; ?>alumnos/ecologia.php">ECOLOGÍA</a>
            <a class="dropdown-item" href="<?php echo $link; ?>alumnos/articulos.php">ARTÍCULOS</a>
            </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo $link; ?>astillero.php">ASTILLERO</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo $link; ?>contacto.php">CONTACTO</a>
        </li>
      </ul>
      <ul class="navbar-nav ml-md-auto">
        <li class="nav-item">
          <a href="https://www.facebook.com/Navemocion" rel="noopener noreferrer" target="_blank" class="fa fa-facebook"></a>
        </li>
        <li class="nav-item">
         <a class="fa fa-instagram" href="https://www.instagram.com/ubatimonelcursosnavemocion/" rel="noopener noreferrer" target="_blank"></a> 
        </li>
        <li class="nav-item">
          <a class="fa fa-whatsapp" href="https://api.whatsapp.com/send?phone=+541157824263&abid=+541157824263" rel="noopener noreferrer" target="_blank"></a>
        </li>
      </ul>
    </div>
  </nav>