<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Viajes - Quilmes</h1>
        </div>
      </div>
      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 text-center">
							<img alt="" class="img-fluid" style="width: 175px;"src="<?php echo $link; ?>img/800px-Flag_of_Argentina.svg.png"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Costa y Mar Argentino:</h4>
							<h4>Quilmes</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
          <div align="center" class="bluetext">
						<p>Esta navegación es la primera que podemos intentar, antes de iniciar cruceros de más largo aliento. Pero no por ser corta ( 3 a 4 horas de navegación para llegar ) deja de ser interesante, siendo una oportunidad para refrescar o incorporar conocimientos de navegación de todo tipo, y para empezar a foguearnos.</p>

						<p>Zarpamos a la mañana, reconocemos la entrada del Club Náutico Quilmes (el que podemos recorrer) y estamos volviendo al atardecer.</p>

						<p><img alt="" height="404" src="<?php echo $link; ?>img/muma.jpg" width="274"></p>

						<div class="commontext"><br>
						En nuestra navegación divisaremos de un extremo a otro la ciudad de Buenos Aires, pasaremos frente al puerto de Buenos Aires, y cruzaremos los canales de acceso al mismo "Norte" y "Sur".&nbsp;</div>

						<div class="commontext">En latitud: 34° 40.927´ (S) y longitud: 58° 13.808´ (w) se encuentra la toma de agua exterior de Bernal, punto notable muy importante en esta travesía, ya que a partir de la misma ya podemos arrumbar hacia la costa buscando la boya indicadora de corriente latitud 34° 41.745´ (S) y longitud: 58° 13.520´ (W), que se encuentra justo en la boca del canal de acceso.
						<p><b>Cartas a utilizar:</b> H-155 a H h-118<br>
						<b>Instrumental a utilizar:</b> navegador satelital, pínula y sextante.&nbsp;</p>
						</div>
					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../../common/footer.php';
      ?>
    </main>
  </body>
</html>
