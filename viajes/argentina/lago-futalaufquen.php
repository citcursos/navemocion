<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Viajes - Lago Futalaufquen</h1>
        </div>
      </div>
      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 text-center">
							<img alt="" class="img-fluid" style="width: 175px;"src="<?php echo $link; ?>img/800px-Flag_of_Argentina.svg.png"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Costa y Mar Argentino:</h4>
							<h4>Lago Futalaufquen</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
        <div class="bluetext">
<div class="bluetext" align="center"><b><i>(Nuestra próxima navegación de instrucción, está prevista para enero de 2004)</i></b></div>
<br>
<div class="bluetext">Estuvimos navegando en enero de 2003, en el Lago Futalaufquen, situado dentro del Parque Nacional "Los Alerces", en la Provincia de Chubut, zarpando de Puerto Limonao. Para navegar aquí hay que tener muy en cuenta que esa zona es el "reino de la racha y del borneo", por lo que hay que estar muy atento a cualquier indicio, con la escota en la mano y el timón muy ágil.</div>
<br>
<div class="commontext">La navegación se realiza por observaciones de la costa y experiencia, ya que no existen cartas náuticas de la zona. Nuestra navegación se desarrolló en el Phantom 19 "Sur" de Ricardo Regis. El clima no nos ayudó, estaba nublado y ventoso (con rachas de 40 nudos, ˇy pensar que cuando el río está planchado decimos que es un lago!), por lo tanto nuestra navegación tuvo que ser con velamen reducido. En un principio pensamos usar la mayor rizada, pero ésta sufrió un rifadura y tuvimos que conformarnos con la vela que nos quedó disponible, un genoa 3, que resultó ser mucho pańo</div>
<div class="commontext" style="text-align: center;">cuando nos apretaban las rachas.</div>
<div class="commontext" style="text-align: center;">&nbsp;</div>
<div class="commontext" style="text-align: center;"><img src="<?php echo $link; ?>img/futa1.jpg" width="378" height="238"></div>
<div class="bluetext" style="text-align: center;" align="center"><i>Lago Futalaufquen: del araucano Futa significa grande y Laufquen significa lago. La profundidad media de este lago es de 140 metros, su superficie de 1825 hectáreas, su altura sobre el nivel del mar de 510 metros, los peces que habitan en él son salmónidos y pejerrey patagónico.</i></div>
<br><br>
<div class="bluetext" style="text-align: center;" align="center">&nbsp;</div>
<div class="bluetext" style="text-align: center;" align="center"><i><img src="<?php echo $link; ?>img/futa2.jpg" width="306" height="207"></i></div>
<div class="bluetext" style="text-align: center;" align="center"><i>En un marco imponente y con aguas cristalinas, fue una experiencia fuerte y enriquecedora.</i></div>
<br><br>
<p><img style="display: block; margin-left: auto; margin-right: auto;" src="<?php echo $link; ?>img/futa3.jpg" width="333" height="176"></p>
</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../../common/footer.php';
      ?>
    </main>
  </body>
</html>
