<!doctype html>
<html lang="es">

<head>
  <?php
  include '../../common/head.php';
  ?>
</head>

<body>
  <header style="height: 100px;">
    <?php
    include '../../common/nav.php';
    ?>
  </header>
  <main role="main">
    <div class="container img-header">
      <div class="col-md-12 px-0">
        <h1 class="display-4">Viajes - General Lavalle</h1>
      </div>
    </div>
    <div class="container grey">
      <div class="row">
        <div class="col-md-12 align-self-center">
          <div class="row justify-content-md-center bg-light">
            <div class="col-md-4 text-center">
              <img alt="" class="img-fluid" style="width: 175px;" src="<?php echo $link; ?>img/800px-Flag_of_Argentina.svg.png" />
            </div>
            <div class="col-md-8 align-self-center">
              <h4>Costa y Mar Argentino:</h4>
              <h4>General Lavalle</h4>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-5">
        <div class="col-md-1"></div>
        <div class="col-md-10">
          <div class="bluetext">
            <div class="bluetext">Estamos armando las tripulaciones para el próximo viaje.
              <p>Participá de las charlas informativas, para realizar la navegación a este puerto que es la escala natural de que disponen todas las embarcaciones deportivas antes de abandonar el Río de la Plata y adentrarse en las aguas del Atlántico Sur.</p>
            </div>
            <p>&nbsp;</p>
            <div class="commontext">Informes e inscripción al&nbsp;<b>4701-4410</b>,&nbsp;más información&nbsp;en&nbsp;<span id="cloakbc89d4c8bc69cf12d89c571e25e5f2b8"><a href="mailto:info@navemocion.com">info@navemocion.com</a></span>
              <script type="text/javascript">
                document.getElementById('cloakbc89d4c8bc69cf12d89c571e25e5f2b8').innerHTML = '';
                var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                var path = 'hr' + 'ef' + '=';
                var addybc89d4c8bc69cf12d89c571e25e5f2b8 = '&#105;nf&#111;' + '&#64;';
                addybc89d4c8bc69cf12d89c571e25e5f2b8 = addybc89d4c8bc69cf12d89c571e25e5f2b8 + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
                var addy_textbc89d4c8bc69cf12d89c571e25e5f2b8 = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
                document.getElementById('cloakbc89d4c8bc69cf12d89c571e25e5f2b8').innerHTML += '<a ' + path + '\'' + prefix + ':' + addybc89d4c8bc69cf12d89c571e25e5f2b8 + '\'>' + addy_textbc89d4c8bc69cf12d89c571e25e5f2b8 + '<\/a>';
              </script>
            </div>
            <p>&nbsp;</p>
          </div>
        </div>
        <div class="col-md-1"></div>
      </div>
    </div>
    <!-- /.container -->
    <!-- FOOTER -->
    <?php
    include '../../common/footer.php';
    ?>
  </main>
</body>

</html>