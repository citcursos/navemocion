<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Viajes - Mar del Plata</h1>
        </div>
      </div>
      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 text-center">
							<img alt="" class="img-fluid" style="width: 175px;"src="<?php echo $link; ?>img/800px-Flag_of_Argentina.svg.png"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Costa y Mar Argentino:</h4>
							<h4>Mar del Plata</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
        <div class="bluetext">
<div class="bluetext" align="center">&nbsp;</div>
<div class="bluetext" align="left">
<div align="center"><big><b>Viaje a Buenos Aires - Mar del Plata </b></big></div>
<p class="commontext" align="left">Aunque el pronóstico preveía para esos días vientos del este y noreste, tuvimos vientos leves del sudeste y algunas calmas. Por lo que no nos quedó más opción que usar el viento de sentina del "San Miguel" (motor Nani diesel de 14 h.p.), desde la zarpada hasta practicamente Punta Piedras. Aprovechamos esta parte de la navegación para cargar bien las baterías, y hacer funcionar la heladera del "San Miguel", una verdadera maravilla fabricada e instalada por Vladimiro Serafini, y que en media hora genera dentro del gabinete una temperatura de - 15° (en un momento el termómetro de la heladera marcaba - 33°). ˇEso de tener alimentos y bebidas congeladas en un viaje de este tipo es un lujo que uno agradece con una sonrisa en los labios! <br><br>En esta travesía es conveniente ir comunicándose con las distintas dependencias de prefectura: Quilmes (L5O), La Plata (L5F), General Lavalle (L5C), Mar del Tuyú (L9U), Villa Gesell (L9S), Mar del Plata (L2O y L2U). <br><br><img src="<?php echo $link; ?>img/100_0053m.jpg" align="right" hspace="10" vspace="10"> Al pasar frente a Atalaya, tuvimos que alterar nuestra derrota prefijada, navegando más alejados de la costa. Ya que justo en ese día y horario, estaban previstas prácticas de tiro desde la costa (de acuerdo a lo informado en los "Avisos a los Navegantes" (que habíamos consultado antes de zarpar). <br><br>Al llegar a Punta Piedras comenzamos a tener la posibilidad de aprovechar el viento leve, ya que a partir de allí nuestro rumbo iba a ser mucho más al sur que en la pierna anterior, y el viento nos incidía en una ceńida primero y luego de través aumentando de intensidad. <br><br>Durante toda la noche pudimos atravesar La Bahía de Samborombón navegando a vela y a más de 7 nudos. Esa misma noche ya comenzamos a percibir ecos del mar cercano en la espuma blanca y las fosforescentes noctilucas. <br><br>En navegación nocturna siempre utilizamos arneses y línea de vida, como se ve en la foto. <br><br>Todavía siendo de noche pasamos el faro San Antonio cuya altura es de 63 mts. (S 36° 185' W 56° 463') con su luz blanca que tiene un destello cada 17 segundos. <br><br>Luego al pasar frente a Santa Teresita nos pegamos un susto cuando casi pasamos por encima de una boya de bastante tamańo que seguramente tenía que ver con algún implemento de pesca, fue algo repentino, cuando vimos la boya ya la estábamos pasando al lado….</p>
<p style="text-align: center;"><u><i>ENUMERACIÓN DE FAROS:</i></u> en un viaje a mar del plata se divisan todos estos faros: <br>- Faro de Punta Piedras b. des c/ 9 s. alt: 45 mts. (S 35° 272' W 57° 084'). <br>- Faro San Antonio -b. des c/ 17 s.- alt: 63 mts. (S 36° 185' W 56° 463'). <br><br><img src="<?php echo $link; ?>img/100_0166m.jpg" vspace="10"> <br><span style="color: #00659c;">- Faro Punta Médanos -b. des (5) c/ 40 s. -alt: 59 mts. (S 36° 532' W 56° 407')</span> <br><br><br><img src="<?php echo $link; ?>img/100_0071m.jpg" vspace="10"> <br><span style="color: #00659c;">- FARO PUNTA MÉDANOS -B. DES (5) C/ 40 S. -ALT: 59 MTS. (S 36° 532' W 56° 407')</span> <br><br><br><img src="<?php echo $link; ?>img/100_0155m.jpg" align="middle" vspace="10"> <br><span style="color: #00659c;">- FARO QUERANDÍ -B. DES (5) C/ 26 S. -ALT: 65 MTS. (S 37° 287' W 57° 075')</span> <br><br><br></p>
<p style="text-align: center;" align="center"><span style="color: #00659c; font-size: large;">Viaje Mar del Plata - Buenos Aires</span></p>
<p style="text-align: center;"><u><i>GALERÍA DE FOTOS:</i></u> <br><img src="<?php echo $link; ?>img/100_0129m.jpg" vspace="10"> <br>Aquí vemos el momento en que estamos por abandonar al puerto deportivo de Mar del Plata, con el puente giratorio en movimiento. <br><br><br><img src="<?php echo $link; ?>img/100_0135m.jpg" vspace="10"> <br>Último adios al Cristo que está en la Punta de la Escollera Sur - es un punto notable muy importante para recalar en este puerto - <br><br><br><img src="<?php echo $link; ?>img/100_0137m.jpg" vspace="10"> <br>Aquí vemos la baliza roja, en la Escollera Norte. <br><br><br><img src="<?php echo $link; ?>img/100_0140m.jpg" vspace="10"> <br>Nos alejábamos del puerto… <br><br><br><img src="<?php echo $link; ?>img/100_0141m.jpg" vspace="10"> <br>Estudiando las cartas <br><br><br><img src="<?php echo $link; ?>img/100_0167m.jpg" vspace="10"> <br>Alfredo lavando los platos <br><br><br><img src="<?php echo $link; ?>img/100_0175m.jpg" vspace="10"> <br>Instalando el timón de viento "Pilomat" <br><br><br><img src="<?php echo $link; ?>img/100_0180m.jpg" vspace="10"> <br>Nuestra tripulante "Camila" - timón de viento - en acción <br><br><br><img src="<?php echo $link; ?>img/100_0173m.jpg" vspace="10"> <br>En esta foto podemos apreciar que estábamos navegando frente a la enfilación entre El Faro San Antonio y la baliza que permite acceder al fondeadero de San Clemente - junto con el Puerto de General Lavalle, es la otra única posibilidad de recalar que nos ofrece esta costa -. <br><br><br><img src="<?php echo $link; ?>img/100_0210m.jpg" vspace="10"> <br>Guardia nocturna con todas las pilas <br><br><br><img src="<?php echo $link; ?>img/100_0245m.jpg" vspace="10"> <br>ˇQue serios están todos! <br><br><br><img src="<?php echo $link; ?>img/100_0256m.jpg" vspace="10"> <br>Ingresando al Puerto de Núńez, el viaje de vuelta lo realizamos en 58 horas <br><br><br><br><br></p>
<div class="bluetext" align="left">Estamos armando las tripulaciones para el próximo viaje. Participá de las charlas informativas.</div>
<div class="commontext"><b>Informes e inscripción al 4701-4410, ó en </b><span id="cloak0c10216f59614a31d2919f818eaa0b7e"><a href="mailto:info@navemocion.com">info@navemocion.com</a></span><script type="text/javascript">
				document.getElementById('cloak0c10216f59614a31d2919f818eaa0b7e').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addy0c10216f59614a31d2919f818eaa0b7e = '&#105;nf&#111;' + '&#64;';
				addy0c10216f59614a31d2919f818eaa0b7e = addy0c10216f59614a31d2919f818eaa0b7e + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
				var addy_text0c10216f59614a31d2919f818eaa0b7e = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';document.getElementById('cloak0c10216f59614a31d2919f818eaa0b7e').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy0c10216f59614a31d2919f818eaa0b7e + '\'>'+addy_text0c10216f59614a31d2919f818eaa0b7e+'<\/a>';
		</script></div>
<div class="commontext">&nbsp;</div>
<div class="commontext"><img style="display: block; margin-left: auto; margin-right: auto;" src="<?php echo $link; ?>img/marpla2.jpg" width="329" height="290"><br><br><br>
<p align="left">Siempre hay una excusa para no largar amarras, que uno ya es viejo o demasiado joven, que el barco no sirve o es muy pequeño..... <br>Lo cierto es que casi siempre el temor es hijo de la ignorancia; un crucero en barco, bien organizado, termina siendo tan seguro como cualquier otro viaje. Pero por supuesto, que embarcarse, es siempre mucho más emocionante. Ya que al soltar amarras uno sabe que abandona sus rutinas de tierra para embarcarse en otra dimensión del tiempo, para empezar a vivir al ritmo de la naturaleza. Un crucero a vela a mar del plata, como el realizado a partir del 21 de febrero de 2003, por egresados de los cursos de timonel y patrón de la u.c.a. Y de la u.b.a. Junto a su profesor, gabriel buduba, en un velero sailor 1000 (diez metros de eslora), se constituyó en una experiencia cargada de energía, sana camaradería, y contemplación de las bellezas del mar. <br>Ese febrero fue un mes muy atípico en cuanto a la meteorología, no obstante gracias a la interpretación de cartas del tiempo que llevaban a bordo, y a la colaboración del meteorólogo de la armada norberto cattaneo (a cargo de cursos de meteorología), la navegación fue realizada sin ningún tipo de inconvenientes. <br>El viaje de ida san isidro/ mar del plata, realizado navegando en forma constante, les insumió 54 horas, y el de vuelta 63. Durante todas esas horas la vida a bordo se organiza de manera que hay dos grupos (guardia "a" y guardia "b") que cada seis horas se van rotando en la conducción y maniobra de la nave, mientras el otro grupo descansa. Al amanecer, coincidiendo con el cambio de guardia, toda la tripulación se junta en cubierta a desayunar y contemplar la salida del sol. <br>Durante el día se van realizando las tareas típicas de la navegación: posicionamiento en cartas náuticas, cambio de velas, evaluación meteorológica...esto matizado con alguna "picadita", y algún chapuzón en aguas verde esmeralda. <br>Luego de contemplar la puesta del sol, comienza otra etapa: la navegación nocturna, en la que además de poder apreciar toda la majestuosidad de un cielo estrellado, la superficie del mar se va haciendo fosforescente a medida que el barco avanza y la estela se ilumina, es que ya se empiezan a ver las noctilucas (minúsculos organismos marinos, que tienen la misma propiedad que las luciérnagas). <br><br>Habitualmente un grupo participa en el viaje de ida y otro en el de vuelta (hay que disponer de por lo menos cuatro días -un fin de semana y dos días más- para participar). <br><br>También existe la opción de realizar la navegación desde Mar del Plata a Necochea. <br><br>Se realizarán charlas y trabajos sobre las cartas náuticas con todos los interesados.</p>
<p align="left"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="<?php echo $link; ?>img/marpla1.jpg" width="290" height="215"></p>
<p align="left">El <b><i>Puerto de Mar del Plata</i></b> se encuentra a 2.300 metros al sur de la baliza Cabo Corrientes. Está cerrado por dos escolleras, que presentan su boca de acceso de 300 metros de ancho hacia al NE, reducida por la formación de un banco de arena sobre el norte de la escollera sur, que disminuye la profundidad del acceso. Sobre la escollera sur, se encuentra emplazado el monumento al Cristo, de aproximadamente nueve metros de altura, que es fácilmente identificable. Para acceder al puerto de Mar del Plata, se ha materializado una enfilación entre dos balizas, siendo la derrota de 238°/058°. La recalada en Cabo Corrientes de día o de noche, no ofrece dificultades. Constituyen buenos puntos para tomar marcaciones o determinar waypoints el edificio "Cosmos" (Lat: 38° 00.55´(S) y Long: 57° 32.57 (W), con una altura de 117 metros, pintado de blanco y gris. Sobre la terraza del edificio Mirador Cabo Corrientes (Lat: 38° 01´ (S) y Long: 57° 31,5´ (W), se encuentra la Baliza Cabo Corrientes. De día, la gran cantidad de puntos notables hace que la recalada resulte segura. De noche, las luces de la ciudad dificultan la identificación del balizamiento luminoso, por lo que se hace aconsejable la utilización de radar o G.P.S.</p>
<p align="left"><b><i>El Puerto de Quequén (Necochea):</i></b> Cuenta con dos escolleras; la sur, que es la principal y corre con una orientación NW a SE con una longitud de 1.192 metros y la norte, que corre con una orientación N-S, con una longitud de 551 metros. Entre ambas dejan un paso de 195 metros, con un ancho de canal navegable de 100 metros. La enfilación de acceso tiene un valor de 291°. El avistaje de Quequén de día desde cualquier dirección se ve facilitado por la presencia de los elevadores de granos del puerto con su construcción alta que los proyecta sobre el horizonte; luego se puede localizar el faro Quequén. De noche, las luces rojas de señalización aérea de los elevadores, con alturas de 40 a 70 metros y la luz del faro conducen con seguridad a la rada de este puerto. En cualquier circunstancia, el radar es un elemento adicional de seguridad puesto que los elevadores dan ecos nítidos a más de 20 millas de distancia. De día constituyen excelentes puntos de reconocimiento las estructuras del elevador de la Junta Nacional de Granos, y los silos que se observan. Como así también, el conjunto de edificios torre en la playa de Necochea. De noche, la iluminación de la ciudad y las luces rojas de la usina y los elevadores.</p>
</div>
<br>
<div class="bluetext"><i>"...Por mi parte les agradezco de corazón a todos por hacer de este viaje algo espectacular. Me subí por primera vez en un velero hace 5 meses y creo que me van a tener que patear el bastón para bajarme de uno cuando sea viejita..."</i></div>
<div class="commontext" align="left">
<p>Egle Fernández, 32 años, egresada del curso de timonel, quién participó en el viaje a Mar del Plata de ida y de vuelta.</p>
</div>
</div>
</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../../common/footer.php';
      ?>
    </main>
  </body>
</html>
