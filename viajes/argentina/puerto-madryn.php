<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Viajes - Puerto Madryn</h1>
        </div>
      </div>
      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 text-center">
							<img alt="" class="img-fluid" style="width: 175px;"src="<?php echo $link; ?>img/800px-Flag_of_Argentina.svg.png"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Costa y Mar Argentino:</h4>
							<h4>Puerto Madryn</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
          <div class="bluetext" align="center">
          <p><b>Puerto Madryn</b> es el mejor fondeadero de Golfo Nuevo (y del litoral atlántico argentino). Está rodeado por una cadena de colinas de cimas aplanadas, de unos 90 metros de altura, el punto dominante es una barranca de 139 metros que se ve al fondo. Tienen preponderancia los vientos del sector Oeste y Sudoeste de unos 14 nudos de intensidad: La presión media anual es de 1006 hPa. La temperatura media anual es de 13°.</p>
          <p><b>Golfo Nuevo</b> (ver carta H-218): situado en la parte sudoeste de la península Valdés, tiene 35 millas de largo por 25 de ancho; su boca, de 7 millas, es profunda y limpia.</p>
          <p>&nbsp;</p>
          <p><img src="<?php echo $link; ?>img/madryn1.jpg" width="219" height="276"></p>
          <p><b>La entrada</b> del golfo se reconoce con facilidad por sus dos puntas altas muy definidas, con sus respectivos faros: el Morro Nuevo al noreste y Punta Ninfas al sudoeste. Al tomar la boca conviene recostarse un poco a Morro Nuevo. La corriente de marea, aunque es bastante fuerte, no produce remolinos en la entrada, pero con marea creciendo y vientos del WNW (fuerza 4 a 6) se notan escarceos. El mismo fenómeno se observa con marea bajando y vientos del SSW. La entrada puede identificarse muy bien en la pantalla del radar.</p>
          <p><b>Corrientes de marea:</b> En Puerto Madryn y en la parte oeste del golfo las corrientes de mareas son muy débiles, pero aumentan gradualmente hacia la boca, donde alcanzan una velocidad de 3,5 nudos.</p>
          <p><b>Escarceos:</b> En la entrada a Golfo Nuevo pueden formarse escarceos de marea. Estos adquieren forma de mar arbolada y confusa, cuando existe la condición de ola considerable u onda marcada en contra de la corriente o con cierto ángulo de incidencia sobre la misma. Estas condiciones se dan con oleaje proveniente entre el Este al Sudoeste y corriente bajando. Cerca de cada costa de entrada, hay lugares donde la corriente tira con más fuerza; es donde los escarceos adquieren mayores proporciones.</p>
          <p><b>Puerto Pirámides</b> (Lat. 42° 35´ S, Long. 64° 17´ W): Está situado en la parte nordeste de Golfo Nuevo y tiene 2 millas de ancho en su boca, limitado al SE por la pequeña península que termina en Punta Pardelas, y al NW por otra península que remata en Punta Pirámide. En esta última se destaca un promontorio, que a pesar de ser mucho más bajo que los demás cerros circundantes, mucho más altos y a pique sobre la playa, por su forma característica es fácilmente identificable, llamado Cerro Pirámide (66 metros), detrás del mismo también se eleva el Cerro Gorro Frigio (95 metros) y sobre el fondo del puerto el Cerro Olazábal (102 metros), que a pesar de su forma extendida, constituye un punto de referencia notable, desde considerable distancia.</p>
          <p><b>Fondeaderos Cracker y Ninfas:</b> A partir de Punta cracker hacia el SE se forman en la costa dos repliegues, conocidos con los nombres de Fondeadero Cracker y Fondeadero Ninfas, respectivamente. El primero está comprendido entre dos barrancas blancas, casi a pique, de 60 metros de altura la del oeste, y de unos 35 metros la del este; al igual que todo el resto de la costa, esas barrancas están bordeadas por restingas que velan en bajamar y que sobresalen unos 400 metros. En el valle entre ambas barrancas hay una estancia y tres molinos. El Fondeadero Ninfas está constituido por una suave ensenada con palya de arena y pedregullo, de 4 millas de largo, el fondo decrece bruscamente, por lo que conviene acercarse despacio para buscar el fondeadero. Hay un casco de estancia y un pequeño bosque de pinos. (El molino que figura en la cartografía al sudoeste del fondeadero es difícil de ver).</p>
          <p>&nbsp;</p>
          <p><img class="img-fluid" src="<?php echo $link; ?>img/madryn2.jpg" width="223" height="225"></p>
          <p>&nbsp;</p>
          </div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../../common/footer.php';
      ?>
    </main>
  </body>
</html>
