<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Viajes - Angra dos Reis</h1>
        </div>
      </div>
      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 text-center">
							<img alt="" class="img-fluid" style="width: 175px;"src="<?php echo $link; ?>img/1200px-Flag_of_Brazil.svg.png"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Mar Brasilero:</h4>
							<h4>Angra dos Reis</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
						<p>&nbsp;</p>

						<p>Tenemos previsto volver a realizar viajes de instrucción en Angra dos Reis (Brasil) a partir de principios de agosto. Está abierta la inscripción para asistir a las charlas previas y sumarse a las tripulaciones para participar de unos días de navegación (5 a 10) recorriendo las islas de Angra dos Reis (con traslados en avión incluídos). Para pedir informes o participar de las charlas informativas comunicarse a&nbsp;<span id="cloakcc84af46c107093db3b5facf5c951b72"><a href="mailto:info@navemocion.com">info@navemocion.com</a></span><script type="text/javascript">
				document.getElementById('cloakcc84af46c107093db3b5facf5c951b72').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addycc84af46c107093db3b5facf5c951b72 = '&#105;nf&#111;' + '&#64;';
				addycc84af46c107093db3b5facf5c951b72 = addycc84af46c107093db3b5facf5c951b72 + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
				var addy_textcc84af46c107093db3b5facf5c951b72 = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';document.getElementById('cloakcc84af46c107093db3b5facf5c951b72').innerHTML += '<a ' + path + '\'' + prefix + ':' + addycc84af46c107093db3b5facf5c951b72 + '\'>'+addy_textcc84af46c107093db3b5facf5c951b72+'<\/a>';
		</script></p>

						<p>También están disponibles para alquilar en Angra dos Reis veleros de 32 a 38 pies. Para pedir informes comunicarse a&nbsp;<span id="cloak4d25c55bee60d12bb476d19b2f6749f2"><a href="mailto:info@navemocion.com">info@navemocion.com</a></span><script type="text/javascript">
				document.getElementById('cloak4d25c55bee60d12bb476d19b2f6749f2').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addy4d25c55bee60d12bb476d19b2f6749f2 = '&#105;nf&#111;' + '&#64;';
				addy4d25c55bee60d12bb476d19b2f6749f2 = addy4d25c55bee60d12bb476d19b2f6749f2 + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
				var addy_text4d25c55bee60d12bb476d19b2f6749f2 = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';document.getElementById('cloak4d25c55bee60d12bb476d19b2f6749f2').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy4d25c55bee60d12bb476d19b2f6749f2 + '\'>'+addy_text4d25c55bee60d12bb476d19b2f6749f2+'<\/a>';
		</script></p>

						<div class="commontext">
						<p>A continuación les mostramos algunas fotos de otra experiencia que disfrutamos en agosto del año 2001.</p>

						<p>Nuestra travesía se inició en la Marina Brachuy, cercana al puerto de Angra dos Reis donde tomamos posesión del velero "Recomenzando", que sería nuestro hogar por los siguientes 7 días. A partir de ese puerto fuimos recorriendo numerosas islas* hasta llegar al límite del continente, donde pusimos proa a la Ilha Grande y dimos la vuelta a la misma por mar abierto (ver carta). Una vez que ingresamos a la cara de la isla que da al continente nos dedicamos a recorrer las numerosas bahías existentes en esa zona, que albergan paisajes paradisíacos. El clima nos acompañó, ya que las temperaturas se mantuvieron siempre cercanas a los 27º, permitiéndonos zambullirnos y bucear todos los días en las cálidas y transparentes aguas.</p>

						<p>&nbsp;</p>

						<div align="center" class="bluetext">
						<h5><b>Acá podemos ver parte de nuestro recorrido en Isla Grande, la línea inferior indica nuestra travesía por mar abierto.</b></h5>

						<div align="center">&nbsp;</div>

						<div align="center"><i>	<img alt="" class="img-fluid"  src="<?php echo $link; ?>img/mapa.gif" style="margin-left: auto; margin-right: auto; display: block;">* Recorrido hecho en agosto del año 2001, Marina Bracuhy (Angra dos Reis) - Isla de Itanhangá - Saco de Tarituba - Isla do Cedro - Parati - Ensenada das Palmas - Playa Lopez Mendez - Abraao - Saco de Ceu - Laguna Azul y Marina Bracuhy.</i></div>

						<p><b><br>
							<img alt="" class="img-fluid"  src="<?php echo $link; ?>img/paisaje.jpg" style=""></b></p>

						<div align="center"><i>El paisaje de Angra</i></div>

						<div align="center">&nbsp;</div>
						</div>

						<div align="center" class="bluetext">&nbsp;</div>

						<div align="center" class="bluetext"><i>	<img alt="" class="img-fluid"  height="422" src="<?php echo $link; ?>img/angra02.jpg" width="295"></i></div>

						<div align="center" class="bluetext"><i>Desembarco en la playa de Parati - Mirim.</i>

						<p><b><br>
						<br>
						<img alt="" class="img-fluid" src="<?php echo $link; ?>img/navegando.jpg" style=""></b><i><br>
						Navegando hacia la próxima isla...</i></p>

						<p><b><img alt="" src="<?php echo $link; ?>img/angra04.jpg" style=""></b></p>

						<div align="center"><i>Playa López Méndez, solitaria y agreste (se pueden bajar cocos, bucear, y fondear a unos pocos metros de la costa)</i></div>

						<p><b></b><br>
						&nbsp;</p>

						<p>"Angra dos Reis: ¿Sería así la puerta del Paraíso? Si no lo era debería ser muy parecida. Eso sentí yo al virar la punta de Joatinga: agua clara color esmeralda, viento suave, islas verdes, arena blanca, calor...Cualquier esfuerzo hecho para llegar hasta aquí se justificaba, desaparecía, se esfumaba en la magia del lugar. Esto sí, era el placer de navegar...Y yo estaba arribando, con el Vito, aleluya!. Anochecía cuando recalé en la primera ensenada, entrando, a la izquierda de la bahía. Como sólo tenía las velas para llegar, lo hice con cuidado, tratando de arrimar lo más cerca posible de la costa, sin pegarle a ninguno de los barquitos pescadores fondeados, como para no dar mala impresión de entrada...Tiré el ancla y al levantar la vista, un millón de luciérnagas agujereaban la noche, haciendo de candilejas al coro polifónico de ranas cantoras, víboras silbadoras y sapos cancioneros que me daban la bienvenida.... Lo que encontré a la mañana siguiente era mucho mejor de lo que mis expectativas suponían. Farallones de piedra negra y ocre, de más de quinientos metros de altura, con sus pies hundidos en la floresta, rodeando el poblado, de no más de cincuenta casitas. Una playa de arena amarilla sembrada de palmeras y flanqueadas por rocas en forma de huevos gigantescos, enmarcaban Pouso. Así se llama el lugar donde el tiempo parece detenido y los únicos sonidos que se perciben son el jadeo del mar, el canto de los pájaros y las voces cansinas de sus habitantes. No hay calles, no hay autos, no hay motos y el único acceso al caserío es a través de sus propios barquitos de pesca. Amanecía y me llegaba el olor vegetal de la tierra, junto con el trinar de docenas de pájaros. Cerca unos pescadores en canoas hechas de troncos recorrían un espinel compitiendo con los cormoranes, que se zambullían y reaparecían con peces plateados en el pico. El agua verde claro invitaba a nadar, el sol recién salido calentaba, "El placer de los Dioses!"&nbsp;<br>
						&nbsp;</p>

						<div align="center"><b>Fragmento del libro "Orza Vito" de Enrique Celesia, quién circunnavegó Sudamérica.</b></div>
						</div>
						</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../../common/footer.php';
      ?>
    </main>
  </body>
</html>
