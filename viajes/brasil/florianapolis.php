<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Viajes - Florianópolis</h1>
        </div>
      </div>
      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 text-center">
							<img alt="" class="img-fluid" style="width: 175px;"src="<?php echo $link; ?>img/1200px-Flag_of_Brazil.svg.png"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Mar Brasilero:</h4>
							<h4>Florianópolis</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
<div class="bluetext">Estamos organizando una navegación a vela a Florianópolis en embarcaciones de 40 pies. Para participar hay que disponer de unos 8 días.</div>
<p>&nbsp;</p>
<div class="commontext">Para cualquier pregunta o para anotarse en la charla informativa, comunicarse a:&nbsp;<span id="cloakf1c09d5552f1088ebac3e0e895dcf9a6"><a href="mailto:info@navemocion.com">info@navemocion.com</a></span><script type="text/javascript">
				document.getElementById('cloakf1c09d5552f1088ebac3e0e895dcf9a6').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addyf1c09d5552f1088ebac3e0e895dcf9a6 = '&#105;nf&#111;' + '&#64;';
				addyf1c09d5552f1088ebac3e0e895dcf9a6 = addyf1c09d5552f1088ebac3e0e895dcf9a6 + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
				var addy_textf1c09d5552f1088ebac3e0e895dcf9a6 = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';document.getElementById('cloakf1c09d5552f1088ebac3e0e895dcf9a6').innerHTML += '<a ' + path + '\'' + prefix + ':' + addyf1c09d5552f1088ebac3e0e895dcf9a6 + '\'>'+addy_textf1c09d5552f1088ebac3e0e895dcf9a6+'<\/a>';
		</script></div>
    <p>&nbsp;</p>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../../common/footer.php';
      ?>
    </main>
  </body>
</html>
