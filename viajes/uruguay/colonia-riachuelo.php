<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Viajes - Colonia-Riachuelo</h1>
        </div>
      </div>
      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 text-center">
							<img alt="" class="img-fluid" style="width: 175px;"src="<?php echo $link; ?>img/uy.png"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Costa y mar Uruguayo:</h4>
							<h4>Colonia-Riachuelo</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
        
<div class="bluetext">Al menos una vez al mes realizamos un crucero a Colonia o Riachuelo, que son los destinos que conforman el bautismo naútico oficial de casi todos los nautas del Río de la Plata. Además de la magia del primer cruce del río se agregan los encantos propios de esta maravillosa ciudad, con sus callecitas empedradas y su arquitectura colonial o el paisaje agreste y la tranquilidad del arroyo Riachuelo.</div>
<p>&nbsp;</p>
<div class="commontext">Esta ciudad fue fundada el 28 de enero de 1680 por Manuel Lobo, siguiendo órdenes impartidas por el rey de Portugal, con el mismo nombre que hoy mantiene, dando cruentas luchas por su posesión entre España y Portugal, hasta 1777 en que queda definitivamente bajo dominio español. El departamento de Colonia se crea en 1816, pero la plaza aún permanecería bajo el mando portugués entre 1818 y 1828, hasta su incorporación definitiva a la patria oriental.
<p>Aqui les mostramos fotos de algunos de los viajes realizados.</p>
<p>&nbsp;</p>
</div>
<img class="img-fluid" style="display: block; margin-left: auto; margin-right: auto;" src="<?php echo $link; ?>img/colonia1.jpg" width="463" height="303"><br>
<div class="bluetext" align="center">El puerto de Colonia, siempre colmado de veleros y gente linda...</div>
<div class="bluetext" align="center">&nbsp;</div>
<br><img class="img-fluid" style="display: block; margin-left: auto; margin-right: auto;" src="<?php echo $link; ?>img/riachuelo1.jpg" width="551" height="361"><br>
<div class="bluetext" align="center">Amarrados en Riachuelo.</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../../common/footer.php';
      ?>
    </main>
  </body>
</html>
