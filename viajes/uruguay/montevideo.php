<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Viajes - Montevideo</h1>
        </div>
      </div>
      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 text-center">
							<img alt="" class="img-fluid" style="width: 175px;"src="<?php echo $link; ?>img/uy.png"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Costa y mar Uruguayo:</h4>
							<h4>Montevideo</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
        <div class="bluetext"><br>
<div class="bluetext">Gracias a una invitación personal cursada por Toribio de Achaval a la escuela de navegación de la U.B.A. (Facultad de Ciencias Exactas y Naturales), los integrantes del equipo docente de la mencionada escuela pilotos de yate Pablo Giraudi y Gabriel Buduba, pudieron formar parte de la tripulación del "Matrero" (diseńo de frers de 50 pies de eslora, que ha participado de regatas en todo el mundo desde 1970, obteniendo todo tipo de trofeos) para participar de la regata a La Panela 2003 (la regata consiste en navegar hasta Montevideo, virar la roca denominada "Panela" y regresar a Buenos Aires, sin escalas).</div>
<p>&nbsp;</p>
<div class="commontext">Además de haber sido una experiencia muy fructífera para todos, obtuvieron el primer puesto.</div>
<div class="commontext">&nbsp;</div>
<div class="commontext"><img class="img-fluid" style="display: block; margin-left: auto; margin-right: auto;" src="<?php echo $link; ?>img/matrero.jpg" width="450" height="379"></div>
<div class="commontext" style="text-align: center;">Navegando en el Matrero</div>
</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../../common/footer.php';
      ?>
    </main>
  </body>
</html>
