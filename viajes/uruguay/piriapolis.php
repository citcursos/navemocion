<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Viajes - Piriápolis</h1>
        </div>
      </div>
      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 text-center">
							<img alt="" class="img-fluid" style="width: 175px;"src="<?php echo $link; ?>img/uy.png"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Costa y mar Uruguayo:</h4>
							<h4>Piriápolis</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
<div class="bluetext">Nuestros viajes a Piriápolis, los realizamos en verano normalmente.
<p>La travesía en velero requiere de aproximadamente unas 40 horas, y se navega tanto de día como de noche.</p>
<p>Por lo tanto para realizar este viaje habrá que disponer de por lo menos cinco días.</p>
</div>
<p>&nbsp;</p>
<div class="commontext">Para cualquier pregunta o consulta, comunicarse a:&nbsp;<span id="cloak68c728f3c10938635d948d175340ff5f"><a href="mailto:info@navemocion.com">info@navemocion.com</a></span><script type="text/javascript">
				document.getElementById('cloak68c728f3c10938635d948d175340ff5f').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addy68c728f3c10938635d948d175340ff5f = '&#105;nf&#111;' + '&#64;';
				addy68c728f3c10938635d948d175340ff5f = addy68c728f3c10938635d948d175340ff5f + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
				var addy_text68c728f3c10938635d948d175340ff5f = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';document.getElementById('cloak68c728f3c10938635d948d175340ff5f').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy68c728f3c10938635d948d175340ff5f + '\'>'+addy_text68c728f3c10938635d948d175340ff5f+'<\/a>';
		</script></div>
<p>&nbsp;</p>
<p><img class="img-fluid" src="<?php echo $link; ?>img/piriapolis.jpg" width="100%"></p>
<div class="bluetext" align="center">&nbsp;</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../../common/footer.php';
      ?>
    </main>
  </body>
</html>
