<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Viajes - Sauce</h1>
        </div>
      </div>
      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 text-center">
							<img alt="" class="img-fluid" style="width: 175px;"src="<?php echo $link; ?>img/uy.png"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Costa y mar Uruguayo:</h4>
							<h4>Sauce</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
<div class="bluetext"><b><i>Las cincuenta millas que nos separan de este puerto, nos ofrecen una travesía más prolongada que un cruce a Colonia, que nos invita a permanecer un mayor número de horas navegando, aprovechando para practicar los conceptos de navegación por estima, satelital y astronómica. Esta navegación es especialmente indicada para los timoneles, que quieran practicar, aprender e ir tomándole el gustito a navegaciones de más largo aliento que las habituales.</i></b></div>
<div class="commontext">
<p>Protegido por una escollera de 720 metros de largo, que se extiende en dirección WNW y cuyo extremo está balizado, Luz Roja Des c/ 5 Seg. (Lat. 34° 26´ (S) Long. 57° 27´ (W), se asienta sobre punta Sauce el puerto del mismo nombre. Se encuentra 20 millas al este de Colonia, y en una zona cuya navegación reglamentariamente ya corresponde a los patrones (por encontrarse este puerto al este de Punta Artilleros), por lo que el despacho de la embarcación debe ser realizado por un navegante con dicha habilitación.</p>
<p>Punta Sauce, que despide rocas en sus proximidades, es fácil de reconocer desde el agua por los edificios y chimeneas de la población de Juan Lacaze, que se asienta en sus inmediaciones, siendo muy visible especialmente el edificio y chimenea de una fábrica de papel "Fanapel".</p>
<p>Esta derrota puede planificarse en las cartas H-5089, H-116 y en el "Atlas Cartográfico" H-5090.</p>
<p>El puerto de Sauce es pequeño y tranquilo, permitiéndonos descansar adecuadamente de la travesía.</p>
</div>
<p>&nbsp;</p>
<p><img class="img-fluid" style="display: block; margin-left: auto; margin-right: auto;" src="<?php echo $link; ?>img/sauce.jpg" width="450" height="326"></p>
<div class="bluetext" align="center">Amarrados en Sauce junto a un carguero</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../../common/footer.php';
      ?>
    </main>
  </body>
</html>
