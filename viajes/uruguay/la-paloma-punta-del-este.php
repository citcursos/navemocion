<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Viajes - La Paloma - Punta del Este</h1>
        </div>
      </div>
      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 text-center">
							<img alt="" class="img-fluid" style="width: 175px;"src="<?php echo $link; ?>img/uy.png"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Costa y mar Uruguayo:</h4>
							<h4>La Paloma - Punta del Este</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					
						<div class="bluetext"><b>Del 2 al 9 del Febrero de 2001 realizamos un crucero de instrucción a La Paloma en Uruguay.</b></div>

						<p style="text-align: left;">&nbsp;</p>

						<div class="commontext">Partimos con rumbo a Montevideo, donde realizamos nuestra primer recalada, para porseguir directamente hacia nuestro destino final.
						<p style="text-align: left;">Luego de un merecido descanso, emprendimos el regreso, recalando esta vez en Punta del Este y Montevideo debido al mal clima predominante.</p>

						<p style="text-align: left;">&nbsp;</p>
						</div>
						<img alt="" class="img-fluid" height="295" src="<?php echo $link; ?>img/paloma1.jpg" style="margin-left: auto; margin-right: auto; display: block;" width="422">
						<div align="center" class="bluetext" style="text-align: center;"><small>A toda vela, solo cielo y mar en el horizonte.</small></div>
						<br>
						<img alt="" class="img-fluid" height="295" src="<?php echo $link; ?>img/paloma2.jpg" style="margin-left: auto; margin-right: auto; display: block;" width="422">
						<div align="center" class="bluetext" style="text-align: center;"><small>Preparados para enfrentar la tormenta.</small></div>
						<br>
						<img class="img-fluid" height="422" src="<?php echo $link; ?>img/paloma3.jpg" style="margin-left: auto; margin-right: auto; display: block;" width="295">
						<div align="center" class="bluetext" style="text-align: center;"><small>La calma después del temporal...</small></div>
					
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../../common/footer.php';
      ?>
    </main>
  </body>
</html>
