<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Astillero - Herrería</h1>
        </div>
      </div>
        <div class="container grey">
        <div itemprop="articleBody">
		<p style="text-align: center;"><span style="font-size: 14pt;"><strong>HERRERÍA</strong></span></p>
<p style="text-align: center;">&nbsp;</p>
<table width="100%">
<tbody>
<tr>
<td style="text-align: center;">
<p><img class="img-fluid" src="<?php echo $link; ?>img/herreria_3.jpg"></p>
</td>
<td style="text-align: center;"><img class="img-fluid" src="<?php echo $link; ?>img/herreria_4.jpg"></td>
</tr>
<tr>
<td class="commontext" colspan="2">
<ul>
<li>Púlpitos</li>
<li>Balcones</li>
<li>Candeleros</li>
<li>Chubasquerasl</li>
<li>Biminis</li>
<li>Cañas de timón</li>
<li>Ejes de timón</li>
<li>Sistemas de rueda de timón</li>
<li>Pie de amigo</li>
<li>Escaleras</li>
</ul>
</td>
</tr>
</tbody>
</table>	</div>
	    </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
