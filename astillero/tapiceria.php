<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Astillero - Tapicería</h1>
        </div>
      </div>
        <div class="container grey">
        <div itemprop="articleBody">
		<p style="text-align: center;"><span style="font-size: 24pt;"><strong>Tapicería</strong></span></p>
<p style="text-align: center;">&nbsp;</p>
<p align="center"><img class="img-fluid" src="<?php echo $link; ?>img/logoalplast.gif"></p>
<p align="center">&nbsp;</p>
<table style="margin-left: auto; margin-right: auto; width: 645px;" border="0" cellpadding="4" >
<tbody>
<tr style="height: 214px;">
<td style="height: 214px; width: 345px;" align="left"><img class="img-fluid" src="<?php echo $link; ?>img/tapiceria1.jpg" align="left"></td>
<td class="commontext" style="height: 214px; width: 274px;"><span class="bluetext"><b>ARTÍCULOS PARA TAPICERÍA NÁUTICA:</b></span> <br>LONAS ACRÍLICAS <br>SAULEDA <br>COBERTURAS LISAS Y RAYADAS <br>BROCHES <br>CIERRES <br>COLCHONES <br>TELAS PLÁSTICAS <br>TELAS PARA CORTINAS <br>PANAS <br>LONETAS ACRÍLICAS <br>CORTES DE POLIESTER <br>ALFOMBRAS <br>ALMOHADAS</td>
</tr>
<tr style="height: 89.625px;">
<td class="commontext" style="height: 89.625px; width: 619px;" colspan="2" align="center"><b>Av. Avellaneda 3112/14 (1646) Virreyes- provincia de Buenos Aires <br>tel/fax: (011) 4745-1646 <span id="cloak7396b1747ee035004c8649cda51e1b18"><a href="mailto:alplast@argentina.com">alplast@argentina.com</a></span><script type="text/javascript">
				document.getElementById('cloak7396b1747ee035004c8649cda51e1b18').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addy7396b1747ee035004c8649cda51e1b18 = '&#97;lpl&#97;st' + '&#64;';
				addy7396b1747ee035004c8649cda51e1b18 = addy7396b1747ee035004c8649cda51e1b18 + '&#97;rg&#101;nt&#105;n&#97;' + '&#46;' + 'c&#111;m';
				var addy_text7396b1747ee035004c8649cda51e1b18 = '&#97;lpl&#97;st' + '&#64;' + '&#97;rg&#101;nt&#105;n&#97;' + '&#46;' + 'c&#111;m';document.getElementById('cloak7396b1747ee035004c8649cda51e1b18').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy7396b1747ee035004c8649cda51e1b18 + '\'>'+addy_text7396b1747ee035004c8649cda51e1b18+'<\/a>';
		</script></b></td>
</tr>
</tbody>
</table>	</div>
	    </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
