<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Astillero - GPS</h1>
        </div>
      </div>
        <div class="container grey">
        <div itemprop="articleBody">
		<div class="commontext">
<div class="commontext">
<p align="center"><a href="http://www.naviar.com.ar" target="_blank" rel="noopener noreferrer"><img class="img-fluid" src="<?php echo $link; ?>img/Naviar.jpg" border="0"></a></p>
<p><img style="margin-right: 10px; margin-left: 10px;" class="img-fluid" src="<?php echo $link; ?>img/image001.png" width="260" height="173" align="left" border="0"> Veamos algunas de las características de estas cartas vectoriales desarrolladas por Naviar (<a href="http://www.naviar.com.ar" target="_blank" rel="noopener noreferrer">www.naviar.com.ar</a>) cuya principal ventaja y novedad, es que se cargan directamente en la propia memoria del G.P.S. y se visualizan en su pantalla. <br>A todas las bondades de un G.P.S., con un sistema de cartas digitales se agrega la posibilidad de ver representada nuestra ubicación sobre una carta náutica, en la que podamos apreciar profundidades, peligros cercanos, boyas y faros. (Los navegadores satelitales G.P.S. permiten conocer de manera constante nuestra posición exacta -latitud y longitud-, nuestra velocidad, nuestra distancia y rumbo a seguir para llegar a destino) <br>Este sistema de cartas digitales tiene muchas ventajas: <br>Las cartas se cargan directamente a la memoria del GPS y se visualizan en la pantalla del mismo equipo, sin necesidad de Plotter ni PC. Por lo tanto son mucho más económicas y prácticas. <br>- Las cartas, al ser vectoriales, ofrecen mas posibilidades que las cartas convencionales. Permite localizar puntos de interés con todos sus datos correspondientes de manera rápida y sencilla. Al seleccionar un elemento indíca su nombre y características, por ejemplo Boya Roja, des.(4) c/5 seg. <br>- 145.000 Km2 de superficie cubierta. <br>- 6.400 puntos de interés (POIs). <br>- 17.800 puntos de sondaje y batimetría en escala de colores. <br>- Admiten la función de zoom para ver mayor detalles de cuarterones, incluidos en las cartas. <br>- Permiten la búsqueda electrónica de los elementos de la carta, localización en la carta, navegación etc. <br>- Pueden ser personalizadas de acuerdo a las necesidades del usuario. <br>- Admiten la incorporación de waypoints y rutas del operador. <br>-Han sido verificadas por distintos medios y usuarios que la utilizan, con uso muy satisfactorio. <br>- Tambión son utilizadas en tierra.</p>
<p><img style="margin-right: 10px; margin-left: 10px;" class="img-fluid" src="<?php echo $link; ?>img/image008.jpg" width="260" height="225" align="right" border="0"> Están disponibles cartas digitales de distintas regiones, entre otras : Carta Náutica del Río de la Plata interior y exterior, Delta, Ríos Paraná y Uruguay, Gualeguychú y Río Negro, incluido los cuarterones correspondientes. De la parte terrestre Capital Federal (calles y avenidas), Gran Buenos Aires (calles y avenidas), carretero del país. Tambien se desarrollan cartas para usos especiales de forestacion, topográficas, etc. <br>Las cartas náuticas incluyen los cuarterones correspondientes y contienen: Puertos y marinas, estaciones de combustible, las costas, canales y vías navegables, boyas, balizas y faros (con indicación de todas sus características) , barcos hundidos o varados, obstrucciones, rocas, profundidades, isobatas...Con datos actualizados en forma permanente.</p>
<p><img style="margin-right: 10px; margin-left: 10px;" class="img-fluid" src="<?php echo $link; ?>img/image004.png" width="260" height="173" align="left" border="0"> Los Navegadores Satelitales o GPS (Posicionador Geográfico Satelital) son instrumentos portátiles, del tamaño aproximado de un teléfono celular, segun el modelo, cuya función básica consiste en captar señales satelitales y determinar automáticamente las coordenadas del lugar en que se encuentre. Esta posición es representada continuamente en la pantalla del GPS de manera que el operador puede "verse" en la posición en que se encuentre, y si está en movimiento, verá el recorrido que realiza, a tiempo real.</p>
<p><br>En Estados Unidos y en algunos países de Europa, muchos modelos de automóviles traen este sistema de GPS, de manera que el automovilista puede ver su propio vehículo en pantalla recorriendo la ciudad. A su vez le brinda otra serie de funciones útiles, entre como la de búsqueda y navegación. El conductor podrá buscar un punto de interés (por orden alfabético, por proximidad o por categoría) y el instrumento le dará la distancia al punto y la dirección en que se encuentra. El usuario podrá pedir que el punto se visualice en pantalla, o que lo guíe hacia el punto elegido, para lo cual el GPS le dirá en forma automática la dirección en que debe desplazarse, la distancia que le falta para llegar, la velocidad de marcha, el tiempo estimado de arribo, y hasta le indicará con una señal sonora y visual, "llegando al punto de arribo".</p>
<p><img style="margin: 10px;" class="img-fluid" src="<?php echo $link; ?>img/image005.png" width="260" height="173" align="right" border="0"> Los GPS son muy utilizados en la actividad náutica. El navegante obtiene su posición, y si tiene incorporada la carta náutica del lugar, podrá navegar hacia determinado puerto, seguir un canal, esquivar una embarcación hundida bajo el agua, etc. El funcionamiento no se ve afectado por falta de visibilidad o condiciones meteorológicas.</p>
<p>Tambión son utilizados en otras actividades, como la aeronáutica, agropecuaria, etc. ya que el GPS brinda otra serie de funciones especificas, como la determinación de superficie de áreas irregulares, medición de distancia, relevamiento de puntos. etc.</p>
<p>Ahora bien, para "verse navegando en la carta", la misma debe haberse incorporado previamente en el GPS. Esta cartografía no es un "dibujo" scaneado, sino que es una carta electrónica digital, un GIS (Sistema de Información Geográfica) referenciada en coordenadas geográficas, que se carga en la memoria del GPS. Esta carta, formada por una base de datos, puede tener una gran cantidad de información, y precisamente al ser "digital", permite la búsqueda de puntos de interés "digitalmente", por orden alfabético, categoría, etc. ya que cada elemento de la carta no es solamente un dibujo, sino que tiene asociado una base de datos. La incorporación de cartas se hace en una memoria independiente de la destinada a trakcs o wayponts, de manera que no afecta la capacidad del GPS para guardar los puntos de interés del operador, los que podran visualizarse sobre la carta incorporada. En el caso de cargar mas de una cata, el operador decide cual dejar activa o apagada.</p>
<p>En ciertos lugares como Europa, estas cartas se comercializan, pero en Argentina, hasta hace poco tiempo, no se disponía de cartografía local de detalle ni precisión suficientes, de manera que el GPS mostraba una pantalla en blanco o con muy poco detalle, salvo los propios wayponits incorporados por el usuario.</p>
<p>La firma local <b>Naviar</b> (<a href="http://www.naviar.com.ar" target="_blank" rel="noopener noreferrer">www.naviar.com.ar</a>) ha desarrollado una serie de cartografía Náutica y Terrestre, como las del Rio de La Plata, Capital Federal y Gran Buenos Aires, entre otras.</p>
<p>La Carta Náutica del Río de La Plata desarrollada por Naviar, abarca ambas márgenes desde Punta del Este y Canal Punta Indio hasta pasar Nueva Palmira, al norte, el Delta y parte de la red vial costera. En dicha carta se ha volcado una gran cantidad de información, como cuarterones, canales y vías navegables, islas y bajos, barcos hundidos o varados, puntos de sondaje y curvas de profundidad, boyados, etc. Por ser una carta vectorial, se pueden ubicar puntos de interés como islas o kilometrajes del río por orden alfabético o de proximidad. Al "clickear" un elemento, despliega las características del mismo: Boya Roja, Des (4) c/5 seg</p>
<p>La misma firma dispone además de la carta de Capital Federal y Gran Buenos Aires, con calles y avenidas, autopistas y puntos de interés, incluido algunos barrios cerrados que habitualmente no figuran en las guías o cartas de ruta.</p>
<p>La utilización y navegación con GPS no requiere de un entrenamiento complicado, ya que el instrumento opera en forma automática, debiendo el operador conocer algunas funciones del mismo para seleccionar las opciones de su preferencia. Podrá además bajar y grabar en una PC sus propios recorridos (tracks) o puntos de interés propios (waypoint).</p>
<p>Los equipos son portátiles y utilizan alimentación de energía con pilas comunes o con cable de conexión de 12 V (encendedor de automóvil). Tienen antena receptora incorporada o externa. El uso del GPS es libre y gratuito. No requiere ningún tramite ni permiso de uso y no tiene abono ni costo fijo.</p>
<p class="tablemenu" align="center">&nbsp;</p>
<p class="tablemenu" align="center"><strong>ACTUALIZACION Y NUEVAS CARTAS NAUTICAS</strong></p>
<p class="tablemenu" align="center">&nbsp;</p>
<p><img class="img-fluid" src="<?php echo $link; ?>img/image009.jpg" align="left" border="0"></p>
<p><img class="img-fluid" src="<?php echo $link; ?>img/gpsmap.jpg" border="0"> <br><br> <a href="http://www.naviar.com.ar" target="_blank" rel="noopener noreferrer">www.naviar.com.ar</a> <br><span id="cloak98b4665c1cf55c17bae6a58f1260b638"><a href="mailto:info@naviar.com.ar">info@naviar.com.ar</a></span><script type="text/javascript">
				document.getElementById('cloak98b4665c1cf55c17bae6a58f1260b638').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addy98b4665c1cf55c17bae6a58f1260b638 = '&#105;nf&#111;' + '&#64;';
				addy98b4665c1cf55c17bae6a58f1260b638 = addy98b4665c1cf55c17bae6a58f1260b638 + 'n&#97;v&#105;&#97;r' + '&#46;' + 'c&#111;m' + '&#46;' + '&#97;r';
				var addy_text98b4665c1cf55c17bae6a58f1260b638 = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#105;&#97;r' + '&#46;' + 'c&#111;m' + '&#46;' + '&#97;r';document.getElementById('cloak98b4665c1cf55c17bae6a58f1260b638').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy98b4665c1cf55c17bae6a58f1260b638 + '\'>'+addy_text98b4665c1cf55c17bae6a58f1260b638+'<\/a>';
		</script> <br>Tel. +54 (11) 4115 9111 <br><br></p>
<p><span class="bluetext"><b>Río de la Plata interior y Exterior:</b></span> <br>de: S 33º51,5' a 36º29' y : W 54º47,4 a 58º43' <br>(De Nueva Palmira a Punta Indio y desde el Delta hasta Montevideo, Punta del Este y La Paloma. Incluye Nueva carta Rocha - La Paloma) <br>Superficie: 91.000 Km2 <br>Aprox. 3359 puntos de interés. <br>Aprox. 7900 puntos de sondaje.</p>
<p><span class="bluetext"><b>Río Uruguay y Rio Negro:</b></span> <br>de: S 33º51,5' a 36º29' y : W 54º47,4 a 58º43' <br>(Fray Bentos, Gualeguaychú, Soriano, Mercedes, etc. (Gualeguaychu, Rio Negro y Rio Uruguay Km 0-120)) <br>Superficie: 26.400 Km2 <br>Aprox. 1200 puntos de interés <br>Aprox. 1800 puntos de sondaje</p>
<p><span class="bluetext"><b>Río Paraná de las Palmas y Paraná Guazú hasta Km 308:</b></span> <br>de: S 33º31' a 34º23,2' y : W 58º43' a 60º06' <br>Río Paraná de las Palmas y Río Paraná Guazú al norte, ríos y arroyos intermedios. Costa de Escobar, Campana y Zárate. Ubicuy sobre Paraná Guazú. Baradero, San Pedro y Obligado. <br>Superficie: 6.300 Km2. <br>Aprox. 900 puntos de interés. <br>Aprox. 3000 puntos de sondaje.</p>
<p><span class="bluetext"><b>Río Paraná hasta Rosario Km 308 a 452:</b></span> <br>de: S 32º41,4' a 33º31' y : W 59º46' a 60º48' <br>Río Paraná, puertos y ciudades: Ramallo, San Nicolás, Villa Constitución, Puerto Esther, Rosario, San Lorenzo. <br>Superficie: 9.100 Km2. <br>Aprox. 620 puntos de interés. <br>Aprox. 3884 puntos de sondaje.</p>
<p><span class="bluetext"><b> Río Paraná hasta Santa Fé Km 452 a 603</b></span> <br>de: S 33º51,5' a 36º29' y : W 60º09' a 60º48' <br> Río Paraná y Coronda, puertos y ciudades: Diamante, Santa Fé, Paraná. <br>Superficie: 12.200 Km2. <br>Aprox. 340 puntos de interés. <br>Aprox. 1240 puntos de sondaje.</p>
<p class="tablemenu">CARTAS TERRESTRES METROPOLITANAS</p>
<p><span class="bluetext"><b>Capital Federal (Calles, avenidas, autopistas y puntos de interés.)</b></span></p>
<p><span class="bluetext"><b>Gran Buenos Aires (Calles, avenidas, autopistas y puntos de interés.)</b></span></p>
<p>Capital Federal / Tigre / Benavidez / San Fernando / San Isidro / Vte Lopez / San Miguel / Malvinas Argentinas / Jose c. Paz / Moreno / Ituzaingo / Tres de Febrero / San Martin / Hurligham / Moron / Merlo / Matanza / Ezeiza / Esteban Echeverria / Avellaneda / Lomas de Zamora / Lanus / Quilmes / Bernal / Florencio Varela / Almirante Brown / Burzaco / Longchamp / Beriso / Ensenada / Citybell / La Plata</p>
<p>Otras Localidades (consultar)</p>
<p class="tablemenu">CARRETEROS</p>
<p><span class="bluetext"><b>Rep.Argentina </b></span></p>
<p><span class="bluetext"><b>Rutas y caminos principales, ferrocarriles, localidades, cursos de agua,Estaciones de GNC, ACA, puntos de interés.</b></span></p>
<p class="tablemenu">CARTAS ESPECIALES</p>
<p><span class="bluetext"><b>Cartografía especial para empresas de servicios, petroleras, forestales, mineras, etc. </b></span></p>
<p class="tablemenu">Puede ver aplicaciones de GPS en:</p>
<p><a href="http:// www.naviar.com.ar">Web Naviar</a></p>
<p><a href="http://www.naviar.com.ar/m_catalogo.html">Catálogo de cartas</a></p>
<p><a href="http://www.cstg.com.ar/arg/gps_func.html">Como funciona el GPS?</a></p>
<p><a href="http://www.navemocion.com/www.cstg.com.ar/arg/sec_8_new.html">Aplicaciones de GPS</a></p>
<p><a href="http://www.cstg.com.ar/arg/gestion.pdf">GPS - Herramienta de gestión</a></p>
<p><a href="http://www.cstg.com.ar/arg/transito.pdf">GPS - Estudios de Tránsito</a></p>
<p align="center"><i>Los interesados en conocer más información acerca de estas novedosas cartas digitales, pueden comunicarse al telefax <b> +54 (11) 4115 9111 </b> o a <span id="cloaka64f580cd788bb82119a3076c2537451"><a href="mailto:info@naviar.com.ar">info@naviar.com.ar</a></span><script type="text/javascript">
				document.getElementById('cloaka64f580cd788bb82119a3076c2537451').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addya64f580cd788bb82119a3076c2537451 = '&#105;nf&#111;' + '&#64;';
				addya64f580cd788bb82119a3076c2537451 = addya64f580cd788bb82119a3076c2537451 + 'n&#97;v&#105;&#97;r' + '&#46;' + 'c&#111;m' + '&#46;' + '&#97;r';
				var addy_texta64f580cd788bb82119a3076c2537451 = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#105;&#97;r' + '&#46;' + 'c&#111;m' + '&#46;' + '&#97;r';document.getElementById('cloaka64f580cd788bb82119a3076c2537451').innerHTML += '<a ' + path + '\'' + prefix + ':' + addya64f580cd788bb82119a3076c2537451 + '\'>'+addy_texta64f580cd788bb82119a3076c2537451+'<\/a>';
		</script> . O ver más detalles en <a href="http://www.naviar.com.ar" target="_blank" rel="noopener noreferrer">www.naviar.com.ar</a> . También pueden dirigirse a Av. de Mayo 1343 8º.</i></p>
</div>
</div>	</div>
	    </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
