<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Astillero - Baterías</h1>
        </div>
      </div>
      <div class="container grey">
      <div itemprop="articleBody">
		<div class="commontext">
<div class="commontext">
<div class="commontext">
<div class="commontext">
<div class="commontext">
<div class="commontext">
<p align="center"><img class="img-fluid" src="<?php echo $link; ?>img/logar.jpg" style=""></p>

<p align="center" class="commontext"><i>Lea tambien nuestra nueva nota "Baterías de Ciclo Profundo (Nota 2)"...</i><br>
<a href="#baterias2"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/baterias_nota2.jpg" style=""></a></p>

<hr>
<p align="center" class="bluetext">&nbsp;</p>

<p align="center" class="bluetext"><span style="font-size: 14pt;"><b>Baterías de Ciclo Profundo (Nota 1)</b></span></p>

<p>Muchos navegantes se quejan que las baterías se descargan muy rápidamente y la carga almacenada en ella nunca les alcanza. En realidad esto es de fácil solución, si pudiésemos comprender como funcionan las baterías y como se las debe cargar correctamente.</p>

<p>Las baterías se descargan principalmente por cuatro motivos distintos, incorrecta elección del tipo de batería, mal dimensionamiento de los bancos de baterías, incorrecta carga de las baterías y desconocimiento del correcto uso y mantenimiento de las baterías.</p>

<p>En primer término debemos comprender que hay dos tipos de baterías que son muy diferentes y su aplicación no debe confundirse.</p>

<p>A) Baterías de arranque: Son las más comunes. El 99% de las baterías disponibles en el mercado son en definitiva baterías de arranque y la única diferencia entre ellas es el material constitutivo.</p>

<p>B) Baterías de Ciclo Profundo ( “Deep Cycle” ): Son las que usted debiera usar con exclusividad en su banco de baterías de servicio. Es muy difícil conseguirlas ya que su uso está muy limitado. ( Prácticamente se las usa para muy pocas aplicaciones: carros de golf, maquinas lavadoras, auto-elevadores eléctricos y bancos de servicio en embarcaciones ó casas rodantes ). De aspecto son muy parecidas a las baterías de arranque, su única gran diferencia es que son un poco más altas. Las hay de varios tipos de materiales de construcción, siendo recomendables las de plomo ácido con tapa, iguales a las de arranque. Estas baterías están construidas con placas mucho más gruesas y más separadas que las que usan las baterías de arranque.</p>

<p>Es realmente difícil diferenciar una batería de ciclo profundo de una de arranque a simple vista, porque como lo hemos mencionado, al tener un mercado tan pequeño, comparado con las baterías de arranque, muchos fabricantes de baterías comunes ó especiales nos ofrecen baterías con diferentes rótulos como ser: ¨ BATERÍAS MARINAS ¨, ¨ BATERÍAS DE BAJO MANTENIMIENTO ¨, ¨ BATERÍAS ESTACIONARIAS ¨, ¨ BATERÍAS FREE WATER ¨, ¨ BATERÍAS DE GEL ¨, ¨ BATERÍAS DE DOBLE PROPOSITO (DUAL PURPOSE) ¨ etc.</p>

<p>Es muy probable que la gran mayoría de estas baterías, no sean realmente de CICLO PROFUNDO.</p>

<p>Finalmente; no nos achiquemos porque las baterías tengan tapitas y agua destilada. Yo se que parecen más lindas las baterías que no requieren mantenimiento, pero eso no significa que sean las más adecuadas.</p>

<p>Para dimensionar el banco de baterías de servicio, en primer lugar debemos saber que si por ejemplo, en nuestra embarcación tenemos un banco de servicio de 100 amperes, en realidad solamente podremos utilizar 50 amperes. Los otros 50 amperes existen pero no son utilizables.</p>

<p>Es decir debemos dividir por dos la totalidad de amperes disponibles en nuestro banco de servicio, dado que cuando las baterías llegan al 50% de su capacidad ya no entregan 12 voltios y entonces continuamos teniendo energía acumulada, pero que no nos será de utilidad.</p>

<p>A esto en ingles se lo conoce como ¨The Mid Capacity Rule¨. Y es muy importante que usted lo tenga siempre en cuenta.</p>

<p>Lo próximo a saber es la totalidad de consumos que tendremos entre los períodos de carga.</p>

<p>Para esta cuenta deberemos saber los amperes que cada equipo ó elemento consume por hora.</p>

<p>Si el consumo esta indicado en Watts y los equipos operan con 12 voltios, deberemos dividir los Watts por los Voltios para obtener amperes/hora.</p>

<p>En este ejemplo si una bombita consume 30 Watts, al dividir 30 por 12 obtendremos 2.5 amp./hora de consumo, es decir en una hora esa bombita habrá consumido 2.5 amps. Y en dos horas serán 5 amperes.</p>

<p>Si usamos un Inverter (aparato que convierte 12 VCC en 220 VCA) la cuenta es similar. Por ejemplo un televisor que consume unos 60 Watts, nos dará 60 dividido 12 = 5 amps./hora. Como los inverters tienen una pérdida de aproximadamente del 10% deberemos agregar este factor y llegaremos a que nuestro televisor consume unos 5.50 amps./hora. En realidad 3 horas contínuas de televisor encendido habremos consumido unos 16.50 amps./hora .</p>

<p>Sumando todos los consumos por hora, llegaremos a una determinada cantidad de amperes. Nuestro banco de servicio deberá tener como mínimo el doble de esos amperes.</p>

<p>Sobre la carga de la batería, básicamente hay dos tipos de alternadores: los alternadores automotrices y los alternadores marinos. Alternador automotriz es el que viene instalado con el motor de su embarcación ( hablamos de motores internos ).</p>

<p>Estos alternadores tienen muy poco rendimiento, porque fueron diseñados básicamente para recuperar las baterías de arranque, de tal manera que nuestro motor pueda arrancar sin problemas y en todo momento pero la realidad es que para lograr esto, hace falta mucho menos carga que la que cualquiera de nosotros imagina.</p>

<p>En realidad el proceso de arranque toma muchísimos amperes, todos juntos pero como el proceso demora solo un par de segundos, al cabo del arranque nuestras baterías habrán perdido muy pocos amperes/hora.</p>

<p>Usted se sorprendería si tuviese un lector de amperes horas consumidos, porque un arranque con un motor diesel frío y en invierno no consume más de 1 a 3 amperes/hora.</p>

<p>Para recuperar esta carga no hace falta un alternador demasiado potente ni que esté preparado para trabajos continuos. La realidad es que nuestro alternador trabajará unos pocos minutos, y de inmediato el regulador de voltaje que generalmente está incorporado en el interior del alternador interrumpirá la carga.</p>

<p>De lo analizado podemos obtener conclusiones básicas: El alternador (automotriz) fue diseñado para producir poca carga, en períodos cortos y no nos llenará al 100% la batería.</p>

<p>Esto no constituye un problema de baterías de arranque, pero cuando nos referimos al banco de servicio la cosa es muy diferente. Las baterías de servicio sufren descargas profundas y sería muy importante para aprovechar al máximo su capacidad poder cargarlas al 100%.</p>

<p>Esto solo se logra con un alternador marino. Equipo no estándar, y cuyas principales características son las siguientes:</p>

<p>-Fueron diseñados para operar a trabajo contínuo y por eso tienen doble ventilación y sistemas de sensado y protección de temperatura adicionales.</p>

<p>-Están construidos en materiales más resistentes a la oxidación y sulfatación.</p>

<p>-Utilizan un regulador de voltaje externo que permite cargar las baterías al 100% en lugar de un regulador interno de tipo automotriz.</p>

<p>-Algunos tienen reguladores de voltaje de etapas múltiples, y en este caso su rendimiento es el doble ó hasta el triple de carga comparado con alternador automotriz de la misma capacidad.</p>

<p>-Las poleas, rulemanes, correas y sistemas de montaje son diferentes, porque estos alternadores producen mayor resistencia que los automotrices.</p>

<p>Calcula usted que restarán aproximadamente 1 HP por cada 25 amperes de carga contínua (a máxima carga ) . Es decir que un alternador marino de 100 amperes continuos consume en su máximo esfuerzo unos 4 HP.</p>

<p>Si comparamos un alternador automotriz de por ejemplo 50 amperes con uno marino de la misma capacidad conectado a un regulador de voltaje de etapas múltiples tendremos que en promedio, el primero nos entregará unos 15 amperes/hora mientras que el segundo nos producirá 50 amperes en el mismo período de tiempo.</p>

<p>Esto quiere decir que con un alternador marino en una hora produciremos tres veces más energía que con un alternador automotriz.</p>

<p>Para la carga de baterías a la red de 220 VCA, desconfíe de la frase ¨ cargador inteligente ¨. Muchas veces le ofrecerán cargadores inteligentes y en realidad le estarán entregando un cargador de baterías que tiene una salida de tensión fija de por ejemplo 13.7 voltios.</p>

<p>Estos cargadores son muy pocos eficientes. Por ejemplo un cargador de este tipo de 30 amperes le entregará 30 amperes cuando su batería esté totalmente descargada pero al cabo de unos pocos minutos bajará a 20 amperes y luego a 15 y en realidad su batería estará con muy poca carga. Estos cargadores al igual que los alternadores automotrices cargan las baterías hasta el 80% de su capacidad y entregan muy poca carga promedio.</p>

<p>De esta forma nunca sobrecargarán su batería y usted escuchará que son tan inteligentes que usted los puede dejar prendidos todo el tiempo porque cortarán automáticamente cuando sus baterías estén llenas. Lo que no le dicen es que este proceso toma muchísimas horas y las baterías no se llenan al máximo.</p>

<p>Al igual que los alternadores marinos usted deberá buscar un ¨ cargador de etapas múltiples “, que como su nombre lo menciona, trabaja en tres etapas diferentes para recuperar su batería al 100 %.</p>

<p>Para una buena vida útil, lo primero que debo saber es que las baterías de arranque no deben descargarse profundamente.</p>

<p>Si uso baterías de arranque en banco de servicio no debo descargarlas más de un 40%.</p>

<p>Superar este valor significa acortarle la vida útil, pero también la capacidad de carga.</p>

<p>Es como si la batería fuese un tanque de agua, con cada descarga profunda produzco sarro en el tanque y disminuyo su capacidad de almacenamiento.</p>

<p>Calcule usted que en este ejemplo podríamos relacionar los amperes/hora con litros de agua. Si mi tanque tiene 100 litros de capacidad, con cada descarga profunda le reduzco en un 10 % la capacidad y entonces en la tercera descarga profunda solamente podré almacenar 70 litros en lugar de los 100 originales.</p>

<p>Siguiendo con la misma analogía es importante saber que las baterías forman sarro con el tiempo. Es decir al cabo del primer año nuestro ¨ tanque de agua ¨ perderá un 30% de su capacidad. Al segundo estaremos con 60% de capacidad reducida y entonces nuestros 100 litros serán apenas 40. Esto equivale a que en nuestra batería su capacidad haya variado de 100 amperes a 40 en solo dos años. ¿Por qué no miramos cuando compramos nuestras baterías y veremos cuantos amperes/hora reales tenemos disponibles hoy? .</p>

<p>Las baterías de plomo ácido (recomendadas para uso marino en sus dos tipos arranque y ciclo profundo ) sufren auto-descargas que oscilan aproximadamente entre 7 y el 8% de su capacidad por mes.</p>

<p>Esto quiere decir que si dejamos las baterías en stand-by por dos meses, estas habrán perdido un 15% de su capacidad almacenada en el momento que las dejamos cargadas en la embarcación.</p>

<p>El régimen de auto-descarga es variable según la temperatura; a mayor temperatura mayor descarga.</p>

<p>La vida útil de una batería también está afectada por la temperatura, a mayor temperatura mayor velocidad de envejecimiento.</p>

<p>A partir de los 23 grados centígrados, y por cada 9 grados de incremento de la temperatura la vida útil puede acortarse a la mitad.</p>

<p>La conclusión que obtenemos acá es que cuanto más frío hace, más duran las baterías y menos se auto-descargan. Esto es real pero juega en contra que el esfuerzo que las baterías deben hacer para arrancar un motor en lugares fríos es mayor y esto es el único inconveniente que el frío les produce. Para todo el resto les favorece.</p>

<p>Por último podemos concluir que para saber cuanta carga tenemos en un batería, habitualmente usamos el voltímetro. Este es un método bastante impreciso de medición. En primer lugar una batería sometida a carga levanta inmediatamente su voltaje y el voltímetro nos dará lecturas de tensión irreales. Para saber el voltaje de las baterías debo dejarlas en reposo por varias horas.</p>

<p>Por otra parte debemos tener un voltímetro muy preciso (en general los que se ven en los barcos son mucho menos precisos de lo que el propietario cree ) .</p>

<p>Debemos siempre dejar la batería en reposo por más de 10 horas si queremos obtener lecturas precisas.</p>

<p>¿Entonces como hago para saber cuanta carga realmente tengo?</p>

<p>La respuesta es comprar un contador de amperes/horas (ojo, esto no es un amperímetro).</p>

<p align="center" class="commontext"><b>Los navegantes de NAVemoción pueden aclarar todas sus dudas acerca del tema “Baterías” comunicándose con los especialistas en el tema de "BATERÍAS LOGAR”:<br>
<span class="bluetext">4765-3547</span> o al e-mail: <a href="mailto:danielgarcia1903@gmail.com:"><span id="cloak63d218ae6e30bf5dba48b578b0a7c577"><a href="mailto:danielgarcia1903@gmail.com">danielgarcia1903@gmail.com</a></span><script type="text/javascript">
				document.getElementById('cloak63d218ae6e30bf5dba48b578b0a7c577').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addy63d218ae6e30bf5dba48b578b0a7c577 = 'd&#97;n&#105;&#101;lg&#97;rc&#105;&#97;1903' + '&#64;';
				addy63d218ae6e30bf5dba48b578b0a7c577 = addy63d218ae6e30bf5dba48b578b0a7c577 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
				var addy_text63d218ae6e30bf5dba48b578b0a7c577 = 'd&#97;n&#105;&#101;lg&#97;rc&#105;&#97;1903' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloak63d218ae6e30bf5dba48b578b0a7c577').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy63d218ae6e30bf5dba48b578b0a7c577 + '\'>'+addy_text63d218ae6e30bf5dba48b578b0a7c577+'<\/a>';
		</script>:</a></b></p>

<p align="center" class="commontext">&nbsp;</p>

<hr>
<p align="center" class="commontext">&nbsp;</p>

<p align="center" class="commontext"><span style="font-size: 14pt;"><strong><a id="baterias2"></a>LAS BATERÍAS DE CICLO PROFUNDO (Nota 2)</strong></span></p>

<p>Las baterías de Ciclo Profundo están diseñadas para proporcionar la performance de vida más larga cuando se descarga y recarga la misma continuamente.<br>
Diferente a las baterías de automóvil normales para arranque, las cuales poseen rejillas de plomo más delgadas y el material activo poroso (la pasta de óxido de plomo que cubre a las rejillas de la batería) para aumentar al máximo el área de la superficie de la rejilla logrando más potencia en los estallidos instantáneos de corriente, se construyen las baterías del Ciclo Profundo con las rejillas con más espesor, una aleación de alto contenido de antimonio y una pasta más densa de material activo para resistir descargas constantes y ciclos de carga.<br>
Un Ciclo, en términos de batería, es el proceso en el cual se descarga completamente (sacándole toda su capacidad) y se recarga completamente (restaurando toda su capacidad).<br>
Para entender bien por qué usted necesita una batería del ciclo profundo para muchas aplicaciones, nosotros compararemos una de estas baterías con una batería de automóvil normal.<br>
La batería de automóvil podría llamarse “de ciclo poco profundo”. Se diseñan para mantener estallidos de alta corriente en tiempos muy cortos; simplemente cada estallido es mucho tiempo como para dar arranque a un automóvil.<br>
En este proceso, sólo una porción pequeña de la capacidad de la batería se utiliza, y el alternador del automóvil restaura esta descarga rápidamente.<br>
La construcción de la batería de Ciclo Profundo le permite entregar esta energía por períodos extendidos de tiempo (el ciclo profundo) sin dañar la misma ni minimizando su vida útil, tal uso causaría una reducción importante en la vida útil en una batería para automotor normal.<br>
Todas las baterías pueden entregar algunos “ciclos profundos” muy pocas veces, pero sólo las diseñadas especialmente para este objetivo sobrevivirán la descarga sustancial repetidamente. Con el diseño y la fabricación correcta la batería de Ciclo Profundo resistirá centenares de ciclos a aproximadamente el 80% de profundidad de descarga, en cada ciclo, y todavía estar lista para más.</p>

<p align="left" class="bluetext"><b>Las verdaderas baterías de Ciclo Profundo tienen:</b></p>

<p>Las rejillas de Plomo mucho más gruesas</p>

<p>La densidad del material activo más elevada</p>

<p>El porcentaje de Antimonio en la aleación rejilla positiva mucho mayor</p>

<p>Separadores hechos de caucho y papel de fibra de vidrio diseñados para retener el material activo.</p>

<p>Cada componente es crítico para proporcionar una performance buena y durable.</p>

<p>Las rejillas gruesas extienden la vida útil.</p>

<p>La cantidad de materia más activa aumenta la capacidad.</p>

<p>El antimonio mejora la habilidad del ciclo profundo.</p>

<p>El diseño del separador afecta la eficacia con que opera, la longevidad y el control de pérdida de líquido.</p>

<p>Las baterías de ciclo profundo se miden en Amperios Horas, dividido el número de horas, normalmente 20hrs. La medida de CCA (cold cranking amp.) no se utiliza generalmente en las baterías de ciclo profundo por su bajo valor.</p>

<p align="center" class="bluetext"><b>Baterías de Ciclo profundo - Hoja de Cálculo</b></p>

<p>Hace un siglo que se diseñan las baterías de Ciclo Profundo para aplicaciones específicas. Así que, es importante tener la capacidad adecuada en la batería para la cantidad de corriente en su uso (conocido como amp/hrs.).<br>
Usted puede hacer un ejercicio rápida y fácilmente estableciendo el consumo de corriente de cada parte del equipo (marcado en el tablero de información del equipo), el número de Horas que usted usará entre las recargas, y el Voltaje del sistema.</p>

<p align="left" class="commontext"><b>Por ejemplo:</b></p>

<p><i>Equipo Carga (Watts) Uso estimado (horas) Watts/Horas<br>
Refrigerador 40 10.0 400<br>
Interior Enciende 20 4.0 80<br>
Torno 90 0.2 18<br>
Watts/Horas totales 498</i></p>

<p>Ahora se divide el total de Watts/horas totales por el Voltaje para obtener los Amperio/Horas.</p>

<p align="left" class="commontext"><b>Por ejemplo:</b></p>

<p><i>498 watts/Horas ÷ 12 Voltios = 41.5 Amperio/Horas</i></p>

<p>Esta figura es básicamente su guía al tamaño de la batería que usted necesita, más a popa el cálculo muy más importante. Los sistemas eléctricos de los vehículos no siempre son perfectos, nosotros le aconsejamos fuertemente que diseñe siempre la capacidad con un poco de reserva extra.<br>
Un margen del 30% de seguridad es una concesión razonable.</p>

<p align="left" class="commontext"><b>Por ejemplo:</b></p>

<p><i>Estimado de Amperios/horas 41.5<br>
más 30% seguridad margen 12.45<br>
Total de Amperios/Horas 53.95</i></p>

<p>Ahora usted puede seleccionar la batería de Ciclo Profundo correcta para sus necesidades, simplemente comparando su Amperio Total calculado contra la especificación de la batería.</p>

<p>Cuanto más rápido una batería se descarga, menos Amperio/Horas entregará antes de la recarga.</p>

<p>Si una batería se cicla al 20% DOD (Depth of Discharge - Profundidad de Descarga) sería razonable esperar más de 1200, a 50% DOD 6-700 ciclos, y a 80% DOD que es lo más común 3-400 ciclos.</p>

<p>Estas baterías necesitan ser recargadas lo más pronto posible después de su uso o comenzará la sulfatación permanente en las rejillas de plomo.</p>

<p align="center" class="commontext"><b>Los navegantes de NAVemoción pueden aclarar todas sus dudas acerca del tema “Baterías” comunicándose con los especialistas en el tema de "BATERÍAS LOGAR”:<br>
<span class="bluetext">4765-3547</span> o al e-mail: <span id="cloakba78fb0812dcf4d6eb37593cead8e31f"><a href="mailto:danielgarcia1903@gmail.com">danielgarcia1903@gmail.com</a></span><script type="text/javascript">
				document.getElementById('cloakba78fb0812dcf4d6eb37593cead8e31f').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addyba78fb0812dcf4d6eb37593cead8e31f = 'd&#97;n&#105;&#101;lg&#97;rc&#105;&#97;1903' + '&#64;';
				addyba78fb0812dcf4d6eb37593cead8e31f = addyba78fb0812dcf4d6eb37593cead8e31f + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
				var addy_textba78fb0812dcf4d6eb37593cead8e31f = 'd&#97;n&#105;&#101;lg&#97;rc&#105;&#97;1903' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloakba78fb0812dcf4d6eb37593cead8e31f').innerHTML += '<a ' + path + '\'' + prefix + ':' + addyba78fb0812dcf4d6eb37593cead8e31f + '\'>'+addy_textba78fb0812dcf4d6eb37593cead8e31f+'<\/a>';
		</script></b></p>
</div>
</div>
</div>
</div>
</div>
</div>
	</div>
	    </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
