<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Astillero - Velas</h1>
        </div>
      </div>
        <div class="container grey">
        <div itemprop="articleBody">
		<div class="commontext" style="text-align: center;"><span style="font-size: 14pt;"><strong>VELAS</strong></span></div>
<div class="commontext" style="text-align: center;">&nbsp;</div>
<div class="commontext" style="text-align: center;">
<p class="bluetext" align="center"><b>MEJORANDO EL TRANSFORMADO</b></p>
<p>Las velas pueden repararse, reforzarse y protegerse del sol, para prolongar su vida útil. <br>Es posible recortarlas para controlar su estiramiento, mejorando su forma y performance. También, mediante la técnica adecuada, transformarlas hasta convertirlas en otra vela, para mejorar sus capacidades, cambiándolas tanto que modifiquen su función original.</p>
<p class="bluetext" align="center"><b>COMO PROLONGAR LA VIDA ÚTIL DE LAS VELAS</b></p>
<p>Un lifting puede hacer lo suyo en caras, colas, lolas y otras anatomías, o la morocha pasa a ser rubia mágicamente, sin que nada cambie en esencia (la vecina de la vuelta, seguirá siendo la vecina de acá a la vuelta) . <br>Hablando de velas su esencia es la tela en que están construidas. Cualquier tela que esté quemada por el sol o envejecida no justifica ninguna reparación o modificación (léase gastar plata en ellas), ya que la vela se romperá o estirará tanto que se convertirá en lo que vulgarmente se conoce como "bolsa de papas". <br>Otra limitación está dada por la resistencia del material. Por ejemplo, un Genoa 1 (es el genoa más grande) de dacron de 4 onzas (se mide en onzas el grosor de una tela) en buen estado de un velero de 20 pies, no podrá ser recortado y transformado para hacer un tormentín, ya que además de reducir la superficie es necesario que la resistencia del material sea adecuada a la exigencia a la que va a estar expuesta (Un tormentín para un 20 pies deberá ser de por lo menos 6 o 7 onzas, de nada serviría que reforcemos los puños y costuras si la tela no es la adecuada.)</p>
<p class="bluetext" align="center"><b>GENOAS</b></p>
<p>El envejecimiento normal de su forma, por el tiempo o por el uso intensivo que sufren los genoas, se traduce en:</p>
<p>1) Estiramiento general y bolsa desplazada hacia popa.</p>
<p>2) Baluma estirada con mayor tendencia a gualdrapear, obligándonos a repicar más el balumero.</p>
<p>Este es un service normal que puede realizarse a una vela, siguiendo en la mayoría de los casos estos procedimientos:</p>
<p>1) Achatado del genoa en general, haciendo más recta la curva de gratil.</p>
<p>2) Acortado del largo de gratil, para aumentar el registro y posibilitar un mayor repicado, obteniéndose como resultado que la bolsa del genoa se desplace más a proa.</p>
<p>3) Acortado de la baluma (algunos centímetros), para rehacer su curva y volverla más negativa.</p>
<p>4) También es oportuno colocar un balumero (si la vela no lo tuviere) o reemplazar el ya existente (en velas de cierta antigüedad el cabo de los balumeros es un chicle).</p>
<p>En genoas de regata, de materiales laminares más resistentes al estiramiento valen las mismas recetas, en tanto y en cuanto estas velas no hayan sufrido grandes deformaciones, debido a un uso con vientos mayores a los recomendados por el fabricante de la vela. Una transformación visual muy apreciable es cuando quiere adaptarse una vela para usar en un enrrollador, ya que en este caso habrá que agregar la correspondiente banda de protección ultravioleta, elemento indispensable si no quiere terminarse con un genoa hecho flecos en uno o dos años)</p>
<p class="bluetext" align="center"><b>MAYORES DE BATTENS CORTOS A FULL BATTENS</b></p>
<p>Durante décadas las velerías hicieron negocio recosiendo fundas y reponiendo battens perdidos en el río. A partir de la aparición de las mayores full battens, este pingüe negocio desapareció. <br>Desde cualquier punto de vista: mantenimiento, performance, comodidad, y durabilidad, las mayores con battens largos tienen ventajas sobre las mayores con battens cortos. <br>El problema de las fundas rotas o descosidas y el de los battens voladores, con un sistema full battens se elimina (por supuesto, cuando está bien hecho), ya que los battens están encapsulados en terminales plásticas que minimizan la posibilidad de que el batten rompa la funda y se vuele. Además como la vela es más plana y rígida, flamea menos (el gualdrapeo es, por lejos, el factor de envejecimiento más importante para una vela) y por lo tanto habrá muchas menos posibilidades de que se produzca una rifadura. El bono adicional de la full batten es la comodidad al arriar e izar, si se la complementa con un sistema lazy jack (sistema de plegado automático de la vela).</p>
<p class="bluetext" align="center">REALIZÓ ESTE ARTICULO PARA "NAVEMOCION" OSCAR RENNER, TITULAR DE LA VELERIA "WIND", A QUIEN PUEDE CONSULTARSE SOBRE CUALQUIER ASPECTO RELACIONADO CON LA REPARACION Y CONFECCION DE VELAS, CARPAS, CHUBASQUERAS, LONAS O CUBREMAYORES, DIRECTAMENTE EN LA PROPIA EMBARCACION, AL 4744-0271 O AL 15-54124680.</p>
<p align="center"><img class="img-fluid" src="<?php echo $link; ?>img/logovelas.jpg"></p>
</div>
<p style="text-align: center;"><br><br></p>	</div>
	    </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
