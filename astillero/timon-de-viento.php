<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Astillero - </h1>
        </div>
      </div>
        <div class="container grey">
        <div itemprop="articleBody">
		<div class="commontext">
<div class="commontext">
<div class="commontext">
<div class="commontext">
<div class="commontext">
<p align="center"><img class="img-fluid" src="<?php echo $link; ?>img/logopilomat.jpg"></p>
<p class="bluetext" align="center"><span style="font-size: 14pt;"><b>CARACTERÍSTICAS Y PRINCIPIO DE FUNCIONAMIENTO</b></span></p>
<p class="bluetext" align="center">&nbsp;</p>
<p><img class="photo" class="img-fluid" src="<?php echo $link; ?>img/pilomat_1.jpg" align="right"> A grandes rasgos el principio de funcionamiento del Pilomat se basa en el aprovechamiento de la fuerza ejercida por el agua (mucho mayor que la del viento) sobre una pala espada sumergida y que pivotea sobre un eje horizontal (ver fotografías). La parte mas importante del principio de funcionamiento de este sistema esta constituida por la pala espada, el fletner, el cuadrante de guardines y la veleta. A todo ello debe agregarse la construcción y el equilibrio que se deben tener en cuenta respetando los distintos brazos de palanca en cada uno de sus componentes.</p>
<p><img class="img-fluid" src="<?php echo $link; ?>img/pilomat_2.jpg" align="left"> Cuando el barco navega normalmente la veleta (que se encuentra en direc- ción al viento aparente) permanece vertical y tanto la pala espada como el fletner mantienen la dirección indicada en gris claro de la figura A. <br>Si el barco se aparta de su rumbo el viento incide sobre una de las caras de la veleta haciéndola pivotear sobre su eje horizontal, este pivoteo, y mediante un sistema de palancas muy equilibrado hace girar el fletner, (el cual actúa como pequeño timón de la pala espada) produciéndose un giro de la misma sobre su eje vertical. (posición negra de la figura A). <br>En esta posición y gracias a la estropada del barco las líneas de agua que inciden sobre la pala espada producen un pivoteo de la misma en su eje horizontal ( tomando entonces la posición indicada en B).</p>
<p>Este pivoteo es transmitido a un cuadrante de guardines (como se puede apreciar en las fotografías) y son los guardines (ó cabitos) los que transmiten la información a la caña de timón, corrigiendo de esta forma el rumbo del barco.</p>
<p><img class="photo2" class="img-fluid" src="<?php echo $link; ?>img/pilomat_3.jpg" align="left"> El pilomat esta totalmente realizado en acero inoxidable, con sus soldaduras de argón y sus partes se mueven sobre bujes de bronce.</p>
<p>El conjunto es total o parcialmente desmontable, pudiéndose guardar de esta forma en el interior del barco.</p>
<p>Para el modelo standard (barcos de hasta 35’) su peso es de 14 Kg; aumentando algo para barcos de mayor eslora.</p>
<p>En caso de querer instalarlo por su cuenta entregamos un instructivo descontando el valor de la instalación, o bien lo instalamos nosotros.</p>
<p>&nbsp;</p>
<table width="80%" align="center">
<tbody>
<tr>
<td><img class="img-fluid" src="<?php echo $link; ?>img/pilomat_4.jpg"></td>
<td><img class="img-fluid" src="<?php echo $link; ?>img/pilomat_5.jpg"></td>
</tr>
</tbody>
</table>
<p>Esperando se encuentre entre nosotros, ya que de decidirse e instalarlo se transformará en un promotor del mismo…………. como ya lo han hecho los veleros más “devoradores de millas” de nuestro río.</p>
<p align="center">Estos timones son realizados en nuestro país con gran calidad, la misma que emplean los principales fabricantes del mundo. <br>Todos los interesados en conocer más detalles acerca del funcionamiento y costos de este gran aliado del navegante, pueden solicitar datos al <span class="bluetext"><b>4251-3952</b></span> o a <span id="cloak647edfa76c14be5b8f0ee21e6c5b7da7"><a href="mailto:pilomatargentina@hotmail.com">pilomatargentina@hotmail.com</a></span><script type="text/javascript">
				document.getElementById('cloak647edfa76c14be5b8f0ee21e6c5b7da7').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addy647edfa76c14be5b8f0ee21e6c5b7da7 = 'p&#105;l&#111;m&#97;t&#97;rg&#101;nt&#105;n&#97;' + '&#64;';
				addy647edfa76c14be5b8f0ee21e6c5b7da7 = addy647edfa76c14be5b8f0ee21e6c5b7da7 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
				var addy_text647edfa76c14be5b8f0ee21e6c5b7da7 = 'p&#105;l&#111;m&#97;t&#97;rg&#101;nt&#105;n&#97;' + '&#64;' + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloak647edfa76c14be5b8f0ee21e6c5b7da7').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy647edfa76c14be5b8f0ee21e6c5b7da7 + '\'>'+addy_text647edfa76c14be5b8f0ee21e6c5b7da7+'<\/a>';
		</script>. También puede solicitarse una charla informativa gratuita.</p>
<p align="center"><b>FABRICACIÓN Y VENTA SCHENKEL Hnos. <br>Carlos Gardel 308 (1876) Bernal – Prov. de Buenos Aires <br>e-mail: <span id="cloak3f97e64cfa81f871330a723300407089"><a href="mailto:pilomatargentina@hotmail.com">pilomatargentina@hotmail.com</a></span><script type="text/javascript">
				document.getElementById('cloak3f97e64cfa81f871330a723300407089').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addy3f97e64cfa81f871330a723300407089 = 'p&#105;l&#111;m&#97;t&#97;rg&#101;nt&#105;n&#97;' + '&#64;';
				addy3f97e64cfa81f871330a723300407089 = addy3f97e64cfa81f871330a723300407089 + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';
				var addy_text3f97e64cfa81f871330a723300407089 = 'p&#105;l&#111;m&#97;t&#97;rg&#101;nt&#105;n&#97;' + '&#64;' + 'h&#111;tm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloak3f97e64cfa81f871330a723300407089').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy3f97e64cfa81f871330a723300407089 + '\'>'+addy_text3f97e64cfa81f871330a723300407089+'<\/a>';
		</script> Tel.: <span class="bluetext">4251-3952</span></b></p>
</div>
</div>
</div>
</div>
</div>	</div>
	    </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
