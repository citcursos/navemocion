<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Astillero - Seguros</h1>
        </div>
      </div>
        <div class="container grey">
        <div itemprop="articleBody">
		<p class="bluetext" align="center"><span style="font-size: 14pt;"><b>SEGUROS</b></span></p>
<hr>
<p class="bluetext" align="center">&nbsp;</p>
<div class="commontext">
<p align="center"><img  class="img-fluid" src="<?php echo $link; ?>img/logosfa.gif"></p>
<p>Resulta importante conocer los tipos de coberturas que pueden contratarse para las embarcaciones, teniendo en cuenta que en general para las compañías de seguros las embarcaciones se encuentran clasificadas, según se describe a continuación: <br><br></p>
<table border="1" width="60%" cellspacing="2" cellpadding="3" align="center">
<tbody>
<tr>
<td class="bluetext" bgcolor="#C6C3C6"><b>a) LANCHAS</b></td>
<td class="commontext" bgcolor="#C6C3C6">&nbsp;</td>
</tr>
<tr>
<td class="bluetext"><b>b) CRUCEROS</b></td>
<td class="commontext">c/motor diesel <br>c/motor naftero</td>
</tr>
<tr>
<td class="bluetext" bgcolor="#C6C3C6"><b>c) VELEROS</b></td>
<td class="commontext" bgcolor="#C6C3C6">c/motor diesel o sin motor interno <br>c/motor naftero</td>
</tr>
<tr>
<td class="bluetext"><b>d) BOTES NEUMÁTICOS</b></td>
<td class="commontext">Auxiliares de a bordo</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>En cuanto a las coberturas, describiremos solamente dos, una básica y una completa, debiendo considerarse que existen alternativas intermedias de acuerdo a cada Compañía.</p>
<p class="tablemenu" align="center">&nbsp;</p>
<p class="tablemenu" align="center"><span style="font-size: 12pt;"><strong>COMPLETA</strong></span></p>
<p class="tablemenu" align="center">&nbsp;</p>
<p>Pérdida y/o daños causados por: naufragio, incendio, varamiento, colisión, rayo y/o explosión.</p>
<p>&nbsp;</p>
<ul>
<li>Robo de la embarcación completa (excluyendo e; domicilio particular y cualquier otro lugar de guarda que no sea una guardería náutica)</li>
<li>Robo de elementos fijos al casco.</li>
<li>Temporal e Inundación.</li>
<li>Navegación oceónica conforme a las posibilidades de la embarcación y a la autorización otorgada por la PNA.</li>
<li>Huelga y Vandalismo.</li>
<li>Tránsito en trailer en Rep.Arg. y R.O.U (Cubriendo pérdidas y/o Averías a causa de choque, vuelco incendio y/o Desbarrancamiento del medio transportador)</li>
<li>Incendio en guardería</li>
<li>Rotura de mástil.</li>
<li>Responsabilidades: <br>Resp.Civil por colisión a cosas de terceros <br>Resp.Civil a personas transportadas y no Transp. <br>Resp.Civil por Incendio <br><b>Hasta la suma Asegurada de la embarcación en su conjunto</b>
<p class="tablemenu" align="center">&nbsp;</p>
<p class="tablemenu" align="center"><span style="font-size: 12pt;"><strong>BÁSICA</strong></span></p>
<p>&nbsp;</p>
</li>
<li>Pérdida total de la embarcación.</li>
<li>Robo de la embarcación completa (excluyendo e; domicilio particular y cualquier otro lugar de guarda que no sea una guardería náutica)</li>
<li>Averías particulares por incendio, rayo, explosión, naufragio, colisión y/o varamiento</li>
<li>Huelga y Vandalismo.</li>
<li>Responsabilidades: <br>Resp.Civil por colisión a cosas de terceros <br>Resp.Civil a personas transportadas y no Transp. <br>Resp.Civil por Incendio <br><b>Hasta la suma Asegurada de la embarcación en su conjunto</b>
<p>También hay que tener en cuenta al momento de la contratación, que algunas Compañías otorgan bonificaciones por buen resultado o descuentos por licencias náuticas de patrón o piloto. <br>Otras brindan servicios de asistencia por desperfectos mecánicos y/o servicios de transporte o remolque de la embarcación y/o servicio de emergencias médicas.</p>
<p align="center">Realizó esta nota para NAVemoción, Miguel Angel Farace, Productor Asesor de Seguros, con amplia experiencia en embarcaciones deportivas. Por cualquier consulta comunicarse al <span class="bluetext"><b>4249-0736</b></span> o a <span id="cloakf30a10eaaa9ecd23c1fc6ea5fdc898e9"><a href="mailto:seguros@santosfarace.com.ar">seguros@santosfarace.com.ar</a></span><script type="text/javascript">
				document.getElementById('cloakf30a10eaaa9ecd23c1fc6ea5fdc898e9').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addyf30a10eaaa9ecd23c1fc6ea5fdc898e9 = 's&#101;g&#117;r&#111;s' + '&#64;';
				addyf30a10eaaa9ecd23c1fc6ea5fdc898e9 = addyf30a10eaaa9ecd23c1fc6ea5fdc898e9 + 's&#97;nt&#111;sf&#97;r&#97;c&#101;' + '&#46;' + 'c&#111;m' + '&#46;' + '&#97;r';
				var addy_textf30a10eaaa9ecd23c1fc6ea5fdc898e9 = 's&#101;g&#117;r&#111;s' + '&#64;' + 's&#97;nt&#111;sf&#97;r&#97;c&#101;' + '&#46;' + 'c&#111;m' + '&#46;' + '&#97;r';document.getElementById('cloakf30a10eaaa9ecd23c1fc6ea5fdc898e9').innerHTML += '<a ' + path + '\'' + prefix + ':' + addyf30a10eaaa9ecd23c1fc6ea5fdc898e9 + '\'>'+addy_textf30a10eaaa9ecd23c1fc6ea5fdc898e9+'<\/a>';
		</script></p>
<p align="center">&nbsp;</p>
<p align="center">&nbsp;</p>
<p align="center"><span class="commontext"><b>Of. Lanús:</b></span> Av. H. Yrigoyen 5881 R. De Escalada Tel.: <span class="bluetext"><b>4249-0736</b></span> Fax: <span class="bluetext"><b>4249-4004</b></span><br><span class="commontext"><b>Of. Villa Gesell:</b></span> Av. Bs. As. Y Alameda 209 - Tel.: <span class="bluetext"><b>(02255) 45-8755 / 4955</b></span><span class="commontext"><br><b>Pinamar</b></span> Tel.: <span class="bluetext"><b>(02254) 48- 5225</b></span></p>
</li>
</ul>
</div>	</div>
	    </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
