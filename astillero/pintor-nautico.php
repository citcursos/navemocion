<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Astillero - Pintor Náutico</h1>
        </div>
      </div>
        <div class="container grey">
        <div itemprop="articleBody">
		<p style="text-align: center;"><span style="font-size: 14pt;"><strong>Pintor Náutico</strong></span></p>
<p style="text-align: center;">&nbsp;</p>
<table border="0" cellspacing="10" align="center">
<tbody>
<tr>
<td>
<div align="center"><img class="img-fluid" src="<?php echo $link; ?>img/melquisedeclogo.jpg" width="229" height="160"></div>
</td>
</tr>
<tr>
<td><img class="img-fluid" src="<?php echo $link; ?>img/melquisedecbolantes.jpg" width="383" height="470" vspace="15"></td>
</tr>
</tbody>
</table>
<table border="0" cellspacing="10" align="center">
<tbody>
<tr>
<td><img class="img-fluid" src="<?php echo $link; ?>img/pint1.jpg"></td>
<td><img class="img-fluid" src="<?php echo $link; ?>img/pint2.jpg"></td>
</tr>
<tr>
<td><img class="img-fluid" src="<?php echo $link; ?>img/pint3.jpg"></td>
<td><img class="img-fluid" src="<?php echo $link; ?>img/pint4.jpg"></td>
</tr>
<tr>
<td><img class="img-fluid" src="<?php echo $link; ?>img/pint5.jpg"></td>
<td><img class="img-fluid" src="<?php echo $link; ?>img/pint6.jpg"></td>
</tr>
<tr>
<td><img class="img-fluid" src="<?php echo $link; ?>img/pint7.jpg"></td>
<td><img class="img-fluid" src="<?php echo $link; ?>img/pint8.jpg"></td>
</tr>
<tr>
<td><img class="img-fluid" src="<?php echo $link; ?>img/pint9.jpg"></td>
<td><img class="img-fluid" src="<?php echo $link; ?>img/pint10.jpg"></td>
</tr>
</tbody>
</table>	</div>
	    </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
