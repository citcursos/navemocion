<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Astillero - Lonería</h1>
        </div>
      </div>
        <div class="container grey">
        <div itemprop="articleBody">
            <p style="text-align: center;"><span style="font-size: 14pt;"><strong>LONERÍA</strong></span></p>
    <p>&nbsp;</p>
    <table style="width: 850px;">
    <tbody>
    <tr>
    <td style="width: 199px;"><img class="img-fluid" src="<?php echo $link; ?>img/lona1.jpg"></td>
    <td class="commontext" style="width: 641px;"><span class="bluetext"><b>OSCAR RENNER</b></span> <br>EN LA CONFECCIÓN DE COLCHONETAS (COTINES) <br>EL PRECIO MÁS BAJO Y LA MEJOR CALIDAD</td>
    </tr>
    <tr>
    <td style="width: 199px;"><img class="img-fluid" src="<?php echo $link; ?>img/lona2.jpg"></td>
    <td class="commontext" style="width: 641px;"><span class="bluetext"><b>ADEMÁS CONFECCIONAMOS TODOS LOS ACCESORIOS PARA SU BARCO:</b></span> <br>CUBRE MOTORES <br>CUBRE CAÑAS <br>CUBRE TAMBUCHOS <br>CHUBASQUERAS <br>BIMINIS <br>TOLDILLAS <br>CUBRE MOLINETES <br>CUBRE MAYORES <br>CENEFAS <br>PROTECCIONES EN VELAS DE PROA</td>
    </tr>
    <tr>
    <td style="width: 199px;"><img class="img-fluid" src="<?php echo $link; ?>img/lona3.jpg"></td>
    <td class="commontext" style="width: 641px;"><b><span class="bluetext">OSCAR RENER</span> (011) 4744-0271</b> <br>MIGUEL CANÉ 1039, SAN FERNANDO, PROVINCIA DE BUENOS AIR</td>
    </tr>
    </tbody>
    </table>
    <p style="text-align: center;"><br><br></p>	</div>
	    </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
