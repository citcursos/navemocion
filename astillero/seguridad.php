<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Astillero - </h1>
        </div>
      </div>
        <div class="container grey">
        <div itemprop="articleBody">
		<p class="bluetext" align="center"><span style="font-size: 14pt;"><b>SEGURIDAD</b></span></p>
<hr>
<p class="bluetext" align="center">&nbsp;</p>
<p class="bluetext" align="center"><span style="font-size: 12pt;"><b>Radiobalizas de Emergencia Indicadoras de Posición EPIRB</b> </span></p>
<p class="bluetext" align="center"><span style="font-size: 12pt;">Las Balizas</span></p>
<p align="center">&nbsp;</p>
<p align="center"><img  class="img-fluid" src="<?php echo $link; ?>img/idpsa_3.jpg"></p>
<p align="center">&nbsp;</p>
<div class="commontext">
<p>Existen tres tipos diferentes de baliza diseñadas para distintos ambientes y tipos uso. Están las diseñadas para uso aeronáutico (ELT, Emergency Locator Transmiter), las de uso marítimo (EPIRB, Emergency Position Indicator) y los localizadores personales (PLB, Personal Locator Beacon). Todas ellas emiten una señal de emergencia a los satélites de COSPAS-SARSAT.</p>
<p>COSPAS-SARSAT es un sistema internacional de búsqueda y rescate desarrollado originariamente por Estados Unidos, Rusia, Francia y Canadá, a los que ya se sumaron mas de 25 países, con cerca de 28 estaciones terrestres y 15 Centros de Control de Misión distribuidos en todo el mundo. El sistema consiste en una red de satélites, estaciones terrestres, Centros de Control de Misión y Centros de Coordinación de Rescate. Los satélites se utilizan para detectar y localizar las radiobalizas, que pueden pertenecer a un buque, avión o persona y que son activadas en una situación de emergencia.</p>
<p>Las radiobalizas emiten una señal a los satélites de COSPAS-SARSAT que circundan la Tierra en órbita polar o a la nueva generación de satélites geoestacionarios. Esta señal es retransmitida a una estación terrena denominada LUT (Local User Terminall) la que procesa la señal y transfiere los datos, junto con la ubicación de la baliza, a un Centro de Control de Misión. Finalmente este informa al Centro de Coordinación de Rescate mas cercano, adjuntando la información del usuario de la baliza.</p>
<p>El segmento terrestre para América Latina incluye las estaciones de Estados Unidos, así como las de Brasil, Chile y Perú. Brasil comenzó a desarrollar su estación terrestre ya en 1986 y desde 1994 opera las estaciones de Brasilia y Río de Janeiro. Idéntico es el caso de Chile, con una estación en Santiago y otra en Punta Arenas. Perú adquirió una estación en 1995, operando en la base naval del Callao. Por su parte nuestro país esta considerando la instalación de una o dos estaciones en su territorio, una de ellas probablemente en Ezeiza.</p>
<table border="0" cellspacing="30" cellpadding="2" align="center">
<tbody>
<tr>
<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/idpsa_1.jpg"></td>
<td><img alt="" class="img-fluid" src="<?php echo $link; ?>img/idpsa_2.jpg"></td>
</tr>
</tbody>
</table>
<p class="bluetext" align="center">&nbsp;</p>
<p class="bluetext" align="center"><span style="font-size: 12pt;"><b>Radiobalizas EPIRB</b></span></p>
<p>Las EPIRBS, diseñadas especialmente para aplicaciones marinas se dividen en dos grandes grupos:</p>
<p>• Radiobalizas Tipo B: (Magnum B, Mini B, Mini B2). emiten con una frecuencia de 121,5 Mhz y generalmente también en 243 Mhz Una baliza que opera en 121,5 Mhz emite una señal analógica, su radio de cobertura es regional y la precisión de su ubicación es de 6 a 12 millas. Para su detección tanto la baliza como la estación terrestre deben encontrarse simultáneamente en el campo visual de los satélites de órbita polar. Si bien estos fueron los primeros equipos empleados por el sistema han perdido vigencia y solo son de aplicación en la navegación deportiva, dado lo económico de su costo.</p>
<p>• Radiobalizas Digitales de 406 Mhz y 121.5 Mhz. En el caso de las balizas de 406 Mhz la emisión es digital y la cobertura es global. La ubicación de la baliza puede determinarse con una precisión de 1 a 3 millas y no es necesaria la alineación del satélite con la estación terrestre ya que estos cuentan con una unidad de memoria que almacena las señales para retransmitirlas al pasar sobre la próxima estación. La señal emitida por las balizas incluye un código hexadecimal que asociado a la base de datos del sistema permite obtener información sobre el buque, bandera, datos del propietario, etc. Estas balizas emiten adicionalmente una señal de baja potencia en 121,5 Mhz utilizada por los equipos de búsqueda y rescate (SAR “Search and Rescue”) durante la aproximación final. Los nuevos diseños de balizas incorporan equipos GPS o interfases para conectarlas con los que se encuentran a bordo del barco. En caso de activarse la baliza, esta emitirá la ultima posición GPS, que dada su precisión se convierte en la más valiosa ayuda para los equipos SAR. <br><br></p>
<table border="1" width="75%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr bgcolor="009AFF">
<td colspan="3" align="center">Características de los Equipos</td>
</tr>
<tr>
<td bgcolor="#94D3FF">&nbsp;</td>
<td class="commontext" align="center" bgcolor="#94D3FF">Tipo B</td>
<td class="commontext" align="center" bgcolor="#94D3FF">406</td>
</tr>
<tr>
<td class="commontext" bgcolor="#E7F7FF">Características</td>
<td class="commontext" align="center">121.5/243 Mhz</td>
<td class="commontext" align="center">406/121.5 Mhz</td>
</tr>
<tr>
<td class="commontext" bgcolor="#E7F7FF">Cobertura</td>
<td class="commontext" align="center">Regional</td>
<td class="commontext" align="center">Global</td>
</tr>
<tr>
<td class="commontext" bgcolor="#E7F7FF">Precisión en la posición</td>
<td class="commontext" align="center">8 – 12 millas</td>
<td class="commontext" align="center">1 – 3 millas</td>
</tr>
<tr>
<td class="commontext" bgcolor="#E7F7FF">Potencia de transmisión</td>
<td class="commontext" align="center">50 – 100 milivatios</td>
<td class="commontext" align="center">5 vatios</td>
</tr>
<tr>
<td class="commontext" bgcolor="#E7F7FF">Tipo de señal</td>
<td class="commontext" align="center">Analógica</td>
<td class="commontext" align="center">Digital</td>
</tr>
<tr>
<td class="commontext" bgcolor="#E7F7FF">Tiempo de alerta</td>
<td class="commontext" align="center">2 hs.</td>
<td class="commontext" align="center">Instantáneo</td>
</tr>
<tr>
<td class="commontext" bgcolor="#E7F7FF">Localización por Doppler</td>
<td class="commontext" align="center">Dos pasadas</td>
<td class="commontext" align="center">Una pasada</td>
</tr>
</tbody>
</table>
</div>	</div>
	    </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
