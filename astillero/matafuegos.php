<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Astillero - Matafuegos</h1>
        </div>
      </div>
      <div class="container grey">
      <div itemprop="articleBody">
		<p style="text-align: center;"><span style="font-size: 14pt;"><strong>MATAFUEGOS CARGAS</strong></span></p>
<p>&nbsp;</p>
<p>El fuego es uno de los riesgos más peligrosos a bordo de cualquier embarcación. Su origen y sus causas son múltiples, pero hay que tener en cuenta que a bordo más del 75% de los materiales son inflamables. Los métodos más eficaces para combatir el fuego a bordo son: el agua (estamos rodeados de ella), los extintores y las mantas contra incendios. Los extintores son uno de los elementos principales en cuanto a seguridad que se exigen a bordo de las embarcaciones.</p>
<p>&nbsp;</p>
<p><img alt="" class="img-fluid" style="display: block; margin-left: auto; margin-right: auto;" src="<?php echo $link; ?>img/fuegomotor.jpg" alt="incendios"></p>
<table border="0" align="center">
<tbody>
<tr>
<td align="center" width="150"><a href="http://www.matafuegoscargas.com.ar" target="_blank" rel="noopener noreferrer"><img src="<?php echo $link; ?>img/mata.gif" border="0"></a></td>
<td align="center" width="150"><a href="http://www.matafuegoscargas.com.ar" target="_blank" rel="noopener noreferrer"><img src="<?php echo $link; ?>img/mata.gif" border="0"></a></td>
</tr>
<tr>
<td class="commontext" align="center" width="150"><b>Central automática contra incendios</b></td>
<td class="commontext" align="center" width="150"><b>Guardamarina</b></td>
</tr>
</tbody>
</table>
<p><br><br><br></p>
<p class="commontext" align="center"><strong>Matafuegos Cargas</strong><br> Holmberg 4777 (C1430DQQ) Capital Federal <br> Tel.: 011-4541-5086 <br> E-mail: <span id="cloak215f6e21f0ac8663dcc0167425f0f75d"><a href="mailto:info@matafuegoscargas.com.ar">info@matafuegoscargas.com.ar</a></span><script type="text/javascript">
				document.getElementById('cloak215f6e21f0ac8663dcc0167425f0f75d').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addy215f6e21f0ac8663dcc0167425f0f75d = '&#105;nf&#111;' + '&#64;';
				addy215f6e21f0ac8663dcc0167425f0f75d = addy215f6e21f0ac8663dcc0167425f0f75d + 'm&#97;t&#97;f&#117;&#101;g&#111;sc&#97;rg&#97;s' + '&#46;' + 'c&#111;m' + '&#46;' + '&#97;r';
				var addy_text215f6e21f0ac8663dcc0167425f0f75d = '&#105;nf&#111;' + '&#64;' + 'm&#97;t&#97;f&#117;&#101;g&#111;sc&#97;rg&#97;s' + '&#46;' + 'c&#111;m' + '&#46;' + '&#97;r';document.getElementById('cloak215f6e21f0ac8663dcc0167425f0f75d').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy215f6e21f0ac8663dcc0167425f0f75d + '\'>'+addy_text215f6e21f0ac8663dcc0167425f0f75d+'<\/a>';
		</script></p>	</div>
	    </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
