<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12">
          <h1 class="display-4">Astillero - Regla de mar</h1>
        </div>
      </div>
        <div class="container grey">
        <div itemprop="articleBody">
		<div class="commontext">
<div class="commontext">
<div class="commontext">
<div class="commontext">
<p align="center"><span style="font-size: 14pt;"><strong>REGLA DE MAR</strong></span></p>
<p align="center">&nbsp;</p>
<p align="center"><img class="img-fluid" src="<?php echo $link; ?>img/reglamar.jpg"></p>
<p align="center">&nbsp;</p>
<p>Trabajar sobre una carta náutica es una de las tareas más distintivas y cautivantes de todo navegante, la novedosa "Regla de Mar", permite realizar todas las mediciones y trazos de la navegación con total precisión y simplicidad.</p>
<p class="bluetext" align="center"><b>PROCEDIMIENTO A SEGUIR UTILIZANDO UN MERIDIANO</b></p>
<p>Imaginemos que nuestra derrota a seguir debe llevarnos del punto A al punto B (ver figura abajo):</p>
<p><img class="img-fluid" src="<?php echo $link; ?>img/reglademar1.jpg" align="left"> - En primer lugar acomodamos la Regla de Mar de manera que uno de sus bordes (superior o inferior) pase por A y por B. (Tener en cuenta que la proa del barquito del centro de la regla debe estar con una orientación concordante con el sentido de nuestra derrota, o sea "de A hacia B".)</p>
<p>- Manteniendo el borde de la regla sobre los puntos A y B, vamos corriendo la regla hasta que el centro más al sur (centro inferior) toque un meridiano. Luego observamos sobre cual valor del semicírculo superior corta el meridiano, exactamente ese es el valor angular de la derrota a seguir. (En nuestro ejemplo 104°).</p>
<p>- Obsérvese que sobre el semicírculo de la Regla de Mar utilizado, existen valores exteriores que nos quedan horizontales que en este ejercicio planteado son los que utilizamos. La escala de valores interiores (que nos quedaron verticales), es para utilizar sobre un paralelo.</p>
<p class="bluetext" align="center"><b>PROCEDIMIENTO A SEGUIR UTILIZANDO UN PARALELO</b></p>
<p><img class="img-fluid" src="<?php echo $link; ?>img/reglademar2.jpg" align="right"> Hagamos de cuenta ahora que pretendemos navegar entre punto C y el punto D (ver figura abajo) <br>- Comenzamos orientando la Regla de Mar de manera que uno de sus bordes (superior o inferior), tenga una orientación tal que pase por C y D. (También tenemos que tener en cuenta que la proa del barquito central de la regla esté orientada "de C a D".) <br>- Con el borde de la regla pasando por C y por D, vamos haciendo correr la regla hasta que el centro más al sur (centro inferior) toque un paralelo. Luego observamos sobre que valor angular del semicírculo superior corta el paralelo, exactamente ese es el valor de la derrota a seguir (En nuestro ejemplo 42°) <br>- Nótese que usamos los valores interiores del semicírculo que se ven horizontales ahora. <br> <br> <br> <strong>Conseguí la novedosa regla de mar,</strong> mandando un mail a <span id="cloak414989043df9fc0b0bdc28e66df3a5e0"><a href="mailto:info@navemocion.com">info@navemocion.com</a></span><script type="text/javascript">
				document.getElementById('cloak414989043df9fc0b0bdc28e66df3a5e0').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addy414989043df9fc0b0bdc28e66df3a5e0 = '&#105;nf&#111;' + '&#64;';
				addy414989043df9fc0b0bdc28e66df3a5e0 = addy414989043df9fc0b0bdc28e66df3a5e0 + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
				var addy_text414989043df9fc0b0bdc28e66df3a5e0 = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';document.getElementById('cloak414989043df9fc0b0bdc28e66df3a5e0').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy414989043df9fc0b0bdc28e66df3a5e0 + '\'>'+addy_text414989043df9fc0b0bdc28e66df3a5e0+'<\/a>';
		</script> o llamando al 4701-4410</p>
</div>
</div>
</div>
</div>	</div>
	    </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
