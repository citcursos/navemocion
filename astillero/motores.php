<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Astillero - Motores</h1>
        </div>
      </div>
        <div class="container grey">
        <div itemprop="articleBody">
		<p style="text-align: center;"><span style="font-size: 14pt;"><strong>MOTORES</strong></span></p>
<p>&nbsp;</p>
<table border="0" cellspacing="10" cellpadding="2" align="center">
<tbody>
<tr>
<td style="text-align: center;"><img class="img-fluid" src="<?php echo $link; ?>img/motor_1.jpg"></td>
<td style="text-align: center;"><img class="img-fluid" src="<?php echo $link; ?>img/motor_2.jpg"></td>
</tr>
<tr>
<td class="commontext" colspan="2">Antiguamente la navegación era exclusivamente a vela, tiempos en que había que esperar cinco días fondeados a que llegaran vientos favorables, ejercitando la paciencia….., o bien recurrir a los remos y ejercitar casi todos los músculos del cuerpo.</td>
</tr>
<tr>
<td style="text-align: center;"><img class="img-fluid" src="<?php echo $link; ?>img/motor_3.jpg"></td>
<td style="text-align: center;"><img class="img-fluid" src="<?php echo $link; ?>img/motor_4.jpg"></td>
</tr>
<tr>
<td class="commontext" colspan="2">Hoy por hoy las cosas cambiaron: el tiempo que antes se empleaba esperando el viento ahora lo pasamos trabajando en una oficina para poder comprar combustible, y dar una vueltita el fin de semana…. <br>Navegando a vela, es sencillo darse cuenta “donde está el problema”, una vela que se rifó o un cabo que está desgastado por ejemplo. En cambio, los motores son complejos organismos –que hasta parecen latir-, en los que no es fácil meter mano. Por eso cuando el mecánico está inspeccionando nuestro motor para darnos un diagnóstico, el nerviosismo se va apoderando de nosotros y quisiéramos preguntarle: “¿Qué tiene doctor, es grave? ¡¿Es grave?!......”</td>
</tr>
<tr>
<td class="commontext" colspan="2">Los navegantes de Navemoción tenemos un doctor de motores, que atenderá no sólo nuestras dudas, sino también intervenciones y resucitaciones…..</td>
</tr>
</tbody>
</table>
<p class="commontext" align="center">&nbsp;</p>
<p class="commontext" align="center"><b>Mecánico José Luis Castro <br>Consultas al <span class="bluetext">4226-6535</span> o al <span class="bluetext">1550383407</span></b></p>	</div>
	    </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
