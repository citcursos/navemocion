<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Astillero - Cintas y Tapetes</h1>
        </div>
      </div>
        <div class="container grey">
        <div itemprop="articleBody">
		<div class="commontext"><img style="display: block; margin-left: auto; margin-right: auto;" class="img-fluid" src="<?php echo $link; ?>img/Logo 3soluciones2.jpg" align="middle" vspace="5"><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img style="display: block; margin-left: auto; margin-right: auto;" class="img-fluid" src="<?php echo $link; ?>img/logo2_dbm.gif">
<p style="text-align: center;"><b>Santa Fe 2707 Martínez (1640) Tel./Fax: 4792-7897</b>&nbsp;&nbsp;<br> MAIL: <span id="cloak144f105949911fd7872f2cafdba69b11"><a href="mailto:info@3soluciones.com">info@3soluciones.com</a></span><script type="text/javascript">
				document.getElementById('cloak144f105949911fd7872f2cafdba69b11').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addy144f105949911fd7872f2cafdba69b11 = '&#105;nf&#111;' + '&#64;';
				addy144f105949911fd7872f2cafdba69b11 = addy144f105949911fd7872f2cafdba69b11 + '3s&#111;l&#117;c&#105;&#111;n&#101;s' + '&#46;' + 'c&#111;m';
				var addy_text144f105949911fd7872f2cafdba69b11 = '&#105;nf&#111;' + '&#64;' + '3s&#111;l&#117;c&#105;&#111;n&#101;s' + '&#46;' + 'c&#111;m';document.getElementById('cloak144f105949911fd7872f2cafdba69b11').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy144f105949911fd7872f2cafdba69b11 + '\'>'+addy_text144f105949911fd7872f2cafdba69b11+'<\/a>';
		</script> ó <span id="cloake89f0ab43599361213af97b619c9ec8d"><a href="mailto:ventas@3soluciones.com">ventas@3soluciones.com</a></span><script type="text/javascript">
				document.getElementById('cloake89f0ab43599361213af97b619c9ec8d').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addye89f0ab43599361213af97b619c9ec8d = 'v&#101;nt&#97;s' + '&#64;';
				addye89f0ab43599361213af97b619c9ec8d = addye89f0ab43599361213af97b619c9ec8d + '3s&#111;l&#117;c&#105;&#111;n&#101;s' + '&#46;' + 'c&#111;m';
				var addy_texte89f0ab43599361213af97b619c9ec8d = 'v&#101;nt&#97;s' + '&#64;' + '3s&#111;l&#117;c&#105;&#111;n&#101;s' + '&#46;' + 'c&#111;m';document.getElementById('cloake89f0ab43599361213af97b619c9ec8d').innerHTML += '<a ' + path + '\'' + prefix + ':' + addye89f0ab43599361213af97b619c9ec8d + '\'>'+addy_texte89f0ab43599361213af97b619c9ec8d+'<\/a>';
		</script><br>WEB: <a href="http://www.3soluciones.com" target="_blank" rel="noopener noreferrer">http://www.3soluciones.com</a> </p>
<p style="text-align: center;"><img class="img-fluid" src="<?php echo $link; ?>img/3mnomad.gif"> <br><img class="img-fluid" src="<?php echo $link; ?>img/image_cinta.jpg" width="290"><img class="img-fluid" src="<?php echo $link; ?>img/image2_cinta.jpg" width="290"></p>
<p style="text-align: center;"><b>Tapetes antideslizantes personalizados:</b> <br>Cushion Plus con base: Ideal para cubiertas de barcos, su estructura de anillos de vinilo atrapa y esconde la suciedad, es resistente al moho y colonias de bacterias, de fácil mantenimiento es altamente resistente a todo tipo de químicos y grasas, pensado para entradas de edificios y locales comerciales es resistente al paso de 150.000 personas por año. <br>Usted solo debe darnos el nombre y logo de su barco especificando los colores identificatorios, y en un breve lapso recibirá un tapete calidad 3M Nomad con su imagen impresa por profesionales, permitiéndole así, una cubierta limpia, atractiva y personalizada. <br>Cushion Plus sin base: de iguales caracteristicas al anterior es ideal para cockpit, su trama abierta permite que el agua embarcada llegue con facilidad a los imbornales.</p>
<p style="text-align: center;"><img class="img-fluid" src="<?php echo $link; ?>img/logo_safety.gif"> <br><img class="img-fluid" src="<?php echo $link; ?>img/image3_cinta.jpg"></p>
<p style="text-align: center;">Cintas antideslizantes para cubiertas de barcos, escaleras, bañeras y rampas Según la necesidad; <u>Conformable:</u> con base de aluminio para áreas de trafico pesado, <u>Uso General Negro/Transparente:</u> escaleras de trafico pesado/medio. <u>Resilent:</u> para bañeras y duchadores de base plástica y textura no abrasiva y <u>Médium:</u> para el área náutica, confeccionadas en goma autoadhesiva con textura no abrasiva.</p>
</div>	</div>
	    </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
