<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Astillero - Plástico</h1>
        </div>
      </div>
        <div class="container grey">
        <div itemprop="articleBody">
		<p style="text-align: center;"><span style="font-size: 14pt;"><strong>Plástico</strong></span></p>
<p style="text-align: center;">&nbsp;</p>
<table border="0" cellspacing="10" align="center">
<tbody>
<tr>
<td>
<div align="center"><img class="img-fluid" src="<?php echo $link; ?>img/melquisedeclogo.jpg" width="229" height="160"></div>
</td>
</tr>
<tr>
<td><img class="img-fluid" src="<?php echo $link; ?>img/melquisedecbolantes.jpg" width="383" height="470" vspace="15"></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<table style="width: 934px;" border="0" cellspacing="10" align="center">
<tbody>
<tr style="height: 258.781px;">
<td style="width: 420px; height: 258.781px;" align="left"><img class="img-fluid" src="<?php echo $link; ?>img/plastico_1.jpg"></td>
<td class="commontext" style="width: 478px; height: 258.781px;" valign="top">
<ul>
<li>TRABAJOS DE PINTURA EN BARCOS DE FIBRA DE VIDRIO: FONDO, BANDAS, CUBIERTA, INTERIORES.</li>
<li>REPARACIONES EN PLÁSTICO.</li>
<li>SELLADO DE QUILLOTES.</li>
<li>CHEQUEO DE EMBARCACIONES EN VENTA SIN GARGO.</li>
</ul>
</td>
</tr>
<tr style="height: 202px;">
<td style="width: 420px; height: 202px;"><img class="img-fluid" src="<?php echo $link; ?>img/plastico_2.jpg"></td>
<td style="width: 478px; height: 202px;"><img class="img-fluid" src="<?php echo $link; ?>img/plastico_3.jpg"></td>
</tr>
<tr style="height: 169px;">
<td style="width: 420px; height: 169px;"><img class="img-fluid" src="<?php echo $link; ?>img/plastico_4.jpg"></td>
<td style="width: 478px; height: 169px;"><img class="img-fluid" src="<?php echo $link; ?>img/plastico_5.jpg"></td>
</tr>
</tbody>
</table>
<p style="text-align: center;">&nbsp;</p>	</div>
	    </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
