<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Astillero - Heladeras</h1>
        </div>
      </div>
        <div class="container grey">
			
        <div itemprop="articleBody">
		<div class="commontext">
<div class="commontext">
<div class="commontext">
<div class="commontext">
<div class="commontext">
<p class="bluetext" align="center"><span style="font-size: 14pt;"><b>HELADERAS PARA EMBARCACIONES</b></span></p>
<p class="commontext" align="center"><b>Distintos sistemas</b></p>
<p>Desde el hielo hasta los equipos más sofisticados, todos tienen ventajas y desventajas.</p>
<p>Hielo: Uno de los recursos más antiguos, pero interesantes aún en nuestros tiempos. Con inconvenientes conocidos como la reposición, el derrame de agua, el gran volumen que ocupa y el pobre nivel de frío que se logra. Existe una manera para mejorar esta idea, que es congelar botellas plásticas no totalmente llenas, en un freezer doméstico por varios días, de esa manera obtendremos diez veces más frío a igualdad de peso.</p>
<img style="margin-right: 10px; margin-left: 10px;" class="img-fluid" src="<?php echo $link; ?>img/heladera2.jpg" width="276" height="332" align="left">
<p>Equipos eléctricos: Mucho se ha hecho, pero los equipos de 12 v. blindados son de una tecnología realmente asombrosa. Por depender de energía eléctrica acumulada (baterías) terminan agotando las mismas, con los trastornos que ocasiona la reposición de la carga, obligándonos por ejemplo, a tener el motor en marcha durante muchas horas. Si considerásemos la potencia de frío que desarrollan, nos daríamos cuenta que estos equipos sirven para pequeños refrigeradores, en los que además tendríamos que colocar líquidos y alimentos ya refrigerados, para de esta manera ayudar al equipo en la etapa inicial de enfriado.</p>
<p>Heladeras comunes de uso doméstico: Una moderna y aparente solución, porque en realidad sólo sirve para decir: “tengo heladera”. La complejidad en llevar a la práctica otros recursos más eficaces, hizo que se considerase a esta idea como una solución fácil. La necesidad de costosos convertidores y grandes bancos de baterías, para obtener un modesto resultado, convierten a esta posibilidad en “relativamente” buena.</p>
<p>Equipos electrónicos: Son los que no tienen tuberías ni compresor. Con un sistema electrónico y el aporte de tensión, se genera un movimiento eléctrico que origina que en un lugar se absorba calor y en otro se lo disipe. Su inconveniente es que sólo produce una diferencia de temperatura con el exterior de la heladera, que en el mejor de los casos será de 14° C.</p>
<p>Equipos a absorción: Son los que no tienen ninguna actividad mecánica, con el aporte de calor se genera el movimiento de los fluidos que están dentro de las tuberías, produciendo el efecto frigorífico. Sus inconvenientes son que necesita estar bien nivelado y ventilado, además de tardar mucho en entrar en régimen.</p>
<p>Equipos mecánicos comunes: Un interesante recurso realizado por aficionados y empresas de reconocida trayectoria. Estos equipos toman energía mecánica del motor propulsor con un compresor del tipo usado en aire acondicionado automotor, con embrague magnético, evacuando el calor del sistema en el agua que toma el mismo motor. El frío que se produce es acumulado en tanques con líquidos anticongelantes y agua, y de esa manera tener una reserva de energía frigorífica cuando no funciona la máquina. Este sistema es conveniente cuando además se cuenta con un segundo circuito para equipo eléctrico, ya que de esta manera nos brindará un completo servicio, que lo hace muy recomendable.</p>
<p>Equipos mecánicos de alto rendimiento: Estos equipos también toman energía mecánica de igual modo que los equipos mecánicos comunes. Un detalle importante, es que siempre se construyen con un segundo circuito para equipo eléctrico aunque este no se coloque. Una verdadera maravilla termodinámica. Su sofisticada construcción hace que no se parezca a nada, y su alta performance hace que sea ideal. Es el sistema que menos pesa, se podría compara su peso con el de una carga de hielo, frío que se obtiene con quince minutos de motor. En su construcción se contemplan conceptos de termodinámica, con un montaje mecánico que hace difícil que se produzcan fallas. Produce hielo rápidamente y es el sistema que menos elementos tiene a simple vista.</p>
<p>Gabinete o caja térmica: Una parte muy importante que comúnmente no se trata con el debido cuidado, porque aún no siendo equipada con ningún equipo, ésta debe ser muy bien hecha y convenientemente ubicada. El mejor lugar es lo más abajo posible, y la aislación de no menos que tres pulgadas.</p>
<p>&nbsp;</p>
<p class="bluetext" align="center">REALIZÓ ESTA NOTA PARA “NAVEMOCION” Sebastián Malcoff especialista en refrigueración para embarcaciones. POR CONSULTAS O INSTALACIÓN DE HELADERAS EN EMBARCACIONES, COMUNICARSE AL <b>15........</b> O <b>474kkkkkk</b></p>
</div>
</div>
</div>
</div>
</div>	</div>
	      </div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
