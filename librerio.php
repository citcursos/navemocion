<!doctype html>
<html lang="es">

<head>
  <?php
  include 'common/head.php';
  ?>
</head>

<body>
  <header style="height: 100px;">
    <?php
    include 'common/nav.php';
    ?>
  </header>
  <main role="main">
    <div class="container img-header">
      <div class="col-md-6 px-0">
        <h1 class="display-4">Librerío</h1>
      </div>
    </div>

    <div class="container">
    <p>&nbsp;</p><p>&nbsp;</p>
      <div class="row">
        <div class="col-md-12 text-center">
              <a href="<?php echo $link; ?>librerio/libros-de-nautica.php"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/libros_nautica.jpg" style=""></a>

              <a href="<?php echo $link; ?>librerio/navegando-con-dios.php"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/dios.jpg" style=""></a>

              <a href="<?php echo $link; ?>librerio/cuentos-de-mar.php"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/cuentos.jpg" style=""></a>

              <a href="<?php echo $link; ?>librerio/versos-marineros.php"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/versos.jpg" style=""></a>

              <a href="<?php echo $link; ?>librerio/navegando-con-conrad.php"><img alt="" class="img-fluid" src="<?php echo $link; ?>img/conrad.jpg" style=""></a>

         

        </div>


      </div>
      <p>&nbsp;</p><p>&nbsp;</p>
    </div>
    <!-- /.container -->
    <!-- FOOTER -->
    <?php
    include 'common/footer.php';
    ?>
  </main>
</body>

</html>