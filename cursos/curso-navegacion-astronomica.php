<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Cursos - Navegación Astronómica (Piloto de Yate)</h1>
        </div>
      </div>
      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 text-center">
							<img alt="" class="img-fluid" style="width: 175px;"src="<?php echo $link; ?>img/patron1.jpg"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Curso:</h4>
							<h4>Navegación Astronómica (Piloto de Yate)</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<p>FECHA DE INCIO:</p>
					<div class="commontext"><b><u>Marzo y Agosto</u></b>
					<p><b>Duración del curso:</b>cuatro meses.</p>
					<p><b>Informes e inscripción:</b>&nbsp;4701-4410&nbsp;<br>
					<span id="cloak3b194af7fd44bbdf15de923ca6120e4d"><a href="mailto:info@navemocion.com">info@navemocion.com</a></span><script type="text/javascript">
						document.getElementById('cloak3b194af7fd44bbdf15de923ca6120e4d').innerHTML = '';
						var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
						var path = 'hr' + 'ef' + '=';
						var addy3b194af7fd44bbdf15de923ca6120e4d = '&#105;nf&#111;' + '&#64;';
						addy3b194af7fd44bbdf15de923ca6120e4d = addy3b194af7fd44bbdf15de923ca6120e4d + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
						var addy_text3b194af7fd44bbdf15de923ca6120e4d = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';document.getElementById('cloak3b194af7fd44bbdf15de923ca6120e4d').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy3b194af7fd44bbdf15de923ca6120e4d + '\'>'+addy_text3b194af7fd44bbdf15de923ca6120e4d+'<\/a>';
				</script><br>
					Para cualquier tipo de consulta (por ejemplo a cerca de otros días u horarios de clase, también comunicarse por las mismas vías).</p>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10 bg-info-2 p-3">
					<div class="row m-2">
						<div class="col-md-12 align-self-center">
							<h5><strong>Profesor de los cursos:</strong></h5>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 text-center">
							<img alt="" class="img-fluid rounded"  src="<?php echo $link; ?>img/cartarecom.jpg" style="">
						</div>
						<div class="col-md-6">
							<p><strong>Gabriel Buduba</strong>, Piloto de Yate Vela/Motor Profesional, ha dictado cursos de navegación en la Fragata Sarmiento, en distintas facultades de la Universidad de Buenos Aires y en la Universidad Católica Argentina, responsable junto al Servicio de Hidrografía Naval en el proyecto Regla de Mar, Ex Secretario de Redacción y Asesor Legal de la revista de información a los navegantes Seaport Report, con más de veinte años en la docencia náutica. Capitán de diversos barcos escuela en las costas argentinas, uruguayas y brasileras. Director y creador del portal de náutica para navegantes:&nbsp;www.navemocion.com</p>
						</div>
					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
						<div class="bluetext">Se dictará dando comienzo en marzo y agosto. Está dirigido a timoneles, patrones o pilotos de yate. <strong>La primera clase es gratuita.</strong></div>
						<div class="bluetext">&nbsp;</div>
						<div class="commontext">
						<div class="commontext">Las clases están orientadas a brindar al alumno los conocimientos necesarios para ubicarse en altamar con la sola ayuda de un sextante, reloj y un juego de tablas. También están previstas prácticas a bordo.
						<p>Algunos probablemente han quedado estupefactos ante los términos recientemente mencionados y para ellos tenemos una descripción más profunda de lo que implica la navegación astronómica y el por qué de su gran importancia.</p>
						<p><b>Si quiere interiorizarse más sobre el tema de la navegación astronómica lea el extenso TEMARIO al final de la página.</b></p>

						<p><img class="rounded float-left mr-3" alt=""src="<?php echo $link; ?>img/sextante.jpg" vspace="10"></p>

						<p>&nbsp;</p>

						<div class="bluetext"><b>El curso dura cuatro meses. Las clases se dictarán una vez por semana de 19:00 a 21:30 horas.</b></div>

						<p>El temario básico a desarrollar en las clases teóricas es el siguiente:<br>
						&nbsp;</p>

						<p>&nbsp;</p>
						</div>
						</div>

						<ul>
							<li>Fundamentos de la navegación de altura y elementos necesarios.</li>
							<li>El sexante: descripción y manejo.</li>
							<li>El tiempo.</li>
							<li>Obtención y corrección de alturas con sexante.</li>
							<li>Uso de almanaque náutico.</li>
							<li>Identificación de estrellas y planetas.</li>
							<li>Líneas de posición astronómicas.</li>
							<li>Utilización de las tablas H.O. 249 y d star finder.</li>
							<li>Cálculo de la latitud: con meridiana y estrella polar.</li>
							<li>Obtención de la posición astronómica.</li>
							<li>Errores sistemáticos y accidentales en las rectas de altura.
							<p><b>Se repartirá el material de estudio y de cálculo en forma gratuita a todos los alumnos.</b>&nbsp;<br>
							También habrá una clase de apoyo en el Planetario de la Ciudad de Buenos Aires (a confirmar).</p>

							<p><br>
							<br>
							<b>"Yo no puedo imaginar a alguien mirando al cielo y negando a Dios"</b></p>
							<i>Abraham Lincoln</i>&nbsp;</li>
						</ul>
						<ul>
							<li>
							<h5>TEMARIO</h5>

							<p><i>En inglés (links):</i></p>

							<p><a href="http://www.ruf.rice.edu/~feegi/astro.html" rel="noopener noreferrer" target="_blank"><b>Navegación astronómica antes del año 1400</b></a>&nbsp;<br>
							<br>
							<i>En castellano:</i></p>

							<p>&nbsp;</p>

							<div><strong>&gt; Para qué sirve la navegación astronómica?</strong></div>

							<p>El G.P.S. es maravilloso. Tiene cobertura mundial, razonable precio y mejora la seguridad de la navegación. Sin embargo, alguna forma de control es necesaria. Ejemplos de fallas electrónicas, problemas eléctricos, rayos e inundaciones, son cosas que pueden ocurrir. Además el sistema G.P.S. no tiene garantía de estar siempre en operación, y hoy por hoy los satélites están controlados por un solo país. El mejor sistema de control es la navegación astronómica, ya que reúne las siguientes características:</p>
							</li>
							<li><i>No requiere del uso de electricidad.</i></li>
							<li><i>Su fuente de datos (los astros) es independiente de la del G.P.S.</i></li>
							<li><i>Con la dupla G.P.S - sextante cada instrumento está verificando el correcto funcionamiento del otro.</i></li>
							<li><i>Puede emplearse en altamar donde ya no hay más referencias que los astros. (además un sextante puede ser utilizado en navegación costera, no ya para tomar alturas de astros sino para tomar ángulos verticales y horizontales a puntos notables de la costa, obteniéndose posiciones de mucha precisión).</i>
							<p>Nunca se olvide que atrás de un G.P.S. debe haber un navegante bien preparado. En ese caso será un complemento de las habilidades marineras -pero la gorra de capitán la seguirá llevando usted-. Por algo el sextante es un instrumento obligatorio a bordo de las embarcaciones deportivas que hacen navegación oceánica, y el uso del mismo es una de las habilidades que deben conocer patrones (en navegación costera) y pilotos (en navegación oceánica).</p>

							<p>&nbsp;</p>

							<p>&nbsp;</p>

							<p><span class="bluetext"><b>&gt; Debemos mantener nuestras habilidades de marinos!</b></span>&nbsp;</p>

							<p><br>
							Algunos dicen que el G.P.S. llevará a nuestro barco a través del océano por nosotros -pero si precisamente eso es lo que queremos y sabemos hacer!-. Sin el establecimiento de una disciplina, una de las principales habilidades del marino estará puesta en riesgo. La clave para mantener estas habilidades reside en ir llevando una estima, que se irá confirmando a través de las posiciones verdaderas obtenidas a través de sextante o G.P.S. (a vista de costa también usaremos un compás de marcaciones, radar, una isobata o una enfilación) Quién lleva una estima aprende a conocer deriva y abatimiento (o errores en el compás), todo lo cual optimizará nuestras navegaciones (de crucero, de regata, o en caso de emergencia).Buena disciplina a bordo será apagar el G.P.S. por varias horas (excepto para efectuar algún necesario chequeo), y navegar con el sol, la luna, las estrellas y los planetas. Esto, además de ser divertido, afila nuestra habilidad como marinos, y la mantiene siempre preparada.</p>

							<p><span class="bluetext"><b>&gt; Conservemos las tradiciones marineras!</b></span>&nbsp;</p>

							<p><br>
							Las tradiciones no son para todos. Hay gente pragmática que rechaza las actividades que no son de utilidad. Sin embargo hay muchos marinos profesionales que conservan lo tradicional tanto como pueden aceptar lo más moderno. El sextante (y sus predecesores) está en el centro de la historia y tradición marineras. Quien no se maravilla al contemplar un sextante de bronce y ébano del siglo XVIII que sirvió para bajar alguna estrella desde la cubierta de uno de esos maravillosos veleros clásicos, quién no se maravilla al entender que aquellos marinos podían navegar cualquier océano sin ninguna ayuda externa.</p>

							<p><span class="bluetext"><b>&gt; Hagamos cosas divertidas!</b></span>&nbsp;</p>

							<p><br>
							Es divertido hacer cosas que son a la vez fáciles y difíciles. Fácil para empezar, como empezar a jugar ajedrez o pegarle a una pelota de golf; pero nos encontramos con que son actividades en las que la maestría no se alcanza fácilmente. Es muy sencillo obtener la longitud simplemente observando la hora exacta de la salida o la puesta del sol, o tomar un rumbo a través de la posición de una estrella. También es relativamente fácil obtener nuestra latitud tomando una altura al sol en mediodía. Pero las cosas ya se complican cuando se trata de identificar las estrellas útiles para el navegante, o cuando se trata de reconocer un planeta, o tener en cuenta la paralaje de la luna. Muchos experimentados navegantes de altura pueden tomar una altura a una estrella a través de un agujero entre las nubes, tomar un altura a un planeta en pleno día, predecir la salida del sol en donde se encuentra el barco, u obtener una tabla de desvíos para el compás de a bordo considerando el acimut de los astros. Cuando el cielo nos es familiar, es como si tuviésemos una gigantesca carta náutica por encima de nuestra cabeza. Una estrella puede estar sobre Punta del Este, otra sobre Ushuaia, y la luna sobre Río de Janeiro. Mostrándonos a nosotros sus distancias relativas y rumbos a seguir. Todo esto nos conecta nuevamente con un mar con muchas señales, y hace que no sigamos perdiendo nuestras capacidad natural de percepción (que el mundo moderno nos va debilitando). Mucha gente se divierte y la pasa muy bien usando un sextante (es una de las pocas actividades marineras que pueden practicarse incluso sin tener un barco!!).&nbsp;</p>
							</li>
						</ul>
				</div>
				<div class="col-md-1"></div>
			</div>

		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
