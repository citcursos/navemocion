
<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Cursos - Timonel de yate a vela y/o motor</h1>
        </div>
      </div>
      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 ">
							<img alt="" src="<?php echo $link; ?>img/cursos_yate.jpg" style="margin-left: auto; margin-right: auto; display: block;"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Curso:</h4>
							<h4>Timonel de yate a vela y/o motor</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<p><span style="font-family: Tahoma, Helvetica, Arial, sans-serif; font-size: 14pt;">&nbsp;FECHA DE INCIO:</span></p>
					<p><span style="font-family: Tahoma, Helvetica, Arial, sans-serif;"><span style="font-size: 12.16px;"></span></span></p>
					<div class="commontext"><b><u>Agosto&nbsp;</u></b>
						<p><b>Duración del curso: </b>5 meses&nbsp; &nbsp; &nbsp; &nbsp;i
							<strong>nfo: whatsaap 01154611418&nbsp; 01136892045&nbsp; 
								<span id="cloak84ef84bb1883bb44d19bae8995ed2d4e">
									<a href="mailto:timonelagro@gmail.com">timonelagro@gmail.com</a>
								</span>
								<script type="text/javascript">
									document.getElementById('cloak84ef84bb1883bb44d19bae8995ed2d4e').innerHTML = '';
									var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
									var path = 'hr' + 'ef' + '=';
									var addy84ef84bb1883bb44d19bae8995ed2d4e = 't&#105;m&#111;n&#101;l&#97;gr&#111;' + '&#64;';
									addy84ef84bb1883bb44d19bae8995ed2d4e = addy84ef84bb1883bb44d19bae8995ed2d4e + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
									var addy_text84ef84bb1883bb44d19bae8995ed2d4e = 't&#105;m&#111;n&#101;l&#97;gr&#111;' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloak84ef84bb1883bb44d19bae8995ed2d4e').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy84ef84bb1883bb44d19bae8995ed2d4e + '\'>'+addy_text84ef84bb1883bb44d19bae8995ed2d4e+'<\/a>';
								</script>
							</strong>
						</p>

						<p><strong>Costo</strong>: 5 cuotas de $3.300, con beneficios exclusivos a los egresados y estudiantes de las Facultades de Odontología, Agronomía y Medicina, teoricos totalmente libres. Abierto al público, descuento grupos de amigos, familiares. Incluye clases terióricas, prácticas, material de estudio y examén ante Prefectura.&nbsp;</p>

						<p>El material de estudio se repartirá en forma gratuita a todos los alumnos.</p>

						<p style="padding-left: 30px;">Podrán asistir todos los interesados, no hace falta tener embarcación ni conocimientos previos.</p>

						<div class="commontext" style="padding-left: 30px;"><b>Clases teóricas</b>&nbsp;a desarrollarse en miércoles en Agronomía y los días jueves en Barrio Norte en el horario&nbsp;&nbsp;19:00 a 21: 30 horas, existe la posibilidad de cursar el teórico en Nuñez, especial para los que no puedan los dos días anteriores. El horario y día depende de los interesados y el cupo), consultar. Trataremos que las clases teóricas que te pierdas enviarte el material para que lo puedas tener y las consultas que te surgan&nbsp;estaremos disponibles para que evacues tus dudas.&nbsp;</div>

						<div class="commontext" style="padding-left: 30px;">El programa teórico comprende el estudio del Vocabulario Marinero, Meteorología, Seguridad, Mareas, Cartografía, Navegación, Reglamentación Internacional y Local.</div>

						<div class="commontext" style="padding-left: 30px;">&nbsp;</div>

						<div class="commontext" style="padding-left: 30px;"><b>Clases prácticas</b>&nbsp;a desarrollarse en distintos días y horarios (los grupos se armarán de acuerdo a las preferencias de los alumnos) desde la Ciudad Autónoma de Buenos Aires.</div>

						<div class="commontext" style="padding-left: 30px;">Desde la primer salida tendrás disponible tu salvavidas para la clase y comenzaras a llevar el timón para que puedas conocer como funciona, también colocaremos las velas y haremos los nudos correspondientes. Para ello será necesario utilizar todo un vocabulario nuevo que te va a permitir familiarizarte con la actividad.&nbsp;</div>

						<div class="commontext" style="padding-left: 30px;">&nbsp;</div>

						<div class="commontext" style="padding-left: 30px;">COMO TAREA te dejo un par de palabras para que vayas investigando de que se tratan y comiences lentamente a vivir este fascinante mundo que comenzo hace miles de años con los primeros navegantes que cruzaban los mares, y nosotros revivimos atraves de la recreación:</div>

						<div class="commontext" style="padding-left: 30px;">Te animas?</div>

						<div class="commontext" style="padding-left: 30px;">- Orzar</div>

						<div class="commontext" style="padding-left: 30px;">- Derivar</div>

						<div class="commontext" style="padding-left: 30px;">- Tambucho</div>

						<div class="commontext" style="padding-left: 30px;">- Guindaleza</div>

						<div class="commontext" style="padding-left: 30px;">- Sotavento</div>

						<div class="commontext" style="padding-left: 30px;">- Barlovento:</div>

						<div class="commontext" style="padding-left: 30px;">&nbsp;</div>

						<div class="commontext" style="padding-left: 30px;">si te animas, cuando vengas al curso charlamos de lo que averiguaste, dale?</div>

						<div class="commontext" style="padding-left: 30px;"><b><img alt="" src="/images/atardecer.jpg"></b></div>

						<div class="commontext" style="padding-left: 30px;">&nbsp;</div>

						<div class="commontext" style="padding-left: 30px;">Las clases prácticas se desarrollan en veleros modelo Tritón 28 y H 26 (son veleros de 10 a 8 metros), y la parte de motor se realiza en semirrígidos y lanchas. Se hacen nudos marineros, maniobras y manejo de cartas náuticas e instrumental. En la parte final del curso tenemos la posibilidad de organizar viajes de instrucción a la costa uruguaya, argentina y brasilera.</div>

						<div class="commontext" style="padding-left: 30px;">&nbsp;</div>

						<p><b>Informes e inscripción:</b>&nbsp;whatsaap al 011 54611418, 01136892045 ó&nbsp;<span id="cloakeb814898ce83c8fa6b4cf9b1646a7b9b"><a href="mailto:info@navemocion.com">info@navemocion.com</a></span><script type="text/javascript">
							document.getElementById('cloakeb814898ce83c8fa6b4cf9b1646a7b9b').innerHTML = '';
							var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
							var path = 'hr' + 'ef' + '=';
							var addyeb814898ce83c8fa6b4cf9b1646a7b9b = '&#105;nf&#111;' + '&#64;';
							addyeb814898ce83c8fa6b4cf9b1646a7b9b = addyeb814898ce83c8fa6b4cf9b1646a7b9b + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
							var addy_texteb814898ce83c8fa6b4cf9b1646a7b9b = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';document.getElementById('cloakeb814898ce83c8fa6b4cf9b1646a7b9b').innerHTML += '<a ' + path + '\'' + prefix + ':' + addyeb814898ce83c8fa6b4cf9b1646a7b9b + '\'>'+addy_texteb814898ce83c8fa6b4cf9b1646a7b9b+'<\/a>';
						</script>&nbsp;o <span id="cloakaa07ad0b1b11af0a25bf0836d1db516f"><a href="mailto:timonelagro@gmail.com">timonelagro@gmail.com</a></span><script type="text/javascript">
								document.getElementById('cloakaa07ad0b1b11af0a25bf0836d1db516f').innerHTML = '';
								var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
								var path = 'hr' + 'ef' + '=';
								var addyaa07ad0b1b11af0a25bf0836d1db516f = 't&#105;m&#111;n&#101;l&#97;gr&#111;' + '&#64;';
								addyaa07ad0b1b11af0a25bf0836d1db516f = addyaa07ad0b1b11af0a25bf0836d1db516f + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
								var addy_textaa07ad0b1b11af0a25bf0836d1db516f = 't&#105;m&#111;n&#101;l&#97;gr&#111;' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloakaa07ad0b1b11af0a25bf0836d1db516f').innerHTML += '<a ' + path + '\'' + prefix + ':' + addyaa07ad0b1b11af0a25bf0836d1db516f + '\'>'+addy_textaa07ad0b1b11af0a25bf0836d1db516f+'<\/a>';
						</script>
					</p>
					<p>
						<br>
						Para cualquier tipo de consulta (por ejemplo a cerca de otros días u horarios de clase, también comunicarse por las mismas vías).<b><img alt="" src="/images/12291724_10153320343947055_8944005205172545272_o.jpg" style=""></b>
					</p>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10 bg-info-2 p-3">
					<div class="row m-2">
						<div class="col-md-12 align-self-center">
							<h5><strong>Profesor de los cursos:</strong></h5>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<img alt="" class="img-fluid rounded float-left mr-3" src="<?php echo $link; ?>img/cartarecom.jpg" style="">
							<p><strong>Gabriel Buduba</strong>, Piloto de Yate Vela/Motor Profesional, ha dictado cursos de navegación desde el año 1991 (con un Polaris de 5 metros de eslora, sin cabina y con solo 2 o 3 alumnos decidio hacer lo que le gustaba, para ello se preparo, busco todas las herramientas posibles para dar lo mejor de si) hasta la fecha, con mas de 2000 exalumnos que ya disfrutan de este maravilloso mundo.</p>
							<p>El curso se a dictado&nbsp;en la Fragata Sarmiento, en distintas facultades de la Universidad de Buenos Aires y en la Universidad Católica Argentina, responsable junto al Servicio de Hidrografía Naval en el proyecto Regla de Mar, Ex Secretario de Redacción y Asesor Legal de la revista de información a los navegantes Seaport Report, con más de veintiocho&nbsp;años en la docencia náutica. Capitán de diversos barcos escuela en las costas argentinas, como viajes Buenos Aires - Mar del Plata, Buenos Aires- La Paloma, Cabo Polonio, Piriapolis, Montevideo, Punta del Este&nbsp;y viajes a la costa brasileras, Florianapolis. Alquiler de barcos en Angra dos Reis, recorriendo Isla Grande y Parati. Alquiler y navegación en Grecia, saliendo de Atenas recorriendo Islas Saronicas, Peloponeso, Hidra, Spetses, Poros, etc. y también en Croacia, saliendo de Split recorriendo Isla Brac, Hvar, Solta, Korcula, etc. Director y creador del portal de náutica para navegantes:&nbsp;www.navemocion.com&nbsp;</p>
						</div>
					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<img alt="" class="img-fluid" src="<?php echo $link; ?>img/grupo 1.jpg" style="">
				</div>
				<div class="col-md-1"></div>
			</div>			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<img alt="" class="img-fluid" src="<?php echo $link; ?>img/barco2.jpg" style="">
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<p><b>Los cursos duran cinco&nbsp;meses</b>, y al fin de los mismos a quienes aprueben los exámenes teórico y práctico tomados por la Prefectura Naval Argentina, se les otorga la habilitación oficial de Timonel Yate Vela y/o Motor.</p>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
