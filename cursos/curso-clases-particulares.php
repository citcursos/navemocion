<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Cursos - Clases particulares / Traslado de embarcaciones</h1>
        </div>
      </div>
      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 text-center">
							<img alt="" class="img-fluid" style="width: 175px;"src="<?php echo $link; ?>img/instructor.jpg"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Curso:</h4>
							<h4>Clases particulares / Traslado de embarcaciones</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<p>Para aprender acerca de los temas que más interesan en vela o motor <strong>-incluso puede ser en su propia embarcación-</strong> en los días y horarios que más convengan</p>
					<p>El temario lo arma uno mismo: Conocimientos Básicos, Uso de G.P.S., Uso de Radar, Uso de Sextante, Trimado de Velas, Navegación Costera, Navegación Astronómica, Cartografía, Meteorología, Derroteros..................</p>
					<p>Para aprender navegando a distintos puertos de la costa argentina, uruguaya y brasilera.</p>
					<p>Clases dictadas por Pilotos de Yate de amplia experiencia.</p>
					<p><strong>También trasladamos embarcaciones</strong> (Mar del Plata, Punta del Este, Brasil....)</p>
					<p>Consultas: <span id="cloak83c3a9ab0c455fc09fd81a0985dcecac"><a href="mailto:info@navemocion.com">info@navemocion.com</a></span><script type="text/javascript">
						document.getElementById('cloak83c3a9ab0c455fc09fd81a0985dcecac').innerHTML = '';
						var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
						var path = 'hr' + 'ef' + '=';
						var addy83c3a9ab0c455fc09fd81a0985dcecac = '&#105;nf&#111;' + '&#64;';
						addy83c3a9ab0c455fc09fd81a0985dcecac = addy83c3a9ab0c455fc09fd81a0985dcecac + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
						var addy_text83c3a9ab0c455fc09fd81a0985dcecac = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';document.getElementById('cloak83c3a9ab0c455fc09fd81a0985dcecac').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy83c3a9ab0c455fc09fd81a0985dcecac + '\'>'+addy_text83c3a9ab0c455fc09fd81a0985dcecac+'<\/a>';
					</script>&nbsp;</p>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10 bg-info-2 p-3">
					<div class="row m-2">
						<div class="col-md-12 align-self-center">
							<h5><strong>Profesor de los cursos:</strong></h5>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 text-center">
							<img alt="" class="img-fluid rounded"  src="<?php echo $link; ?>img/cartarecom.jpg" style="">
						</div>
						<div class="col-md-6">
							<p><strong>Gabriel Buduba</strong>, Piloto de Yate Vela/Motor Profesional, ha dictado cursos de navegación en la Fragata Sarmiento, en distintas facultades de la Universidad de Buenos Aires y en la Universidad Católica Argentina, responsable junto al Servicio de Hidrografía Naval en el proyecto Regla de Mar, Ex Secretario de Redacción y Asesor Legal de la revista de información a los navegantes Seaport Report, con más de veinte años en la docencia náutica. Capitán de diversos barcos escuela en las costas argentinas, uruguayas y brasileras. Director y creador del portal de náutica para navegantes:&nbsp;www.navemocion.com</p>
						</div>
					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
