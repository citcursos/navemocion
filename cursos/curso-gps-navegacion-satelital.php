<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Cursos - G.P.S - Navegación Satelital</h1>
        </div>
      </div>
      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 text-center">
							<img alt="" class="img-fluid" style="width: 175px;"src="<?php echo $link; ?>img/gpssaling.fw.png"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Curso:</h4>
							<h4>G.P.S - Navegación Satelital</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<p>FECHA DE INCIO:</p>
					<b><u>Mayo y Octubre</u></b>
					<p><b>Informes e inscripción:</b>&nbsp;4701-4410&nbsp;<br>
					<span id="cloak3b194af7fd44bbdf15de923ca6120e4d"><a href="mailto:info@navemocion.com">info@navemocion.com</a></span><script type="text/javascript">
						document.getElementById('cloak3b194af7fd44bbdf15de923ca6120e4d').innerHTML = '';
						var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
						var path = 'hr' + 'ef' + '=';
						var addy3b194af7fd44bbdf15de923ca6120e4d = '&#105;nf&#111;' + '&#64;';
						addy3b194af7fd44bbdf15de923ca6120e4d = addy3b194af7fd44bbdf15de923ca6120e4d + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
						var addy_text3b194af7fd44bbdf15de923ca6120e4d = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';document.getElementById('cloak3b194af7fd44bbdf15de923ca6120e4d').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy3b194af7fd44bbdf15de923ca6120e4d + '\'>'+addy_text3b194af7fd44bbdf15de923ca6120e4d+'<\/a>';
				</script><br>
					Para cualquier tipo de consulta (por ejemplo a cerca de otros días u horarios de clase, también comunicarse por las mismas vías).</p>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10 bg-info-2 p-3">
					<div class="row m-2">
						<div class="col-md-12 align-self-center">
							<h5><strong>Profesor de los cursos:</strong></h5>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 text-center">
							<img alt="" class="img-fluid rounded"  src="<?php echo $link; ?>img/cartarecom.jpg" style="">
						</div>
						<div class="col-md-6">
							<p><strong>Gabriel Buduba</strong>, Piloto de Yate Vela/Motor Profesional, ha dictado cursos de navegación en la Fragata Sarmiento, en distintas facultades de la Universidad de Buenos Aires y en la Universidad Católica Argentina, responsable junto al Servicio de Hidrografía Naval en el proyecto Regla de Mar, Ex Secretario de Redacción y Asesor Legal de la revista de información a los navegantes Seaport Report, con más de veinte años en la docencia náutica. Capitán de diversos barcos escuela en las costas argentinas, uruguayas y brasileras. Director y creador del portal de náutica para navegantes:&nbsp;www.navemocion.com</p>
						</div>
					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<p>Serán tres clases que tendrán por finalidad abordar su estudio de manera breve y efectiva, y desde tres ángulos distintos:</p>
					<p>&nbsp;</p>
					<div class="bluetext"><b>CLASE 1</b></div>
					<p><br><b><i>G.P.S.: "Introducción y conceptos generales" "Conocimiento y optimización de todas las funciones que puede brindarnos cada equipo"</i></b></p>
					<p>&nbsp;</p>
					<div class="bluetext"><b>CLASE 2</b></div>
					<p><br><b><i>"Cartas Digitales Raster y Vectoriales, y características de los distintos modelos de G.P.S." "La utilización del G.P.S. en conjunción con la P.C."</i></b></p>
					<p>&nbsp;</p>
					<div class="bluetext"><b>CLASE 3</b></div>
					<div class="bluetext">&nbsp;</div>
					<div class="bluetext"><b><i>"El G.P.S. en acción"</i></b></div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
