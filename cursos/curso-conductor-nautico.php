
<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Cursos - Conductor Náutico</h1>
        </div>
      </div>

      

      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 ">
							<img alt="" class="img-fluid" src="<?php echo $link; ?>img/cursoconductor.gif" style="margin-left: auto; margin-right: auto; display: block;"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Curso:</h4>
							<h4>Conductor Náutico</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<p class="text-center"><strong>Podrán asistir todos los interesados, no hace falta tener embarcación ni conocimientos previos</strong></p>
					<p>FECHA DE INCIO:</p>
					<p><b><u>Marzo, Junio, Agosto y Noviembre</u></b></p>
					<p><b>Duración del curso:</b>&nbsp;un mes.</p>
					<p>El material de estudio se repartirá en forma gratuita a todos los alumnos.</p>
					<p><b>Informes e inscripción:</b>&nbsp;4701-4410 ó&nbsp;<span id="cloakb78963a65ee831597ff1f78a907f6ad5"><a href="mailto:info@navemocion.com">info@navemocion.com</a></span>
					<script type="text/javascript">
						document.getElementById('cloakb78963a65ee831597ff1f78a907f6ad5').innerHTML = '';
						var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
						var path = 'hr' + 'ef' + '=';
						var addyb78963a65ee831597ff1f78a907f6ad5 = '&#105;nf&#111;' + '&#64;';
						addyb78963a65ee831597ff1f78a907f6ad5 = addyb78963a65ee831597ff1f78a907f6ad5 + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
						var addy_textb78963a65ee831597ff1f78a907f6ad5 = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';document.getElementById('cloakb78963a65ee831597ff1f78a907f6ad5').innerHTML += '<a ' + path + '\'' + prefix + ':' + addyb78963a65ee831597ff1f78a907f6ad5 + '\'>'+addy_textb78963a65ee831597ff1f78a907f6ad5+'<\/a>';
					</script>
					<p>Para cualquier tipo de consulta (por ejemplo a cerca de otros días u horarios de clase, también comunicarse por las mismas vías).</p>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10 text-center">
					<img alt="" data-image-widget-flowlayout="1"  class="img-fluid bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" src="<?php echo $link; ?>img/presentacion3.jpg">
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10 bg-info-2 p-3">
					<div class="row m-2">
						<div class="col-md-12 align-self-center">
							<h5><strong>Profesor de los cursos:</strong></h5>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 text-center">
							<img alt="" class="img-fluid rounded" src="<?php echo $link; ?>img/cartarecom.jpg" style="">
						</div>
						<div class="col-md-6">
							<p style="text-align: left;"><strong>Gabriel Buduba</strong>, Piloto de Yate Vela/Motor Profesional, ha dictado cursos de navegación en la Fragata Sarmiento, en distintas facultades de la Universidad de Buenos Aires y en la Universidad Católica Argentina, responsable junto al Servicio de Hidrografía Naval en el proyecto Regla de Mar, Ex Secretario de Redacción y Asesor Legal de la revista de información a los navegantes Seaport Report, con más de veinte años en la docencia náutica. Capitán de diversos barcos escuela en las costas argentinas, uruguayas y brasileras. Director y creador del portal de náutica para navegantes:&nbsp;www.navemocion.com</p>
						</div>
					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
