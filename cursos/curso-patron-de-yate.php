
<!doctype html>
<html lang="es">
  <head>
      <?php
          include '../common/head.php';
      ?>
  </head>
  <body>
    <header style="height: 100px;">
      <?php
          include '../common/nav.php';
      ?>
    </header>
    <main role="main">
      <div class="container img-header">
        <div class="col-md-12 px-0">
          <h1 class="display-4">Cursos - Curso de Patrón de Yate a Vela y Motor</h1>
        </div>
      </div>
      <div class="container grey">
			<div class="row">
				<div class="col-md-12 align-self-center">
					<div class="row justify-content-md-center bg-light">
						<div class="col-md-4 text-center">
							<img alt="" class="img-fluid" style="width: 175px;"src="<?php echo $link; ?>img/patron1.jpg"/>
						</div>
						<div class="col-md-8 align-self-center">
							<h4>Curso:</h4>
							<h4>Curso de Patrón de Yate a Vela y Motor</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<p>FECHA DE INCIO:</p>
					<div class="commontext"><b><u>Marzo y Agosto</u></b>
					<p><b>Duración del curso:</b>cuatro meses.</p>
					<p><b>Informes e inscripción:</b>&nbsp;4701-4410&nbsp;<br>
					<span id="cloak3b194af7fd44bbdf15de923ca6120e4d"><a href="mailto:info@navemocion.com">info@navemocion.com</a></span><script type="text/javascript">
						document.getElementById('cloak3b194af7fd44bbdf15de923ca6120e4d').innerHTML = '';
						var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
						var path = 'hr' + 'ef' + '=';
						var addy3b194af7fd44bbdf15de923ca6120e4d = '&#105;nf&#111;' + '&#64;';
						addy3b194af7fd44bbdf15de923ca6120e4d = addy3b194af7fd44bbdf15de923ca6120e4d + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';
						var addy_text3b194af7fd44bbdf15de923ca6120e4d = '&#105;nf&#111;' + '&#64;' + 'n&#97;v&#101;m&#111;c&#105;&#111;n' + '&#46;' + 'c&#111;m';document.getElementById('cloak3b194af7fd44bbdf15de923ca6120e4d').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy3b194af7fd44bbdf15de923ca6120e4d + '\'>'+addy_text3b194af7fd44bbdf15de923ca6120e4d+'<\/a>';
					</script><br>
					Para cualquier tipo de consulta (por ejemplo a cerca de otros días u horarios de clase, también comunicarse por las mismas vías).</p>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10 bg-info-2 p-3">
					<div class="row m-2">
						<div class="col-md-12 align-self-center">
							<h5><strong>Profesor de los cursos:</strong></h5>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 text-center">
							<img alt="" class="img-fluid rounded"  src="<?php echo $link; ?>img/cartarecom.jpg" style="">
						</div>
						<div class="col-md-6">
							<p><strong>Gabriel Buduba</strong>, Piloto de Yate Vela/Motor Profesional, ha dictado cursos de navegación en la Fragata Sarmiento, en distintas facultades de la Universidad de Buenos Aires y en la Universidad Católica Argentina, responsable junto al Servicio de Hidrografía Naval en el proyecto Regla de Mar, Ex Secretario de Redacción y Asesor Legal de la revista de información a los navegantes Seaport Report, con más de veinte años en la docencia náutica. Capitán de diversos barcos escuela en las costas argentinas, uruguayas y brasileras. Director y creador del portal de náutica para navegantes:&nbsp;www.navemocion.com</p>
						</div>
					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row mt-5">
				<div class="col-md-1"></div>
				<div class="col-md-10">
						<div class="bluetext">Es un curso orientado a quienes <strong>ya son Timoneles</strong> y desean ampliar sus horizontes en el campo de la náutica.</div>
						<p><b>Finalidad</b>: Se busca profundizar los conocimientos adquiridos en el curso de Timonel, no solamente en lo teórico sino fundamentalmente en la parte práctica con viajes de instrucción a Punta del Este, Mar del Plata y Brasil. En este curso se agregan los elementos necesarios para enfrentar una navegación de largo aliento y sin costa a la vista, ya que la habilitación de Patrón nos permite navegar en todo el Río de la Plata (Interior y Exterior) y también realizar navegaciones oceánicas dentro de las 12 millas de la costa (cada milla náutica equivale a 1852 metros, y es el arco barrido por un minuto de latitud).</p>
						<p><b>Clases Teóricas:</b>&nbsp;Se dictarán un día de semana de 19:00 a 21:30 horas. El temario teórico estará orientado hacia aquellos temas que tengan real importancia en la vida y necesidades de a bordo. Se plantearán en cada una de las clases problemas de navegación para resolver en la semana con los conocimientos adquiridos.</p>
						<p><b>Clases Prácticas:</b>&nbsp;Este curso de Patrón tiene la ventaja de reforzar los conocimientos teóricos a través de tres prácticas, en las que se verán temas como G.P.S, Sextante, Medición de Abatimiento, Spinaker, Cartografía y Navegación Nocturna. Se zarpará desde Buenos Aires en día y horario a convenir, teniendo cada clase una duración de tres horas. Se utilizarán embarcaciones cabinadas de 28 pies con todos los elementos reglamentarios, y seguro contra todo riesgo.</p>
						<p><b>Habilitación:</b>&nbsp;Una vez terminadas las clases, se presentará a los alumnos ante las autoridades de Prefectura, con el objeto de que rindan el exámen para obtener su brevet de patrón de yate a vela y/o motor.</p>
				</div>
				<div class="col-md-1"></div>
			</div>

		</div>
      <!-- /.container -->
      <!-- FOOTER -->
      <?php
          include '../common/footer.php';
      ?>
    </main>
  </body>
</html>
